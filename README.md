# L-Math - The simple math library #

A simple math library made to interface with the LWJGL.




Notes:

  * If a method name begins with *unwrap*, it means: unwrap the data in this thing, and put it into this thing.
  * If a method name contains 'lerp', it means: interpolation.
  * If a method name contains 'random', it means that the method uses the random-instance in the LMath-class to generate random numbers.
