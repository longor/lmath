TODO!

  * Create a Matrix3f class. (how? no internet?)
  * Create a Matrix3d class. (how? no internet?)
  * Create a class that can handle spline-interpolation of different interpolation-methods (both 3D and 2D).
  * Create a class that represents a infinite Plane.
  * Create a class that can handle line-segments.
  * Create a class that can generate a Icosahedron-Sphere (IcoSphere) model.
  * Create a class that can generate a cube model.
  * Create a class that can generate a cylinder model.
  * Create a class that can generate a heightmap model.
  * Create a class that can do things with 3D-models.
  * Create a class capable of rendering 3D-Data onto a 2D-Plane with ONLY Java2D. (No realtime-rendering needed)
  * Create a class that can generate a rectangle/circle/(sp-)line/roundedrectangle.
  * Create a class that represents RGBAs colors.

???

...

PROFIT!
