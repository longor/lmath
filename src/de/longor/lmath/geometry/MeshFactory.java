package de.longor.lmath.geometry;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import de.longor.lmath.geometry.geom3D.Aabb;

public class MeshFactory
{
	
	public static MeshData generateCubeMeshData(float width, float height, float length, boolean centered)
	{
		Aabb box = new Aabb();
		
		if(centered)
		{
			box.minX = width / -2f;
			box.minY = height / -2f;
			box.minZ = length / -2f;
			box.maxX = width / 2f;
			box.maxY = height / 2f;
			box.maxZ = length / 2f;
		}
		else
		{
			box.minX = 0;
			box.minY = 0;
			box.minZ = 0;
			box.maxX = width;
			box.maxY = height;
			box.maxZ = length;
		}
		
		return generateCubeMeshData(box);
	}
	
	private static MeshData generateCubeMeshData(Aabb box)
	{
		final int numOf_point = 6/*faces*/ * 4/*points per face*/;
		final int numOf_edge = 6/*faces*/ * 4/*edges per face*/;
		final int sizeOf_point = 2/*vec3f*/ * 3/*x,y,z*/ * 4/*float*/;
		final int sizeOf_edge = 2/*int*/ * 1/*i*/ * 4/*int*/;
		
		MeshData mesh = new MeshData();
		mesh.pointBuffer = ByteBuffer.allocateDirect(numOf_point*sizeOf_point).order(ByteOrder.nativeOrder()).asFloatBuffer();
		mesh.edgeBuffer = ByteBuffer.allocateDirect(numOf_edge*sizeOf_edge).order(ByteOrder.nativeOrder()).asIntBuffer();
		
		// Cube Vertices
		FloatBuffer pointBuffer = mesh.pointBuffer;
		{
			final float[] normal = new float[3];
			
			// (positive y)
			normal[0] = 0; normal[1] = +1; normal[2] = 0;
			pointBuffer.put(box.minX); pointBuffer.put(box.maxY); pointBuffer.put(box.minZ); pointBuffer.put(normal); // @0
			pointBuffer.put(box.minX); pointBuffer.put(box.maxY); pointBuffer.put(box.maxZ); pointBuffer.put(normal); // @6
			pointBuffer.put(box.maxX); pointBuffer.put(box.maxY); pointBuffer.put(box.maxZ); pointBuffer.put(normal); // @12
			pointBuffer.put(box.maxX); pointBuffer.put(box.maxY); pointBuffer.put(box.minZ); pointBuffer.put(normal); // @18
			
			// (negative y)
			normal[0] = 0; normal[1] = -1; normal[2] = 0;
			pointBuffer.put(box.minX); pointBuffer.put(box.minY); pointBuffer.put(box.minZ); pointBuffer.put(normal); // @24
			pointBuffer.put(box.maxX); pointBuffer.put(box.minY); pointBuffer.put(box.minZ); pointBuffer.put(normal); // @30
			pointBuffer.put(box.maxX); pointBuffer.put(box.minY); pointBuffer.put(box.maxZ); pointBuffer.put(normal); // @36
			pointBuffer.put(box.minX); pointBuffer.put(box.minY); pointBuffer.put(box.maxZ); pointBuffer.put(normal); // @42
			
			// (positive x)
			normal[0] = +1; normal[1] = 0; normal[2] = 0;
			pointBuffer.put(box.maxX); pointBuffer.put(box.minY); pointBuffer.put(box.minZ); pointBuffer.put(normal); // @48
			pointBuffer.put(box.maxX); pointBuffer.put(box.maxY); pointBuffer.put(box.minZ); pointBuffer.put(normal); // @54
			pointBuffer.put(box.maxX); pointBuffer.put(box.maxY); pointBuffer.put(box.maxZ); pointBuffer.put(normal); // @60
			pointBuffer.put(box.maxX); pointBuffer.put(box.minY); pointBuffer.put(box.maxZ); pointBuffer.put(normal); // @66
			
			// (negative x)
			normal[0] = -1; normal[1] = 0; normal[2] = 0;
			pointBuffer.put(box.minX); pointBuffer.put(box.minY); pointBuffer.put(box.maxZ); pointBuffer.put(normal); // @72
			pointBuffer.put(box.minX); pointBuffer.put(box.maxY); pointBuffer.put(box.maxZ); pointBuffer.put(normal); // @78
			pointBuffer.put(box.minX); pointBuffer.put(box.maxY); pointBuffer.put(box.minZ); pointBuffer.put(normal); // @84
			pointBuffer.put(box.minX); pointBuffer.put(box.minY); pointBuffer.put(box.minZ); pointBuffer.put(normal); // @90
			
			// (negative z)
			normal[0] = 0; normal[1] = 0; normal[2] = -1;
			pointBuffer.put(box.minX); pointBuffer.put(box.minY); pointBuffer.put(box.minZ); pointBuffer.put(normal); // @96
			pointBuffer.put(box.minX); pointBuffer.put(box.maxY); pointBuffer.put(box.minZ); pointBuffer.put(normal); // @102
			pointBuffer.put(box.maxX); pointBuffer.put(box.maxY); pointBuffer.put(box.minZ); pointBuffer.put(normal); // @108
			pointBuffer.put(box.maxX); pointBuffer.put(box.minY); pointBuffer.put(box.minZ); pointBuffer.put(normal); // @114
			
			// (positive z)
			normal[0] = 0; normal[1] = 0; normal[2] = +1;
			pointBuffer.put(box.maxX); pointBuffer.put(box.minY); pointBuffer.put(box.maxZ); pointBuffer.put(normal); // @120
			pointBuffer.put(box.maxX); pointBuffer.put(box.maxY); pointBuffer.put(box.maxZ); pointBuffer.put(normal); // @126
			pointBuffer.put(box.minX); pointBuffer.put(box.maxY); pointBuffer.put(box.maxZ); pointBuffer.put(normal); // @132
			pointBuffer.put(box.minX); pointBuffer.put(box.minY); pointBuffer.put(box.maxZ); pointBuffer.put(normal); // @138
		}
		pointBuffer.flip();
		
		IntBuffer edgeBuffer = mesh.edgeBuffer;
		{
			//*
			edgeBuffer.put(0); edgeBuffer.put(6);   // @0
			edgeBuffer.put(6); edgeBuffer.put(12);  // @2
			edgeBuffer.put(12); edgeBuffer.put(18); // @4
			edgeBuffer.put(18); edgeBuffer.put(0);  // @6
			//*/
			
			//*
			edgeBuffer.put(24); edgeBuffer.put(30); // @8
			edgeBuffer.put(30); edgeBuffer.put(36); // @10
			edgeBuffer.put(36); edgeBuffer.put(42); // @12
			edgeBuffer.put(42); edgeBuffer.put(24); // @14
			//*/
			
			//*
			edgeBuffer.put(48); edgeBuffer.put(54); // @16
			edgeBuffer.put(54); edgeBuffer.put(60); // @18
			edgeBuffer.put(60); edgeBuffer.put(66); // @20
			edgeBuffer.put(66); edgeBuffer.put(48); // @22
			//*/
			
			//*
			edgeBuffer.put(72); edgeBuffer.put(78); // @24
			edgeBuffer.put(78); edgeBuffer.put(84); // @26
			edgeBuffer.put(84); edgeBuffer.put(90); // @28
			edgeBuffer.put(90); edgeBuffer.put(72); // @30
			//*/
			
			//*
			edgeBuffer.put(96); edgeBuffer.put(102); // @32
			edgeBuffer.put(102); edgeBuffer.put(108);// @34
			edgeBuffer.put(108); edgeBuffer.put(114);// @36
			edgeBuffer.put(114); edgeBuffer.put(96); // @38
			//*/
			
			//*
			edgeBuffer.put(120); edgeBuffer.put(126);// @40
			edgeBuffer.put(126); edgeBuffer.put(132);// @42
			edgeBuffer.put(132); edgeBuffer.put(138);// @44
			edgeBuffer.put(138); edgeBuffer.put(120);// @46
			//*/
		}
		edgeBuffer.flip();
		
		// TODO: Face Data (LMath-MeshFactory)
		
		return mesh;
	}
	
}
