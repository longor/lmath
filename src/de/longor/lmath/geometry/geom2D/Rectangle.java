package de.longor.lmath.geometry.geom2D;

public interface Rectangle
{
	public Rectangle getCopy();
	
	public float getX();
	public float getY();
	public float getWidth();
	public float getHeight();
	
	public Rectangle setX(float newValue);
	public Rectangle setY(float newValue);
	public Rectangle setWidth(float newValue);
	public Rectangle setHeight(float newValue);
	
	public Rectangle setLocation(float newX, float newY);
	public Rectangle setSize(float newWidth, float newHeight);
	public Rectangle setBounds(float newX, float newY, float newWidth, float newHeight);
}
