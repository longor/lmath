package de.longor.lmath.geometry.geom2D;

/**
 * Size of any 'something' in a 2D-space as a AxiAlignedBoundingBox.
 **/
public interface Dimension
{
	public void setWidth(int newValue);
	public void setHeight(int newValue);
	
	public void setWidth(float newValue);
	public void setHeight(float newValue);
	
	public void setWidth(double newValue);
	public void setHeight(double newValue);
	
	public Dimension set(Dimension dimension);
	public Dimension set(int newW, int newH);
	public Dimension set(float newW, float newH);
	public Dimension set(double newW, double newH);
	
	public int getWidthI();
	public int getHeightI();
	
	public float getWidthF();
	public float getHeightF();
	
	public static final class DimensionInt implements Dimension
	{
		int width;
		int height;
		
		@Override
		public void setWidth(int newValue)
		{
			width = newValue;
		}

		@Override
		public void setHeight(int newValue)
		{
			height = newValue;
		}

		@Override
		public void setWidth(float newValue)
		{
			width = (int) newValue;
		}

		@Override
		public void setHeight(float newValue)
		{
			height = (int) newValue;
		}

		@Override
		public void setWidth(double newValue)
		{
			width = (int) newValue;
		}

		@Override
		public void setHeight(double newValue)
		{
			height = (int) newValue;
		}

		@Override
		public Dimension set(Dimension dimension)
		{
			width = dimension.getWidthI();
			height = dimension.getHeightI();
			return this;
		}

		@Override
		public Dimension set(int newW, int newH)
		{
			width = newW;
			height = newH;
			return this;
		}

		@Override
		public Dimension set(float newW, float newH)
		{
			width = (int) newW;
			height = (int) newH;
			return this;
		}

		@Override
		public Dimension set(double newW, double newH)
		{
			width = (int) newW;
			height = (int) newH;
			return this;
		}

		@Override
		public int getWidthI()
		{
			return width;
		}

		@Override
		public int getHeightI()
		{
			return height;
		}

		@Override
		public float getWidthF()
		{
			return width;
		}

		@Override
		public float getHeightF()
		{
			return height;
		}
	}
	
}
