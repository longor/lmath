package de.longor.lmath.geometry.geom3D;

import java.util.Collection;

import de.longor.lmath.tuples.Tuple3;
import de.longor.lmath.tuples.Vec3f;

/**
 * This class represents a "Axis Aligned Bounding Box",
 * which is basically a set of two points that make up two opposite corners of a box.
 **/
public class Aabb
{
	/**
	 * Minimum X
	 **/
	public float minX;
	
	/**
	 * Minimum Y
	 **/
	public float minY;
	
	/**
	 * Minimum Z
	 **/
	public float minZ;
	
	/**
	 * Maximum X
	 **/
	public float maxX;
	
	/**
	 * Maximum Y
	 **/
	public float maxY;
	
	/**
	 * Maximum Z
	 **/
	public float maxZ;
	
	/**
	 * Creates a default AABB that goes from (-1,-1,-1) to (+1,+1,+1).
	 **/
	public Aabb()
	{
		this.minX = -1;
		this.minY = -1;
		this.minZ = -1;
		this.maxX = +1;
		this.maxY = +1;
		this.maxZ = +1;
	}
	
	/**
	 * Creates a AABB which is a copy of the given in AABB.
	 **/
	public Aabb(Aabb box)
	{
		this.minX = box.minX;
		this.minY = box.minY;
		this.minZ = box.minZ;
		this.maxX = box.maxX;
		this.maxY = box.maxY;
		this.maxZ = box.maxZ;
	}
	
	/**
	 * Creates a AABB with the two given corner-points.
	 **/
	public Aabb(float minX, float minY, float minZ, float maxX, float maxY, float maxZ)
	{
		this.minX = minX;
		this.minY = minY;
		this.minZ = minZ;
		this.maxX = maxX;
		this.maxY = maxY;
		this.maxZ = maxZ;
	}
	
	/**
	 * Creates a AABB given a width, height and length.
	 * Also has a flag to control whenever the box is centered around the origin
	 * or goes into the positive axis directions.
	 **/
	public Aabb(float width, float height, float length, boolean centered)
	{
		if(centered)
		{
			this.minX = width / -2f;
			this.minY = height / -2f;
			this.minZ = length / -2f;
			this.maxX = width / 2f;
			this.maxY = height / 2f;
			this.maxZ = length / 2f;
		}
		else
		{
			this.minX = 0;
			this.minY = 0;
			this.minZ = 0;
			this.maxX = width;
			this.maxY = height;
			this.maxZ = length;
		}
	}
	
	/**
	 * Creates a AABB given a width, height and length.
	 * Also has a flag to control whenever the box is centered around the origin
	 * or goes into the positive axis directions.
	 **/
	public Aabb(Vec3f extents, boolean centered)
	{
		if(centered)
		{
			minX = -extents.x;
			minY = -extents.y;
			minZ = -extents.z;
			maxX = +extents.x;
			maxY = +extents.y;
			maxZ = +extents.z;
		}
		else
		{
			minX = extents.x*2;
			minY = extents.y*2;
			minZ = extents.z*2;
			maxX = extents.x*2;
			maxY = extents.y*2;
			maxZ = extents.z*2;
		}
	}
	
	/**
	 * Creates a exact copy of this AABB.
	 **/
	@Override
	public Aabb clone()
	{
		return new Aabb(this);
	}
	
	/**
	 * Generates a 'JSON'-style string representation of this AABB.
	 **/
	@Override
	public String toString()
	{
		return "aabb:[" + minX + ", " + minY + ", " + minZ + ", " + maxX + ", " + maxY + ", " + maxZ + "]";
	}
	
	/**
	 * Moves this AABB along the given translation vector, and stores the result in this AABB-instance.
	 **/
	public Aabb translate(float x, float y, float z)
	{
		Aabb.translate(this, x, y, z, this);
		return this;
	}
	
	/**
	 * Moves this AABB along the given translation vector, and stores the result in the given AABB-instance.
	 **/
	public Aabb translate(float x, float y, float z, Aabb outbox)
	{
		Aabb.translate(this, x, y, z, outbox);
		return outbox;
	}
	
	/**
	 * Moves this AABB along the given translation vector, and stores the result in this AABB-instance.
	 **/
	public Aabb translate(Vec3f translation)
	{
		Aabb.translate(this, translation, this);
		return this;
	}
	
	/**
	 * Moves this AABB along the given translation vector, and stores the result in the given AABB-instance.
	 **/
	public Aabb translate(Vec3f translation, Aabb outbox)
	{
		Aabb.translate(this, translation, outbox);
		return outbox;
	}
	
	/**
	 * Moves this AABB along the given translation vector, and stores the result in this AABB-instance.
	 **/
	public Aabb translate(Tuple3 translation)
	{
		Aabb.translate(this, translation, this);
		return this;
	}
	
	/**
	 * Moves this AABB along the given translation vector, and stores the result in the given AABB-instance.
	 **/
	public Aabb translate(Tuple3 translation, Aabb outbox)
	{
		Aabb.translate(this, translation, outbox);
		return outbox;
	}
	
	/**
	 * Expands this AABB by the given extent, and stores the result in this AABB-instance.
	 **/
	public Aabb expand(float extent)
	{
		Aabb.expand(this, extent, extent, extent, this);
		return this;
	}
	
	/**
	 * Expands this AABB by the given extent, and stores the result in this AABB-instance.
	 **/
	public Aabb expand(float x, float y, float z)
	{
		Aabb.expand(this, x, y, z, this);
		return this;
	}
	
	/**
	 * Expands this AABB by the given extent, and stores the result in this AABB-instance.
	 **/
	public Aabb expand(Vec3f expents)
	{
		Aabb.expand(this, expents, this);
		return this;
	}
	
	/**
	 * Expands this AABB by the given extent, and stores the result in the given AABB-instance.
	 **/
	public Aabb expand(float extent, Aabb outbox)
	{
		Aabb.expand(this, extent, outbox);
		return outbox;
	}
	
	/**
	 * Expands this AABB by the given extent, and stores the result in the given AABB-instance.
	 **/
	public Aabb expand(float x, float y, float z, Aabb outbox)
	{
		Aabb.expand(this, x, y, z, outbox);
		return outbox;
	}
	
	/**
	 * Expands this AABB by the given extent, and stores the result in the given AABB-instance.
	 **/
	public Aabb expand(Vec3f expents, Aabb outbox)
	{
		Aabb.expand(this, expents, outbox);
		return outbox;
	}
	
	/**
	 * Expands this AABB so the given vector is contained within. The result is stored in this AABB-instance.
	 **/
	public Aabb expandToFitVector(float x, float y, float z)
	{
		Aabb.expandToFitVector(this, x,y,z, this);
		return this;
	}
	
	/**
	 * Expands this AABB so the given vector is contained within. The result is stored in the given AABB-instance.
	 **/
	public Aabb expandToFitVector(float x, float y, float z, Aabb outbox)
	{
		Aabb.expandToFitVector(this, x,y,z, outbox);
		return outbox;
	}
	
	/**
	 * Expands this AABB so the given vector is contained within. The result is stored in this AABB-instance.
	 **/
	public Aabb expandToFitVector(Vec3f vec)
	{
		Aabb.expandToFitVector(this, vec, this);
		return this;
	}
	
	/**
	 * Expands this AABB so the given vector is contained within. The result is stored in the given AABB-instance.
	 **/
	public Aabb expandToFitVector(Vec3f tuple, Aabb outbox)
	{
		Aabb.expandToFitVector(this, tuple, outbox);
		return outbox;
	}
	
	/**
	 * Expands this AABB so the given vector is contained within. The result is stored in this AABB-instance.
	 **/
	public Aabb expandToFitVector(Tuple3 tuple)
	{
		Aabb.expandToFitVector(this, tuple, this);
		return this;
	}
	
	/**
	 * Expands this AABB so the given vector is contained within. The result is stored in the given AABB-instance.
	 **/
	public Aabb expandToFitVector(Tuple3 tuple, Aabb outbox)
	{
		Aabb.expandToFitVector(this, tuple, outbox);
		return outbox;
	}
	
	public static final void translate(Aabb box, float x, float y, float z, Aabb outbox)
	{
		outbox.minX = box.minX + x;
		outbox.minY = box.minY + y;
		outbox.minZ = box.minZ + z;
		outbox.maxX = box.maxX + x;
		outbox.maxY = box.maxY + y;
		outbox.maxZ = box.maxZ + z;
	}
	
	public static final void translate(Aabb box, Vec3f translation, Aabb outbox)
	{
		outbox.minX = box.minX + translation.x;
		outbox.minY = box.minY + translation.y;
		outbox.minZ = box.minZ + translation.z;
		outbox.maxX = box.maxX + translation.x;
		outbox.maxY = box.maxY + translation.y;
		outbox.maxZ = box.maxZ + translation.z;
	}
	
	public static final void translate(Aabb box, Tuple3 translation, Aabb outbox)
	{
		outbox.minX = box.minX + translation.getXf();
		outbox.minY = box.minY + translation.getYf();
		outbox.minZ = box.minZ + translation.getZf();
		outbox.maxX = box.maxX + translation.getXf();
		outbox.maxY = box.maxY + translation.getYf();
		outbox.maxZ = box.maxZ + translation.getZf();
	}
	
	public static final void expand(Aabb box, float xyz, Aabb outbox)
	{
		outbox.minX = box.minX - xyz;
		outbox.minY = box.minY - xyz;
		outbox.minZ = box.minZ - xyz;
		outbox.maxX = box.maxX + xyz;
		outbox.maxY = box.maxY + xyz;
		outbox.maxZ = box.maxZ + xyz;
	}
	
	public static final void expand(Aabb box, float x, float y, float z, Aabb outbox)
	{
		outbox.minX = box.minX - x;
		outbox.minY = box.minY - y;
		outbox.minZ = box.minZ - z;
		outbox.maxX = box.maxX + x;
		outbox.maxY = box.maxY + y;
		outbox.maxZ = box.maxZ + z;
	}
	
	public static final void expand(Aabb box, Vec3f extents, Aabb outbox)
	{
		outbox.minX = box.minX - extents.x;
		outbox.minY = box.minY - extents.y;
		outbox.minZ = box.minZ - extents.z;
		outbox.maxX = box.maxX + extents.x;
		outbox.maxY = box.maxY + extents.y;
		outbox.maxZ = box.maxZ + extents.z;
	}
	
	public static final void expandToFitVector(Aabb box, float inx, float iny, float inz, Aabb outbox)
	{
		outbox.minX = Math.min(box.minX, inx);
		outbox.minY = Math.min(box.minY, iny);
		outbox.minZ = Math.min(box.minZ, inz);
		outbox.maxX = Math.max(box.maxX, inx);
		outbox.maxY = Math.max(box.maxY, iny);
		outbox.maxZ = Math.max(box.maxZ, inz);
	}
	
	public static final void expandToFitVector(Aabb box, Vec3f vec, Aabb outbox)
	{
		float inx = vec.x;
		float iny = vec.y;
		float inz = vec.z;
		outbox.minX = Math.min(box.minX, inx);
		outbox.minY = Math.min(box.minY, iny);
		outbox.minZ = Math.min(box.minZ, inz);
		outbox.maxX = Math.max(box.maxX, inx);
		outbox.maxY = Math.max(box.maxY, iny);
		outbox.maxZ = Math.max(box.maxZ, inz);
	}
	
	public static final void expandToFitVector(Aabb box, Tuple3 tuple, Aabb outbox)
	{
		float inx = tuple.getXf();
		float iny = tuple.getYf();
		float inz = tuple.getZf();
		outbox.minX = Math.min(box.minX, inx);
		outbox.minY = Math.min(box.minY, iny);
		outbox.minZ = Math.min(box.minZ, inz);
		outbox.maxX = Math.max(box.maxX, inx);
		outbox.maxY = Math.max(box.maxY, iny);
		outbox.maxZ = Math.max(box.maxZ, inz);
	}

	public static Aabb overlap(Aabb A, Aabb B, Aabb OUT)
	{
		OUT.minX = Math.max(A.minX, B.minX);
		OUT.minY = Math.max(A.minY, B.minY);
		OUT.minZ = Math.max(A.minZ, B.minZ);
		
		OUT.maxX = Math.min(A.maxX, B.maxX);
		OUT.maxY = Math.min(A.maxY, B.maxY);
		OUT.maxZ = Math.min(A.maxZ, B.maxZ);
		
		// check if the result is unreal, and if it is, set all values to NaN.
		if(OUT.isUnreal())
		{
			OUT.set(Float.NaN);
		}
		
		return OUT;
	}
	
	public void set(float val)
	{
		minX = maxX = minY = maxY = minZ = maxZ = val;
	}
	
	public void set(float x, float y, float z)
	{
		minX = maxX = x;
		minY = maxY = y;
		minZ = maxZ = z;
	}
	
	/**
	 * Checks if this AABB is not 'real'.
	 * @return False, if MIN is bigger than MAX on nay axis. True if not.
	 **/
	public boolean isUnreal()
	{
		float diffx = maxX - minX;
		float diffy = maxY - minY;
		float diffz = maxZ - minZ;
		
		if(diffx < 0) return true;
		if(diffy < 0) return true;
		if(diffz < 0) return true;
		
		return false;
	}
	
	public static final Aabb createBoundingBoxForCollectionOfVectors(Collection<Tuple3> vectors, Aabb outbox)
	{
		if(vectors.size() == 0)
			return outbox;
		
		// Get the first vector in the collection and make teh AABB a cube around that single vector.
		Tuple3 first = vectors.iterator().next();
		outbox.set(first.getXf(),first.getYf(),first.getZf());
		
		// Iterate trough ALL vectors and make sure that the AABB encompasses them all.
		for(Tuple3 vector : vectors)
		{
			expandToFitVector(outbox, vector, outbox);
		}
		
		// Expand the AABB a tiny bit
		expand(outbox, 0.01f, outbox);
		
		return outbox;
	}
	
}
