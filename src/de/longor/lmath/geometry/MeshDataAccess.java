package de.longor.lmath.geometry;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public interface MeshDataAccess
{
	/**
	 * {FOR_EACH ENTRY}
	 * Point:
	 *   - Position vec3f
	 *   - Normal vec3f
	 **/
	public FloatBuffer pointBuffer();
	
	/**
	 * {FOR_EACH ENTRY}
	 * Edge:
	 *   - StartVertexIndex int
	 *   - EndVertexIndex int
	 **/
	public IntBuffer edgeBuffer();
	
	/**
	 * {FOR_EACH ENTRY}
	 * Face:
	 *   - FaceNormal vec3f
	 *   - FaceTangent vec3f
	 *   - FaceBiTangent vec3f
	 *   - Number of Edges int
	 *   - EdgeIndices int['Number of Edges'].
	 **/
	public ByteBuffer faceBuffer();
	
}
