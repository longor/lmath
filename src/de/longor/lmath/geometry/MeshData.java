package de.longor.lmath.geometry;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public class MeshData implements MeshDataAccess
{
	FloatBuffer pointBuffer;
	IntBuffer edgeBuffer;
	ByteBuffer faceBuffer;
	
	public MeshData()
	{
		pointBuffer = null;
		edgeBuffer = null;
		faceBuffer = null;
	}
	
	@Override
	public FloatBuffer pointBuffer() {
		return pointBuffer;
	}
	@Override
	public IntBuffer edgeBuffer() {
		return edgeBuffer;
	}
	@Override
	public ByteBuffer faceBuffer() {
		return faceBuffer;
	}
	
}
