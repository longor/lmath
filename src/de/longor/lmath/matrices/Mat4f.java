package de.longor.lmath.matrices;

import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;

import de.longor.lmath.LMath;
import de.longor.lmath.tuples.Tuple3;
import de.longor.lmath.tuples.Tuple4;
import de.longor.lmath.tuples.Vec3f;
import de.longor.lmath.tuples.Vec4f;





public class Mat4f implements IMatrix
{
	private static final ThreadLocal<Mat4f> quickScrapMat = new ThreadLocal<Mat4f>(){
		public Mat4f initialValue() { return new Mat4f(); };
	};
	
	// Matrix Fields
	/**00**/public float m00; /**01**/public float m01; /**02**/public float m02; /**03**/public float m03;
	/**04**/public float m10; /**05**/public float m11; /**06**/public float m12; /**07**/public float m13;
	/**08**/public float m20; /**09**/public float m21; /**10**/public float m22; /**11**/public float m23;
	/**12**/public float m30; /**13**/public float m31; /**14**/public float m32; /**15**/public float m33;
	
	/**
	 * Default Constructor. (Sets Matrix to Identity)
	 **/
	public Mat4f()
	{
		this.m00 = 1.0f;
		this.m01 = 0.0f;
		this.m02 = 0.0f;
		this.m03 = 0.0f;
		this.m10 = 0.0f;
		this.m11 = 1.0f;
		this.m12 = 0.0f;
		this.m13 = 0.0f;
		this.m20 = 0.0f;
		this.m21 = 0.0f;
		this.m22 = 1.0f;
		this.m23 = 0.0f;
		this.m30 = 0.0f;
		this.m31 = 0.0f;
		this.m32 = 0.0f;
		this.m33 = 1.0f;
	}
	
	/**
	 * Copy constructor.
	 **/
	public Mat4f(Mat4f mat)
	{
		this.m00 = mat.m00;
		this.m01 = mat.m01;
		this.m02 = mat.m02;
		this.m03 = mat.m03;
		this.m10 = mat.m10;
		this.m11 = mat.m11;
		this.m12 = mat.m12;
		this.m13 = mat.m13;
		this.m20 = mat.m20;
		this.m21 = mat.m21;
		this.m22 = mat.m22;
		this.m23 = mat.m23;
		this.m30 = mat.m30;
		this.m31 = mat.m31;
		this.m32 = mat.m32;
		this.m33 = mat.m33;
	}
	
	/**
	 * 'From Array' constructor.
	 **/
	public Mat4f(float[] array)
	{
		int i = 0;
		this.m00 = array[i++];
		this.m01 = array[i++];
		this.m02 = array[i++];
		this.m03 = array[i++];
		this.m10 = array[i++];
		this.m11 = array[i++];
		this.m12 = array[i++];
		this.m13 = array[i++];
		this.m20 = array[i++];
		this.m21 = array[i++];
		this.m22 = array[i++];
		this.m23 = array[i++];
		this.m30 = array[i++];
		this.m31 = array[i++];
		this.m32 = array[i++];
		this.m33 = array[i++];
	}
	
	/**
	 * Creates a 'Translated' Matrix
	 **/
	public Mat4f(float x, float y, float z)
	{
		this.m00 = 1.0f;
		this.m01 = 0.0f;
		this.m02 = 0.0f;
		this.m03 = 0.0f;
		this.m10 = 0.0f;
		this.m11 = 1.0f;
		this.m12 = 0.0f;
		this.m13 = 0.0f;
		this.m20 = 0.0f;
		this.m21 = 0.0f;
		this.m22 = 1.0f;
		this.m23 = 0.0f;
		
		this.m30 = (this.m00 * x) + (this.m10 * y) + (this.m20 * z);
		this.m31 = (this.m01 * x) + (this.m11 * y) + (this.m21 * z);
		this.m32 = (this.m02 * x) + (this.m12 * y) + (this.m22 * z);
		this.m33 = 1.0f + ((this.m03 * x) + (this.m13 * y) + (this.m23 * z));
	}
	
	@Override
	public String toString()
	{
		String S =
				"[" +
						this.m00 + ", " + this.m01 + ", "+ this.m02 + ", "+ this.m03 + ", "+
						this.m10 + ", " + this.m11 + ", "+ this.m12 + ", "+ this.m13 + ", "+
						this.m20 + ", " + this.m21 + ", "+ this.m22 + ", "+ this.m23 + ", "+
						this.m30 + ", " + this.m31 + ", "+ this.m32 + ", "+ this.m33 +
				"]";
		
		return S;
	}
	
	@Override
	public Mat4f clone()
	{
		return new Mat4f(this);
	}

	
	/**
	 *
	 **/
	public Mat4f setToIdentity()
	{
		this.m00 = 1.0f; this.m01 = 0.0f; this.m02 = 0.0f; this.m03 = 0.0f;
		this.m10 = 0.0f; this.m11 = 1.0f; this.m12 = 0.0f; this.m13 = 0.0f;
		this.m20 = 0.0f; this.m21 = 0.0f; this.m22 = 1.0f; this.m23 = 0.0f;
		this.m30 = 0.0f; this.m31 = 0.0f; this.m32 = 0.0f; this.m33 = 1.0f;
		return this;
	}
	
	/**
	 *
	 **/
	public Mat4f setToZero()
	{
		this.m00 = 0.0f;
		this.m01 = 0.0f;
		this.m02 = 0.0f;
		this.m03 = 0.0f;
		this.m10 = 0.0f;
		this.m11 = 0.0f;
		this.m12 = 0.0f;
		this.m13 = 0.0f;
		this.m20 = 0.0f;
		this.m21 = 0.0f;
		this.m22 = 0.0f;
		this.m23 = 0.0f;
		this.m30 = 0.0f;
		this.m31 = 0.0f;
		this.m32 = 0.0f;
		this.m33 = 0.0f;
		return this;
	}
	
	/**
	 * Copies the contents of the given matrix into this matrix, overriding the existing data.
	 * @param mat The matrix to copy from.
	 * @return This matrix.
	 **/
	public Mat4f setToMatrix(Mat4f mat)
	{
		this.m00 = mat.m00;
		this.m01 = mat.m01;
		this.m02 = mat.m02;
		this.m03 = mat.m03;
		this.m10 = mat.m10;
		this.m11 = mat.m11;
		this.m12 = mat.m12;
		this.m13 = mat.m13;
		this.m20 = mat.m20;
		this.m21 = mat.m21;
		this.m22 = mat.m22;
		this.m23 = mat.m23;
		this.m30 = mat.m30;
		this.m31 = mat.m31;
		this.m32 = mat.m32;
		this.m33 = mat.m33;
		return this;
	}
	
	/**
	 * Copies the contents of the given buffer into this matrix, overriding the existing data.
	 * @param buf The buffer to copy the contents from.
	 * @return This matrix.
	 **/
	public Mat4f setToFloatBuffer(FloatBuffer buf)
	{
		this.m00 = buf.get();
		this.m01 = buf.get();
		this.m02 = buf.get();
		this.m03 = buf.get();
		this.m10 = buf.get();
		this.m11 = buf.get();
		this.m12 = buf.get();
		this.m13 = buf.get();
		this.m20 = buf.get();
		this.m21 = buf.get();
		this.m22 = buf.get();
		this.m23 = buf.get();
		this.m30 = buf.get();
		this.m31 = buf.get();
		this.m32 = buf.get();
		this.m33 = buf.get();
		return this;
	}
	
	/**
	 * Copies the contents of the given buffer into this matrix and transposes this matrix, overriding the existing data.
	 * @param buf The buffer to copy the contents from.
	 * @return This matrix.
	 **/
	public Mat4f setToFloatBufferAndTranspose(FloatBuffer buf)
	{
		this.m00 = buf.get();
		this.m10 = buf.get();
		this.m20 = buf.get();
		this.m30 = buf.get();
		this.m01 = buf.get();
		this.m11 = buf.get();
		this.m21 = buf.get();
		this.m31 = buf.get();
		this.m02 = buf.get();
		this.m12 = buf.get();
		this.m22 = buf.get();
		this.m32 = buf.get();
		this.m03 = buf.get();
		this.m13 = buf.get();
		this.m23 = buf.get();
		this.m33 = buf.get();
		return this;
	}
	
	/**
	 * Copies the contents of the given array into this matrix, overriding the existing data.
	 * @param buf The array to copy the contents from.
	 * @return This matrix.
	 **/
	public Mat4f setToArray(float[] buf)
	{
		int i = 0;
		this.m00 = buf[i++];
		this.m01 = buf[i++];
		this.m02 = buf[i++];
		this.m03 = buf[i++];
		this.m10 = buf[i++];
		this.m11 = buf[i++];
		this.m12 = buf[i++];
		this.m13 = buf[i++];
		this.m20 = buf[i++];
		this.m21 = buf[i++];
		this.m22 = buf[i++];
		this.m23 = buf[i++];
		this.m30 = buf[i++];
		this.m31 = buf[i++];
		this.m32 = buf[i++];
		this.m33 = buf[i++];
		return this;
	}
	
	public Mat4f zeroOutTranslation()
	{
		this.m30 = 0.0f; this.m31 = 0.0f; this.m32 = 0.0f; this.m33 = 1.0f;
		return this;
	}

	@Override
	public void unwrapMatrixData(float[] out, int position) {
		int i = position;
		out[i++] = m00;
		out[i++] = m01;
		out[i++] = m02;
		out[i++] = m03;
		out[i++] = m10;
		out[i++] = m11;
		out[i++] = m12;
		out[i++] = m13;
		out[i++] = m20;
		out[i++] = m21;
		out[i++] = m22;
		out[i++] = m23;
		out[i++] = m30;
		out[i++] = m31;
		out[i++] = m32;
		out[i++] = m33;
	}

	@Override
	public void unwrapMatrixData(double[] out, int position) {
		int i = position;
		out[i++] = m00;
		out[i++] = m01;
		out[i++] = m02;
		out[i++] = m03;
		out[i++] = m10;
		out[i++] = m11;
		out[i++] = m12;
		out[i++] = m13;
		out[i++] = m20;
		out[i++] = m21;
		out[i++] = m22;
		out[i++] = m23;
		out[i++] = m30;
		out[i++] = m31;
		out[i++] = m32;
		out[i++] = m33;
	}

	@Override
	public FloatBuffer unwrapMatrixData(FloatBuffer buffer) {
		buffer.put(m00);
		buffer.put(m01);
		buffer.put(m02);
		buffer.put(m03);
		buffer.put(m10);
		buffer.put(m11);
		buffer.put(m12);
		buffer.put(m13);
		buffer.put(m20);
		buffer.put(m21);
		buffer.put(m22);
		buffer.put(m23);
		buffer.put(m30);
		buffer.put(m31);
		buffer.put(m32);
		buffer.put(m33);
		return buffer;
	}

	@Override
	public DoubleBuffer unwrapMatrixData(DoubleBuffer buffer) {
		buffer.put(m00);
		buffer.put(m01);
		buffer.put(m02);
		buffer.put(m03);
		buffer.put(m10);
		buffer.put(m11);
		buffer.put(m12);
		buffer.put(m13);
		buffer.put(m20);
		buffer.put(m21);
		buffer.put(m22);
		buffer.put(m23);
		buffer.put(m30);
		buffer.put(m31);
		buffer.put(m32);
		buffer.put(m33);
		return buffer;
	}
	
	/**
	 *
	 **/
	private static final float determinant3x3(
					float d00, float d01, float d02,
				    float d10, float d11, float d12,
				    float d20, float d21, float d22
		)
	{
		return   (d00 * ((d11 * d22) - (d12 * d21)))
		       + (d01 * ((d12 * d20) - (d10 * d22)))
		       + (d02 * ((d10 * d21) - (d11 * d20)));
	}
	
	/**
	 *
	 **/
	public static final Mat4f add(Mat4f left, Mat4f right, Mat4f dest)
	{
		dest.m00 = left.m00 + right.m00;
		dest.m01 = left.m01 + right.m01;
		dest.m02 = left.m02 + right.m02;
		dest.m03 = left.m03 + right.m03;
		dest.m10 = left.m10 + right.m10;
		dest.m11 = left.m11 + right.m11;
		dest.m12 = left.m12 + right.m12;
		dest.m13 = left.m13 + right.m13;
		dest.m20 = left.m20 + right.m20;
		dest.m21 = left.m21 + right.m21;
		dest.m22 = left.m22 + right.m22;
		dest.m23 = left.m23 + right.m23;
		dest.m30 = left.m30 + right.m30;
		dest.m31 = left.m31 + right.m31;
		dest.m32 = left.m32 + right.m32;
		dest.m33 = left.m33 + right.m33;
		return dest;
	}
	
	/**
	 *
	 **/
	public static final Mat4f sub(Mat4f left, Mat4f right, Mat4f dest)
	{
		dest.m00 = left.m00 - right.m00;
		dest.m01 = left.m01 - right.m01;
		dest.m02 = left.m02 - right.m02;
		dest.m03 = left.m03 - right.m03;
		dest.m10 = left.m10 - right.m10;
		dest.m11 = left.m11 - right.m11;
		dest.m12 = left.m12 - right.m12;
		dest.m13 = left.m13 - right.m13;
		dest.m20 = left.m20 - right.m20;
		dest.m21 = left.m21 - right.m21;
		dest.m22 = left.m22 - right.m22;
		dest.m23 = left.m23 - right.m23;
		dest.m30 = left.m30 - right.m30;
		dest.m31 = left.m31 - right.m31;
		dest.m32 = left.m32 - right.m32;
		dest.m33 = left.m33 - right.m33;
		
		return dest;
	}
	
	/**
	 *
	 **/
	public static final Mat4f mul(Mat4f leftMatrix, Mat4f rightMatrix, Mat4f destination)
	{
		float m00 = (leftMatrix.m00 * rightMatrix.m00) + (leftMatrix.m10 * rightMatrix.m01) + (leftMatrix.m20 * rightMatrix.m02) + (leftMatrix.m30 * rightMatrix.m03);
		float m01 = (leftMatrix.m01 * rightMatrix.m00) + (leftMatrix.m11 * rightMatrix.m01) + (leftMatrix.m21 * rightMatrix.m02) + (leftMatrix.m31 * rightMatrix.m03);
		float m02 = (leftMatrix.m02 * rightMatrix.m00) + (leftMatrix.m12 * rightMatrix.m01) + (leftMatrix.m22 * rightMatrix.m02) + (leftMatrix.m32 * rightMatrix.m03);
		float m03 = (leftMatrix.m03 * rightMatrix.m00) + (leftMatrix.m13 * rightMatrix.m01) + (leftMatrix.m23 * rightMatrix.m02) + (leftMatrix.m33 * rightMatrix.m03);
		float m10 = (leftMatrix.m00 * rightMatrix.m10) + (leftMatrix.m10 * rightMatrix.m11) + (leftMatrix.m20 * rightMatrix.m12) + (leftMatrix.m30 * rightMatrix.m13);
		float m11 = (leftMatrix.m01 * rightMatrix.m10) + (leftMatrix.m11 * rightMatrix.m11) + (leftMatrix.m21 * rightMatrix.m12) + (leftMatrix.m31 * rightMatrix.m13);
		float m12 = (leftMatrix.m02 * rightMatrix.m10) + (leftMatrix.m12 * rightMatrix.m11) + (leftMatrix.m22 * rightMatrix.m12) + (leftMatrix.m32 * rightMatrix.m13);
		float m13 = (leftMatrix.m03 * rightMatrix.m10) + (leftMatrix.m13 * rightMatrix.m11) + (leftMatrix.m23 * rightMatrix.m12) + (leftMatrix.m33 * rightMatrix.m13);
		float m20 = (leftMatrix.m00 * rightMatrix.m20) + (leftMatrix.m10 * rightMatrix.m21) + (leftMatrix.m20 * rightMatrix.m22) + (leftMatrix.m30 * rightMatrix.m23);
		float m21 = (leftMatrix.m01 * rightMatrix.m20) + (leftMatrix.m11 * rightMatrix.m21) + (leftMatrix.m21 * rightMatrix.m22) + (leftMatrix.m31 * rightMatrix.m23);
		float m22 = (leftMatrix.m02 * rightMatrix.m20) + (leftMatrix.m12 * rightMatrix.m21) + (leftMatrix.m22 * rightMatrix.m22) + (leftMatrix.m32 * rightMatrix.m23);
		float m23 = (leftMatrix.m03 * rightMatrix.m20) + (leftMatrix.m13 * rightMatrix.m21) + (leftMatrix.m23 * rightMatrix.m22) + (leftMatrix.m33 * rightMatrix.m23);
		float m30 = (leftMatrix.m00 * rightMatrix.m30) + (leftMatrix.m10 * rightMatrix.m31) + (leftMatrix.m20 * rightMatrix.m32) + (leftMatrix.m30 * rightMatrix.m33);
		float m31 = (leftMatrix.m01 * rightMatrix.m30) + (leftMatrix.m11 * rightMatrix.m31) + (leftMatrix.m21 * rightMatrix.m32) + (leftMatrix.m31 * rightMatrix.m33);
		float m32 = (leftMatrix.m02 * rightMatrix.m30) + (leftMatrix.m12 * rightMatrix.m31) + (leftMatrix.m22 * rightMatrix.m32) + (leftMatrix.m32 * rightMatrix.m33);
		float m33 = (leftMatrix.m03 * rightMatrix.m30) + (leftMatrix.m13 * rightMatrix.m31) + (leftMatrix.m23 * rightMatrix.m32) + (leftMatrix.m33 * rightMatrix.m33);
		
		destination.m00 = m00;
		destination.m01 = m01;
		destination.m02 = m02;
		destination.m03 = m03;
		destination.m10 = m10;
		destination.m11 = m11;
		destination.m12 = m12;
		destination.m13 = m13;
		destination.m20 = m20;
		destination.m21 = m21;
		destination.m22 = m22;
		destination.m23 = m23;
		destination.m30 = m30;
		destination.m31 = m31;
		destination.m32 = m32;
		destination.m33 = m33;
		return destination;
	}
	
	/**
	 *
	 **/
	public static final Mat4f translate(Vec3f translationVector, Mat4f source, Mat4f destination)
	{
		destination.m30 += (source.m00 * translationVector.x) + (source.m10 * translationVector.y) + (source.m20 * translationVector.z);
		destination.m31 += (source.m01 * translationVector.x) + (source.m11 * translationVector.y) + (source.m21 * translationVector.z);
		destination.m32 += (source.m02 * translationVector.x) + (source.m12 * translationVector.y) + (source.m22 * translationVector.z);
		destination.m33 += (source.m03 * translationVector.x) + (source.m13 * translationVector.y) + (source.m23 * translationVector.z);
		
		return destination;
	}
	
	/**
	 *
	 **/
	public static final Mat4f translate(Vec4f translationVector, Mat4f source, Mat4f destination)
	{
		destination.m30 += (source.m00 * translationVector.x) + (source.m10 * translationVector.y) + (source.m20 * translationVector.z);
		destination.m31 += (source.m01 * translationVector.x) + (source.m11 * translationVector.y) + (source.m21 * translationVector.z);
		destination.m32 += (source.m02 * translationVector.x) + (source.m12 * translationVector.y) + (source.m22 * translationVector.z);
		destination.m33 += (source.m03 * translationVector.x) + (source.m13 * translationVector.y) + (source.m23 * translationVector.z);
		
		return destination;
	}
	
	/**
	 *
	 **/
	public static final Mat4f translateNegative(Vec3f translationVector, Mat4f source, Mat4f destination)
	{
		float X = -translationVector.x;
		float Y = -translationVector.y;
		float Z = -translationVector.z;
		
		destination.m30 += (source.m00 * X) + (source.m10 * Y) + (source.m20 * Z);
		destination.m31 += (source.m01 * X) + (source.m11 * Y) + (source.m21 * Z);
		destination.m32 += (source.m02 * X) + (source.m12 * Y) + (source.m22 * Z);
		destination.m33 += (source.m03 * X) + (source.m13 * Y) + (source.m23 * Z);
		
		return destination;
	}
	
	/**
	 *
	 **/
	public static final Mat4f translate(
			float translationVectorX,
			float translationVectorY,
			float translationVectorZ,
			Mat4f source, Mat4f destination)
	{
		destination.m30 += (source.m00 * translationVectorX) + (source.m10 * translationVectorY) + (source.m20 * translationVectorZ);
		destination.m31 += (source.m01 * translationVectorX) + (source.m11 * translationVectorY) + (source.m21 * translationVectorZ);
		destination.m32 += (source.m02 * translationVectorX) + (source.m12 * translationVectorY) + (source.m22 * translationVectorZ);
		destination.m33 += (source.m03 * translationVectorX) + (source.m13 * translationVectorY) + (source.m23 * translationVectorZ);
		
		return destination;
	}
	
	/**
	 *
	 **/
	public static final Mat4f translate(
			double translationVectorX,
			double translationVectorY,
			double translationVectorZ,
			Mat4f source, Mat4f destination)
	{
		destination.m30 += (source.m00 * translationVectorX) + (source.m10 * translationVectorY) + (source.m20 * translationVectorZ);
		destination.m31 += (source.m01 * translationVectorX) + (source.m11 * translationVectorY) + (source.m21 * translationVectorZ);
		destination.m32 += (source.m02 * translationVectorX) + (source.m12 * translationVectorY) + (source.m22 * translationVectorZ);
		destination.m33 += (source.m03 * translationVectorX) + (source.m13 * translationVectorY) + (source.m23 * translationVectorZ);
		
		return destination;
	}
	
	/**
	 *
	 **/
	public static final Mat4f scale(Vec3f scaleVector, Mat4f source, Mat4f destination)
	{
		destination.m00 = source.m00 * scaleVector.x;
		destination.m01 = source.m01 * scaleVector.x;
		destination.m02 = source.m02 * scaleVector.x;
		destination.m03 = source.m03 * scaleVector.x;
		destination.m10 = source.m10 * scaleVector.y;
		destination.m11 = source.m11 * scaleVector.y;
		destination.m12 = source.m12 * scaleVector.y;
		destination.m13 = source.m13 * scaleVector.y;
		destination.m20 = source.m20 * scaleVector.z;
		destination.m21 = source.m21 * scaleVector.z;
		destination.m22 = source.m22 * scaleVector.z;
		destination.m23 = source.m23 * scaleVector.z;
		return destination;
	}
	
	/**
	 *
	 **/
	public static final Mat4f scale(float x, float y, float z, Mat4f source, Mat4f destination)
	{
		destination.m00 = source.m00 * x;
		destination.m01 = source.m01 * x;
		destination.m02 = source.m02 * x;
		destination.m03 = source.m03 * x;
		destination.m10 = source.m10 * y;
		destination.m11 = source.m11 * y;
		destination.m12 = source.m12 * y;
		destination.m13 = source.m13 * y;
		destination.m20 = source.m20 * z;
		destination.m21 = source.m21 * z;
		destination.m22 = source.m22 * z;
		destination.m23 = source.m23 * z;
		return destination;
	}
	
	/**
	 * @return destination
	 **/
	public static final Mat4f rotateAroundAxisByRadians(float angleToRotateBy, Vec3f axisToRotateAround, Mat4f source, Mat4f destination)
	{
		float cos = (float) Math.cos(angleToRotateBy);
		float sin = (float) Math.sin(angleToRotateBy);
		float cosFlipped = 1.0f - cos;
		float xy = axisToRotateAround.x*axisToRotateAround.y;
		float yz = axisToRotateAround.y*axisToRotateAround.z;
		float xz = axisToRotateAround.x*axisToRotateAround.z;
		float xs = axisToRotateAround.x*sin;
		float ys = axisToRotateAround.y*sin;
		float zs = axisToRotateAround.z*sin;
		
		// X-Axis
		float d00 = (axisToRotateAround.x*axisToRotateAround.x*cosFlipped)+cos;
		float d01 = (xy*cosFlipped)+zs;
		float d02 = (xz*cosFlipped)-ys;
		
		// Y-Axis
		float d10 = (xy*cosFlipped)-zs;
		float d11 = (axisToRotateAround.y*axisToRotateAround.y*cosFlipped)+cos;
		float d12 = (yz*cosFlipped)+xs;
		
		// Z-Axis
		float d20 = (xz*cosFlipped)+ys;
		float d21 = (yz*cosFlipped)-xs;
		float d22 = (axisToRotateAround.z*axisToRotateAround.z*cosFlipped)+cos;
		
		
		float f00 = (source.m00 * d00) + (source.m10 * d01) + (source.m20 * d02);
		float f01 = (source.m01 * d00) + (source.m11 * d01) + (source.m21 * d02);
		float f02 = (source.m02 * d00) + (source.m12 * d01) + (source.m22 * d02);
		float f03 = (source.m03 * d00) + (source.m13 * d01) + (source.m23 * d02);
		float f10 = (source.m00 * d10) + (source.m10 * d11) + (source.m20 * d12);
		float f11 = (source.m01 * d10) + (source.m11 * d11) + (source.m21 * d12);
		float f12 = (source.m02 * d10) + (source.m12 * d11) + (source.m22 * d12);
		float f13 = (source.m03 * d10) + (source.m13 * d11) + (source.m23 * d12);
		
		destination.m20 = (source.m00 * d20) + (source.m10 * d21) + (source.m20 * d22);
		destination.m21 = (source.m01 * d20) + (source.m11 * d21) + (source.m21 * d22);
		destination.m22 = (source.m02 * d20) + (source.m12 * d21) + (source.m22 * d22);
		destination.m23 = (source.m03 * d20) + (source.m13 * d21) + (source.m23 * d22);
		
		destination.m00 = f00;
		destination.m01 = f01;
		destination.m02 = f02;
		destination.m03 = f03;
		destination.m10 = f10;
		destination.m11 = f11;
		destination.m12 = f12;
		destination.m13 = f13;
		return destination;
	}
	
	/**
	 * @return destination
	 **/
	public static final Mat4f rotateAroundAxisByRadians(
			float angleToRotateBy,
			float axisToRotateAroundX, float axisToRotateAroundY, float axisToRotateAroundZ,
			Mat4f source, Mat4f destination
		)
	{
		float cos = (float) Math.cos(angleToRotateBy);
		float sin = (float) Math.sin(angleToRotateBy);
		float cosFlipped = 1.0f - cos;
		float xy = axisToRotateAroundX*axisToRotateAroundY;
		float yz = axisToRotateAroundY*axisToRotateAroundZ;
		float xz = axisToRotateAroundX*axisToRotateAroundZ;
		float xs = axisToRotateAroundX*sin;
		float ys = axisToRotateAroundY*sin;
		float zs = axisToRotateAroundZ*sin;
		
		// X-Axis
		float d00 = (axisToRotateAroundX*axisToRotateAroundX*cosFlipped)+cos;
		float d01 = (xy*cosFlipped)+zs;
		float d02 = (xz*cosFlipped)-ys;
		
		// Y-Axis
		float d10 = (xy*cosFlipped)-zs;
		float d11 = (axisToRotateAroundY*axisToRotateAroundY*cosFlipped)+cos;
		float d12 = (yz*cosFlipped)+xs;
		
		// Z-Axis
		float d20 = (xz*cosFlipped)+ys;
		float d21 = (yz*cosFlipped)-xs;
		float d22 = (axisToRotateAroundZ*axisToRotateAroundZ*cosFlipped)+cos;
		
		
		float f00 = (source.m00 * d00) + (source.m10 * d01) + (source.m20 * d02);
		float f01 = (source.m01 * d00) + (source.m11 * d01) + (source.m21 * d02);
		float f02 = (source.m02 * d00) + (source.m12 * d01) + (source.m22 * d02);
		float f03 = (source.m03 * d00) + (source.m13 * d01) + (source.m23 * d02);
		float f10 = (source.m00 * d10) + (source.m10 * d11) + (source.m20 * d12);
		float f11 = (source.m01 * d10) + (source.m11 * d11) + (source.m21 * d12);
		float f12 = (source.m02 * d10) + (source.m12 * d11) + (source.m22 * d12);
		float f13 = (source.m03 * d10) + (source.m13 * d11) + (source.m23 * d12);
		
		destination.m20 = (source.m00 * d20) + (source.m10 * d21) + (source.m20 * d22);
		destination.m21 = (source.m01 * d20) + (source.m11 * d21) + (source.m21 * d22);
		destination.m22 = (source.m02 * d20) + (source.m12 * d21) + (source.m22 * d22);
		destination.m23 = (source.m03 * d20) + (source.m13 * d21) + (source.m23 * d22);
		
		destination.m00 = f00;
		destination.m01 = f01;
		destination.m02 = f02;
		destination.m03 = f03;
		destination.m10 = f10;
		destination.m11 = f11;
		destination.m12 = f12;
		destination.m13 = f13;
		return destination;
	}
	
	
	
	
	
	
	
	
	
	

	
	/**
	 * @return destination
	 **/
	public static final Mat4f rotateAroundAxisByDegrees(float angleToRotateBy, Vec3f axisToRotateAround, Mat4f source, Mat4f destination)
	{
		angleToRotateBy *= 0.0174532925;
		float cos = (float) Math.cos(angleToRotateBy);
		float sin = (float) Math.sin(angleToRotateBy);
		float cosFlipped = 1.0f - cos;
		
		float xy = axisToRotateAround.x*axisToRotateAround.y;
		float yz = axisToRotateAround.y*axisToRotateAround.z;
		float xz = axisToRotateAround.x*axisToRotateAround.z;
		float xs = axisToRotateAround.x*sin;
		float ys = axisToRotateAround.y*sin;
		float zs = axisToRotateAround.z*sin;
		
		// X-Axis
		float d00 = (axisToRotateAround.x*axisToRotateAround.x*cosFlipped)+cos;
		float d01 = (xy*cosFlipped)+zs;
		float d02 = (xz*cosFlipped)-ys;
		
		// Y-Axis
		float d10 = (xy*cosFlipped)-zs;
		float d11 = (axisToRotateAround.y*axisToRotateAround.y*cosFlipped)+cos;
		float d12 = (yz*cosFlipped)+xs;
		
		// Z-Axis
		float d20 = (xz*cosFlipped)+ys;
		float d21 = (yz*cosFlipped)-xs;
		float d22 = (axisToRotateAround.z*axisToRotateAround.z*cosFlipped)+cos;
		
		
		float f00 = (source.m00 * d00) + (source.m10 * d01) + (source.m20 * d02);
		float f01 = (source.m01 * d00) + (source.m11 * d01) + (source.m21 * d02);
		float f02 = (source.m02 * d00) + (source.m12 * d01) + (source.m22 * d02);
		float f03 = (source.m03 * d00) + (source.m13 * d01) + (source.m23 * d02);
		float f10 = (source.m00 * d10) + (source.m10 * d11) + (source.m20 * d12);
		float f11 = (source.m01 * d10) + (source.m11 * d11) + (source.m21 * d12);
		float f12 = (source.m02 * d10) + (source.m12 * d11) + (source.m22 * d12);
		float f13 = (source.m03 * d10) + (source.m13 * d11) + (source.m23 * d12);
		
		destination.m20 = (source.m00 * d20) + (source.m10 * d21) + (source.m20 * d22);
		destination.m21 = (source.m01 * d20) + (source.m11 * d21) + (source.m21 * d22);
		destination.m22 = (source.m02 * d20) + (source.m12 * d21) + (source.m22 * d22);
		destination.m23 = (source.m03 * d20) + (source.m13 * d21) + (source.m23 * d22);
		
		destination.m00 = f00;
		destination.m01 = f01;
		destination.m02 = f02;
		destination.m03 = f03;
		destination.m10 = f10;
		destination.m11 = f11;
		destination.m12 = f12;
		destination.m13 = f13;
		return destination;
	}

	
	/**
	 * @return destination
	 **/
	public static final Mat4f rotateAroundAxisByDegrees(
			float angleToRotateBy,
			float axisToRotateAroundX, float axisToRotateAroundY, float axisToRotateAroundZ,
			Mat4f source, Mat4f destination
		)
	{
		angleToRotateBy *= 0.0174532925;
		float cos = (float) Math.cos(angleToRotateBy);
		float sin = (float) Math.sin(angleToRotateBy);
		float cosFlipped = 1.0f - cos;
		
		float xy = axisToRotateAroundX*axisToRotateAroundY;
		float yz = axisToRotateAroundY*axisToRotateAroundZ;
		float xz = axisToRotateAroundX*axisToRotateAroundZ;
		float xs = axisToRotateAroundX*sin;
		float ys = axisToRotateAroundY*sin;
		float zs = axisToRotateAroundZ*sin;
		
		// X-Axis
		float d00 = (axisToRotateAroundX*axisToRotateAroundX*cosFlipped)+cos;
		float d01 = (xy*cosFlipped)+zs;
		float d02 = (xz*cosFlipped)-ys;
		
		// Y-Axis
		float d10 = (xy*cosFlipped)-zs;
		float d11 = (axisToRotateAroundY*axisToRotateAroundY*cosFlipped)+cos;
		float d12 = (yz*cosFlipped)+xs;
		
		// Z-Axis
		float d20 = (xz*cosFlipped)+ys;
		float d21 = (yz*cosFlipped)-xs;
		float d22 = (axisToRotateAroundZ*axisToRotateAroundZ*cosFlipped)+cos;
		
		
		float f00 = (source.m00 * d00) + (source.m10 * d01) + (source.m20 * d02);
		float f01 = (source.m01 * d00) + (source.m11 * d01) + (source.m21 * d02);
		float f02 = (source.m02 * d00) + (source.m12 * d01) + (source.m22 * d02);
		float f03 = (source.m03 * d00) + (source.m13 * d01) + (source.m23 * d02);
		float f10 = (source.m00 * d10) + (source.m10 * d11) + (source.m20 * d12);
		float f11 = (source.m01 * d10) + (source.m11 * d11) + (source.m21 * d12);
		float f12 = (source.m02 * d10) + (source.m12 * d11) + (source.m22 * d12);
		float f13 = (source.m03 * d10) + (source.m13 * d11) + (source.m23 * d12);
		
		destination.m20 = (source.m00 * d20) + (source.m10 * d21) + (source.m20 * d22);
		destination.m21 = (source.m01 * d20) + (source.m11 * d21) + (source.m21 * d22);
		destination.m22 = (source.m02 * d20) + (source.m12 * d21) + (source.m22 * d22);
		destination.m23 = (source.m03 * d20) + (source.m13 * d21) + (source.m23 * d22);
		
		destination.m00 = f00;
		destination.m01 = f01;
		destination.m02 = f02;
		destination.m03 = f03;
		destination.m10 = f10;
		destination.m11 = f11;
		destination.m12 = f12;
		destination.m13 = f13;
		return destination;
	}
	
	
	/**
	 * Inverts the given matrix 'source', and stores the result in 'destination'.
	 * @return The 'destination', or null if the matrix could not be inverted.
	 **/
	public static final Mat4f invert(Mat4f source, Mat4f destination)
	{
		// Get the determinant.
		float determinant = source.determinant();
		
		if (determinant != 0)
		{
			// Calculate the divisor of the determinant.
			float determinant_inv = 1f/determinant;
			
			// --- Calculate the inverse, row by row.
			// first row
			float t00 =  determinant3x3(source.m11, source.m12, source.m13, source.m21, source.m22, source.m23, source.m31, source.m32, source.m33);
			float t01 = -determinant3x3(source.m10, source.m12, source.m13, source.m20, source.m22, source.m23, source.m30, source.m32, source.m33);
			float t02 =  determinant3x3(source.m10, source.m11, source.m13, source.m20, source.m21, source.m23, source.m30, source.m31, source.m33);
			float t03 = -determinant3x3(source.m10, source.m11, source.m12, source.m20, source.m21, source.m22, source.m30, source.m31, source.m32);
			
			// second row
			float t10 = -determinant3x3(source.m01, source.m02, source.m03, source.m21, source.m22, source.m23, source.m31, source.m32, source.m33);
			float t11 =  determinant3x3(source.m00, source.m02, source.m03, source.m20, source.m22, source.m23, source.m30, source.m32, source.m33);
			float t12 = -determinant3x3(source.m00, source.m01, source.m03, source.m20, source.m21, source.m23, source.m30, source.m31, source.m33);
			float t13 =  determinant3x3(source.m00, source.m01, source.m02, source.m20, source.m21, source.m22, source.m30, source.m31, source.m32);
			
			// third row
			float t20 =  determinant3x3(source.m01, source.m02, source.m03, source.m11, source.m12, source.m13, source.m31, source.m32, source.m33);
			float t21 = -determinant3x3(source.m00, source.m02, source.m03, source.m10, source.m12, source.m13, source.m30, source.m32, source.m33);
			float t22 =  determinant3x3(source.m00, source.m01, source.m03, source.m10, source.m11, source.m13, source.m30, source.m31, source.m33);
			float t23 = -determinant3x3(source.m00, source.m01, source.m02, source.m10, source.m11, source.m12, source.m30, source.m31, source.m32);
			
			// fourth row
			float t30 = -determinant3x3(source.m01, source.m02, source.m03, source.m11, source.m12, source.m13, source.m21, source.m22, source.m23);
			float t31 =  determinant3x3(source.m00, source.m02, source.m03, source.m10, source.m12, source.m13, source.m20, source.m22, source.m23);
			float t32 = -determinant3x3(source.m00, source.m01, source.m03, source.m10, source.m11, source.m13, source.m20, source.m21, source.m23);
			float t33 =  determinant3x3(source.m00, source.m01, source.m02, source.m10, source.m11, source.m12, source.m20, source.m21, source.m22);
			
			// Transpose and divide by the determinant.
			destination.m00 = t00*determinant_inv;
			destination.m11 = t11*determinant_inv;
			destination.m22 = t22*determinant_inv;
			destination.m33 = t33*determinant_inv;
			
			destination.m01 = t10*determinant_inv;
			destination.m10 = t01*determinant_inv;
			destination.m20 = t02*determinant_inv;
			destination.m02 = t20*determinant_inv;
			
			destination.m12 = t21*determinant_inv;
			destination.m21 = t12*determinant_inv;
			destination.m03 = t30*determinant_inv;
			destination.m30 = t03*determinant_inv;
			
			destination.m13 = t31*determinant_inv;
			destination.m31 = t13*determinant_inv;
			destination.m32 = t23*determinant_inv;
			destination.m23 = t32*determinant_inv;
			
			return destination;
		}
		
		return null;
	}
	
	/**
	 * @return destination
	 **/
	public static final Vec3f transformVec3f(Mat4f transform, Vec3f source, Vec3f destination)
	{
		float x = (transform.m00 * source.x) + (transform.m10 * source.y) + (transform.m20 * source.z) + (transform.m30);
		float y = (transform.m01 * source.x) + (transform.m11 * source.y) + (transform.m21 * source.z) + (transform.m31);
		float z = (transform.m02 * source.x) + (transform.m12 * source.y) + (transform.m22 * source.z) + (transform.m32);
		
		destination.x = x;
		destination.y = y;
		destination.z = z;
		
		return destination;
	}
	
	/**
	 * @return destination
	 **/
	public static final Vec4f transformVec4f(Mat4f transform, Vec4f source, Vec4f destination) {
		float x = (transform.m00 * source.x) + (transform.m10 * source.y) + (transform.m20 * source.z) + (transform.m30 * source.w);
		float y = (transform.m01 * source.x) + (transform.m11 * source.y) + (transform.m21 * source.z) + (transform.m31 * source.w);
		float z = (transform.m02 * source.x) + (transform.m12 * source.y) + (transform.m22 * source.z) + (transform.m32 * source.w);
		float w = (transform.m03 * source.x) + (transform.m13 * source.y) + (transform.m23 * source.z) + (transform.m33 * source.w);
		
		destination.x = x;
		destination.y = y;
		destination.z = z;
		destination.w = w;
		
		return destination;
	}
	
	/**
	 * @return destination
	 **/
	public static final Tuple3 transformTuple3(Mat4f transform, Tuple3 source, Tuple3 destination)
	{
		float sourcex = source.getXf();
		float sourcey = source.getYf();
		float sourcez = source.getZf();
		float x = (transform.m00 * sourcex) + (transform.m10 * sourcey) + (transform.m20 * sourcez) + (transform.m30);
		float y = (transform.m01 * sourcex) + (transform.m11 * sourcey) + (transform.m21 * sourcez) + (transform.m31);
		float z = (transform.m02 * sourcex) + (transform.m12 * sourcey) + (transform.m22 * sourcez) + (transform.m32);
		
		destination.setXYZ(x, y, z);
		
		return destination;
	}
	
	/**
	 * @return destination
	 **/
	public static final Tuple4 transformTuple4(Mat4f transform, Tuple4 source, Tuple4 destination)
	{
		float sourcex = source.getXf();
		float sourcey = source.getYf();
		float sourcez = source.getZf();
		float sourcew = source.getWf();
		float x = (transform.m00 * sourcex) + (transform.m10 * sourcey) + (transform.m20 * sourcez) + (transform.m30 * sourcew);
		float y = (transform.m01 * sourcex) + (transform.m11 * sourcey) + (transform.m21 * sourcez) + (transform.m31 * sourcew);
		float z = (transform.m02 * sourcex) + (transform.m12 * sourcey) + (transform.m22 * sourcez) + (transform.m32 * sourcew);
		float w = (transform.m03 * sourcex) + (transform.m13 * sourcey) + (transform.m23 * sourcez) + (transform.m33 * sourcew);
		
		destination.setXYZW(x, y, z, w);
		return destination;
	}
	
	/**
	 * Transposes the matrix SOURCE and stores the result in DESTINATION.
	 * @param source The matrix to transpose.
	 * @param destination The matrix to store the result in.
	 **/
	public static final Mat4f transpose(Mat4f source, Mat4f destination)
	{
		float m00 = source.m00;
		float m01 = source.m10;
		float m02 = source.m20;
		float m03 = source.m30;
		float m10 = source.m01;
		float m11 = source.m11;
		float m12 = source.m21;
		float m13 = source.m31;
		float m20 = source.m02;
		float m21 = source.m12;
		float m22 = source.m22;
		float m23 = source.m32;
		float m30 = source.m03;
		float m31 = source.m13;
		float m32 = source.m23;
		float m33 = source.m33;
		
		destination.m00 = m00;
		destination.m01 = m01;
		destination.m02 = m02;
		destination.m03 = m03;
		destination.m10 = m10;
		destination.m11 = m11;
		destination.m12 = m12;
		destination.m13 = m13;
		destination.m20 = m20;
		destination.m21 = m21;
		destination.m22 = m22;
		destination.m23 = m23;
		destination.m30 = m30;
		destination.m31 = m31;
		destination.m32 = m32;
		destination.m33 = m33;
		
		return destination;
	}
	
	/**
	 * Calculates the perspective projection matrix as described on Wikipedia.
	 * @param fovy The Field of View.
	 * @param aspect The aspect ratio of the viewport.
	 * @param zNear The near-plane.
	 * @param zFar The far-plane.
	 * @throws IllegalArgumentException If any of the parameters are invalid. (Aspect == 0, zFar-zNear == 0, fovy <= 0 OR fovy >= 360)
	 **/
	public void calculatePerspectiveProjectionMatrix(float fovy, float aspect, float zNear, float zFar) {
		float sine, cotangent, deltaZ;
		float radians = ((fovy / 2F) * (float)Math.PI) / 180F;
		
		deltaZ = zFar - zNear;
		sine = (float) Math.sin(radians);
		
		if ((deltaZ == 0) || (sine <= 0.01f) || (aspect == 0))
		{
			throw new IllegalArgumentException("fov/aspect/near/far ratios are impossible.");
		}
		
		cotangent = (float) Math.cos(radians) / sine;
		
		setToIdentity();
		
		this.m00 = (cotangent / aspect);
		this.m11 = (cotangent);
		this.m22 = ((-2 * zNear * zFar) / deltaZ);
		this.m23 = (-1);
		this.m31 = (- (zFar + zNear) / deltaZ);
		this.m32 = (0);
		
		zeroOutTranslation();
	}
	
	/**
	 * Calculates the perspective projection matrix the exact same way the GLU-library does it.
	 * @param fovy The Field of View.
	 * @param aspect The aspect ratio of the viewport.
	 * @param zNear The near-plane.
	 * @param zFar The far-plane.
	 **/
	public void calculatePerspectiveProjectionMatrixGLU(float fovy, float aspect, float zNear, float zFar)
	{
		float sine, cotangent, deltaZ;
		float radians = fovy / 2f * LMath.PIf / 180f;
		
		deltaZ = zFar - zNear;
		sine = (float) Math.sin(radians);
		
		if ((deltaZ == 0) || (sine == 0) || (aspect == 0)) {
			return;
		}
		
		cotangent = (float) Math.cos(radians) / sine;
		
		Mat4f matrix = quickScrapMat.get();
		matrix.setToIdentity();
		
		matrix.m00 = cotangent / aspect;
		matrix.m11 = cotangent;
		matrix.m22 = - (zFar + zNear) / deltaZ;
		matrix.m23 = -1;
		matrix.m32 = -2 * zNear * zFar / deltaZ;
		matrix.m33 = 0;
		
		Mat4f.mul(this, matrix, this);
	}
	
	/**
	 *
	 **/
	public void calculateOrthoProjectionMatrix(float left, float right, float bottom, float top, float near, float far)
	{
		this.m00 = +2F / (right-left);	this.m10 = 0;					this.m20 = 0;					this.m30 = -((right+left)/(right-left));
		this.m01 = 0;					this.m11 = +2F / (top-bottom);	this.m21 = 0;					this.m31 = -((top+bottom)/(top-bottom));
		this.m02 = 0;					this.m12 = 0;					this.m22 = -2F / (far-near);	this.m32 = (far+near) / (far-near);
		this.m03 = 0;					this.m13 = 0;					this.m23 = 0;					this.m33 = 1;
	}
	
	/**
	 *
	 **/
	public void calculateScreenProjectionMatrix(float width, float height, float far, float near)
	{
		this.m00 = +2F / width;			this.m10 = 0;					this.m20 = 0;					this.m30 = -1;
		this.m01 = 0;					this.m11 = +2F / -height;		this.m21 = 0;					this.m31 = 1;
		this.m02 = 0;					this.m12 = 0;					this.m22 = -2F / (far-near);	this.m32 = (far+near) / (far-near);
		this.m03 = 0;					this.m13 = 0;					this.m23 = 0;					this.m33 = 1;
	}
	
	/**
	 *
	 **/
	public void calculateScreenProjectionMatrix(float width, float height)
	{
		this.calculateScreenProjectionMatrix(width, height, 1, -1);
	}
	
	/**
	 *
	 **/
	public float determinant()
	{
		float determinant = this.m00 * (((this.m11 * this.m22 * this.m33) + (this.m12 * this.m23 * this.m31) + (this.m13 * this.m21 * this.m32))
					- (this.m13 * this.m22 * this.m31)
					- (this.m11 * this.m23 * this.m32)
					- (this.m12 * this.m21 * this.m33));
		
		determinant -= this.m01 * (((this.m10 * this.m22 * this.m33) + (this.m12 * this.m23 * this.m30) + (this.m13 * this.m20 * this.m32))
				- (this.m13 * this.m22 * this.m30)
				- (this.m10 * this.m23 * this.m32)
				- (this.m12 * this.m20 * this.m33));
		
		determinant += this.m02 * (((this.m10 * this.m21 * this.m33) + (this.m11 * this.m23 * this.m30) + (this.m13 * this.m20 * this.m31))
				- (this.m13 * this.m21 * this.m30)
				- (this.m10 * this.m23 * this.m31)
				- (this.m11 * this.m20 * this.m33));
		
		determinant -= this.m03 * (((this.m10 * this.m21 * this.m32) + (this.m11 * this.m22 * this.m30) + (this.m12 * this.m20 * this.m31))
				- (this.m12 * this.m21 * this.m30)
				- (this.m10 * this.m22 * this.m31)
				- (this.m11 * this.m20 * this.m32));
		
		return determinant;
	}
	
	public void add(Mat4f mat)
	{
		Mat4f.add(this, mat, this);
	}
	
	public void add(Mat4f mat, Mat4f destination)
	{
		Mat4f.add(this, mat, destination);
	}
	
	public void sub(Mat4f mat)
	{
		Mat4f.sub(this, mat, this);
	}
	
	public void sub(Mat4f mat, Mat4f destination)
	{
		Mat4f.sub(this, mat, destination);
	}
	
	public void multiply(Mat4f mat)
	{
		Mat4f.mul(this, mat, this);
	}
	
	public void multiply(Mat4f mat, Mat4f destination)
	{
		Mat4f.mul(this, mat, destination);
	}
	
	public Mat4f translate(float x, float y, float z)
	{
		return Mat4f.translate(x, y, z, this, this);
	}
	
	public Mat4f translate(float x, float y, float z, Mat4f destination)
	{
		return Mat4f.translate(x, y, z, this, destination);
	}
	
	public Mat4f translate(Vec3f vector)
	{
		return Mat4f.translate(vector, this, this);
	}
	
	public Mat4f translate(Vec3f vector, Mat4f destination)
	{
		return Mat4f.translate(vector, this, destination);
	}
	
	public Mat4f translate(Vec4f vector)
	{
		return Mat4f.translate(vector, this, this);
	}
	
	public Mat4f translate(Vec4f vector, Mat4f destination)
	{
		return Mat4f.translate(vector, this, destination);
	}
	
	public Mat4f translate(Tuple3 vector)
	{
		return Mat4f.translate(vector.getXf(),vector.getYf(),vector.getZf(), this, this);
	}
	
	public Mat4f translate(Tuple3 vector, Mat4f destination)
	{
		return Mat4f.translate(vector.getXf(),vector.getYf(),vector.getZf(), this, destination);
	}
	
	public Mat4f scale(float scalex, float scaley, float scalez)
	{
		return Mat4f.scale(scalex, scaley, scalez, this, this);
	}
	
	public Mat4f scale(float scalex, float scaley, float scalez, Mat4f destination)
	{
		return Mat4f.scale(scalex, scaley, scalez, this, destination);
	}
	
	public Mat4f scale(Vec3f scaleVector)
	{
		return Mat4f.scale(scaleVector, this, this);
	}
	
	public Mat4f scale(Vec3f scaleVector, Mat4f destination)
	{
		return Mat4f.scale(scaleVector, this, destination);
	}
	
	/**
	 * @return 'destination' if this matrix could be inverted, null if matrix is not invertable.
	 **/
	public Mat4f invert()
	{
		return Mat4f.invert(this, this);
	}
	
	/**
	 * @return 'destination' if this matrix could be inverted, null if matrix is not invertable.
	 **/
	public Mat4f invert(Mat4f destination)
	{
		return Mat4f.invert(this, destination);
	}
	
	public Vec3f transform(Vec3f vec)
	{
		return Mat4f.transformVec3f(this, vec, vec);
	}
	
	public Vec3f transform(Vec3f vec, Vec3f destination)
	{
		return Mat4f.transformVec3f(this, vec, destination);
	}
	
	public Vec4f transform(Vec4f vec)
	{
		return Mat4f.transformVec4f(this, vec, vec);
	}
	
	public Vec4f transform(Vec4f vec, Vec4f destination)
	{
		return Mat4f.transformVec4f(this, vec, destination);
	}
	
	public Tuple3 transform(Tuple3 vec)
	{
		return Mat4f.transformTuple3(this, vec, vec);
	}
	
	public Tuple3 transform(Tuple3 vec, Tuple3 destination)
	{
		return Mat4f.transformTuple3(this, vec, destination);
	}
	
	public Tuple4 transform(Tuple4 vec)
	{
		return Mat4f.transformTuple4(this, vec, vec);
	}
	
	public Tuple4 transform(Tuple4 vec, Tuple4 destination)
	{
		return Mat4f.transformTuple4(this, vec, destination);
	}
	
	
	
	
	public Mat4f transpose()
	{
		Mat4f.transpose(this, this);
		return this;
	}
	
	public Mat4f transpose(Mat4f destination)
	{
		Mat4f.transpose(this, destination);
		return destination;
	}
	
	
	
	
	public Mat4f rotateAroundAxisByDegrees(float degree, Vec3f axisToRotateAround)
	{
		return Mat4f.rotateAroundAxisByDegrees(degree, axisToRotateAround, this, this);
	}
	
	public Mat4f rotateAroundAxisByDegrees(float degree, Vec3f axisToRotateAround, Mat4f destination)
	{
		return Mat4f.rotateAroundAxisByDegrees(degree, axisToRotateAround, this, destination);
	}
	
	public Mat4f rotateAroundAxisByDegrees(float degree, float axisX, float axisY, float axisZ)
	{
		return Mat4f.rotateAroundAxisByDegrees(degree, axisX, axisY, axisZ, this, this);
	}
	
	public Mat4f rotateAroundAxisByDegrees(float degree, float axisX, float axisY, float axisZ, Mat4f destination)
	{
		return Mat4f.rotateAroundAxisByDegrees(degree, axisX, axisY, axisZ, this, destination);
	}
	
	
	
	
	
	public Mat4f rotateAroundAxisByRadians(float degree, Vec3f axisToRotateAround)
	{
		return Mat4f.rotateAroundAxisByRadians(degree, axisToRotateAround, this, this);
	}
	
	public Mat4f rotateAroundAxisByRadians(float degree, Vec3f axisToRotateAround, Mat4f destination)
	{
		return Mat4f.rotateAroundAxisByRadians(degree, axisToRotateAround, this, destination);
	}
	
	public Mat4f rotateAroundAxisByRadians(float degree, float axisX, float axisY, float axisZ)
	{
		return Mat4f.rotateAroundAxisByRadians(degree, axisX, axisY, axisZ, this, this);
	}
	
	public Mat4f rotateAroundAxisByRadians(float degree, float axisX, float axisY, float axisZ, Mat4f destination)
	{
		return Mat4f.rotateAroundAxisByRadians(degree, axisX, axisY, axisZ, this, destination);
	}
	
	
	
}
