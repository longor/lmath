package de.longor.lmath.matrices;

import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;

public interface IMatrix
{
	
	public IMatrix setToZero();
	public IMatrix setToIdentity();
	public IMatrix setToFloatBuffer(FloatBuffer source);
	public IMatrix setToFloatBufferAndTranspose(FloatBuffer source);
	public IMatrix setToArray(float[] source);
	
	
	/**
	 * Puts all values in this matrix into the given array.
	 * @param out The array to put the matrix-data into.
	 * @param position The position in the array to put the matrix-data at.
	 * @throws IndexOutOfBoundsException If the array is not big enough or the position is out of bounds.
	 * @throws NullPointerException If the given array is null.
	 **/
	public void unwrapMatrixData(float[] out, int position);
	
	/**
	 * Puts all values in this matrix into the given array.
	 * @param out The array to put the matrix-data into.
	 * @param position The position in the array to put the matrix-data at.
	 * @throws IndexOutOfBoundsException If the array is not big enough or the position is out of bounds.
	 * @throws NullPointerException If the given array is null.
	 **/
	public void unwrapMatrixData(double[] out, int position);
	
	/**
	 * Puts all values in this matrix into the given buffer at the current position.
	 * @param buffer The buffer to put the matrix-data into.
	 * @throws NullPointerException If the given buffer is null.
	 * @return The given buffer, for convenience.
	 **/
	public FloatBuffer unwrapMatrixData(FloatBuffer buffer);
	
	/**
	 * Puts all values in this matrix into the given buffer at the current position.
	 * @param buffer The buffer to put the matrix-data into.
	 * @throws NullPointerException If the given buffer is null.
	 * @return The given buffer, for convenience.
	 **/
	public DoubleBuffer unwrapMatrixData(DoubleBuffer buffer);
	
}
