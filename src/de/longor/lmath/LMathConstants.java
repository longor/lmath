package de.longor.lmath;

import de.longor.lmath.tuples.Vec3f;

public class LMathConstants
{
	public static final Vec3f UP = new Vec3f(0,1,0);
	public static final Vec3f DOWN = new Vec3f(0,-1,0);
	public static final Vec3f ONEONEONEvec3f = new Vec3f(1,1,1);
	
	/** double : PI. */
	public static final double PI = java.lang.Math.PI;
	
	/** float : PI. */
	public static final float PIf = (float) java.lang.Math.PI;
	
	/** double : PI / 2. */
	public static final double HALF_PI = java.lang.Math.PI / 2D;
	
	/** float : PI / 2. */
	public static final float HALF_PIf = (float) (java.lang.Math.PI / 2D);
	
	/** double : PI * 2. */
	public static final double PI2 = PI * 2d;
	
	/** float : PI * 2. */
	public static final float PI2f = PIf * 2f;
	
	/**
	 * Multiply a number in degree by this number to get it as radiant-number.<br>
	 * Divide a radiant by this number to get a degree-number.
	 **/
	public static final double PI180 = 0.0174532925;
	
	/**
	 * Multiply a number in degree by this number to get it as radiant-number.<br>
	 * Divide a radiant by this number to get a degree-number.
	 **/
	public static final double PI180f = 0.0174532925f;
	
}
