package de.longor.lmath;

import java.text.NumberFormat;

/**
 * This class contains methods and objects to turn primitive numbers into strings in a memory-friendly way.
 * Note that this only works (and is thus only useful) if the Garbage-Collector
 * is capable of deleting string directly after they go out of method-scope.
 */
public class LMathStringHelper
{
	/**
	 * A simple NumberFormat instance to quickly format numbers.
	 **/
	public static final NumberFormat numberFormat;
	static
	{
		numberFormat = NumberFormat.getInstance();
		numberFormat.setGroupingUsed(false);
		numberFormat.setMaximumFractionDigits(2);
		numberFormat.setMinimumFractionDigits(2);
		numberFormat.setMaximumIntegerDigits(32);
		numberFormat.setMinimumIntegerDigits(1);
	}
	
	public static final String format(int value)
	{
		return numberFormat.format(value);
	}
	
	public static final String format(long value)
	{
		return numberFormat.format(value);
	}
	
	public static final String format(float value)
	{
		return numberFormat.format(value);
	}
	
	public static final String format(double value)
	{
		return numberFormat.format(value);
	}
	
    /**
     * Tries to generate a string for the given input into a character-array, without trashing memory.
     *
     * @param input the input
     * @param radix the radix
     * @param index the index
     * @param output the output
     * @return the int
     */
    public static final int toString(int input, int radix, int index, char[] output)
    {
    	// Get that string
    	String s = Integer.toString(input, radix);
    	
    	// Get the length
    	int sL = s.length();
    	
    	// Copy
    	for(int i = 0; i < sL; i++)
    		output[index++] = s.charAt(i);
    	
    	// NULL the String and hope that the GC instantly deletes it,
    	// since there are no more references to it.
    	s = null;
    	
    	return sL;
    }
	
    /**
     * Tries to generate a string for the given input into a character-array, without trashing memory.
     *
     * @param input the input
     * @param index the index
     * @param output the output
     * @return the int
     */
    public static final int toString(float input, int index, char[] output)
    {
    	// Get that string
    	String s = Float.toString(input);
    	
    	// Get the length
    	int sL = s.length();
    	
    	// Copy
    	for(int i = 0; i < sL; i++)
    		output[index++] = s.charAt(i);
    	
    	// NULL the String and hope that the GC instantly deletes it,
    	// since there are no more references to it.
    	s = null;
    	
    	return sL;
    }
	
    /**
     * Tries to generate a string for the given input into a character-array, without trashing memory.
     *
     * @param input the input
     * @param index the index
     * @param output the output
     * @return the int
     */
    public static final int toString(double input, int index, char[] output)
    {
    	// Get that string
    	String s = Double.toString(input);
    	
    	// Get the length
    	int sL = s.length();
    	
    	// Copy
    	for(int i = 0; i < sL; i++)
    		output[index++] = s.charAt(i);
    	
    	// NULL the String and hope that the GC instantly deletes it,
    	// since there are no more references to it.
    	s = null;
    	
    	return sL;
    }
}
