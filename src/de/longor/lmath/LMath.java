package de.longor.lmath;

import java.util.Random;

/**
 * This class contains Math, Math, Math and even more Math.
 * This is a utility class that can do useful math operations that can not be found in the java.lang.Math-class,
 * and it has a bunch of static fields to help with some things.
 * <hr>
 * THIS CLASS IS COMPLETELY STATIC FOR PERFORMANCE REASONS.
 */
public class LMath
{
	/** Static Utility Class. Constructor not needed, thus private. **/
	private LMath(){}
	
	/**
	 * In the case that you DO need an instance of this class (such in the case of scripting-systems),
	 * you can get one trough this method.
	 * <hr>
	 * Example: In the Jython scripting library,
	 * there is (apparently) no such thing like static method access,
	 * so you have to create instances of the objects you want to use the methods of.
	 * (May be wrong, but it appears that way)
	 **/
	public static final LMath helperInstance = new LMath();
	
	/**
	 * The point in time at which this class was loaded, in milliseconds.
	 **/
	public static final long INITTIME = System.currentTimeMillis();
	
	/**
	 * The point in time at which this class was loaded, in nanoseconds.
	 **/
	public static final long INITNANOTIME = System.nanoTime();
	
	public static final float TSCL()
	{
		return System.currentTimeMillis() - INITTIME;
	}
	
	/** double : PI. */
	public static final double PI = java.lang.Math.PI;
	
	/** float : PI. */
	public static final float PIf = (float) java.lang.Math.PI;
	
	/** double : PI * 2. */
	public static final double PI2 = PI * 2d;
	
	/** float : PI * 2. */
	public static final float PI2f = PIf * 2f;
	
	/**
	 * Multiply a number in degree by this number to get it as radiant-number.<br>
	 * Divide a radiant by this number to get a degree-number.
	 **/
	public static final double PI180 = 0.0174532925;
	
	/**
	 * Multiply a number in degree by this number to get it as radiant-number.<br>
	 * Divide a radiant by this number to get a degree-number.
	 **/
	public static final float PI180f = 0.0174532925f;
	
	/**
	 * Fast floor function.
	 * See {@code java.lang.Math.floor();}
	 * @param x The number to be floor'ed.
	 * @return The floor'ed number.
	 **/
	public static final int fastFloor(float x)
	{
		int xi = (int) x;
		return x < xi ? xi - 1 : xi;
	}
	
	/**
	 * Fast floor function.
	 * See {@code java.lang.Math.floor();}
	 * @param x The number to be floor'ed.
	 * @return The floor'ed number.
	 **/
	public static final int fastFloor(double x)
	{
		int xi = (int) x;
		return x < xi ? xi - 1 : xi;
	}
	
	/**
	 * Returns the fractional part of the given number by omitting the integer part.
	 * @param x The number to get the fractional part from.
	 * @return The fractional part of X.
	 **/
	public static final float fract(float x)
	{
		return x - fastFloor(x);
	}
	
	/**
	 * Returns the fractional part of the given number by omitting the integer part.
	 * @param x The number to get the fractional part from.
	 * @return The fractional part of X.
	 **/
	public static final double fract(double x)
	{
		return x - fastFloor(x);
	}
	
	/**
	 * Finds the next PowerOfTwo number for the given number.
	 * @param in The number to get the next PoT number for.
	 * @return The next biggest PoT.
	 **/
	public static final int nextPoT(int in)
	{
		int b = 1;
		
		while (b < in)
		{
			b = b << 1;
		}
		
		return b;
	}
	
	/**
	 * Finds the next PowerOfTwo number for the given number.
	 * @param fin The number to get the next PoT number for.
	 * @return The next biggest PoT.
	 **/
	public static final int nextPoT(float fin)
	{
		int in = fastFloor(fin);
		int b = 1;
		
		while (b < in)
		{
			b = b << 1;
		}
		
		return b;
	}
	
	/**
	 * Finds the next PowerOfTwo number for the given number.
	 * @param fin The number to get the next PoT number for.
	 * @return The next biggest PoT.
	 **/
	public static final int nextPoT(double fin)
	{
		int in = fastFloor(fin);
		int b = 1;
		
		while (b < in)
		{
			b = b << 1;
		}
		
		return b;
	}
	
	/**
	 * Returns the linear interpolation of A and B trough X.
	 * @param a The value A.
	 * @param b The value B.
	 * @param x The interpolation fraction X.
	 * @return The interpolated value.
	 **/
	public static final int lerp(int a, int b, float x)
	{
		return (int) (a + (x * (b - a)));
	}
	
	/**
	 * Returns the linear interpolation of A and B trough X.
	 * @param a The value A.
	 * @param b The value B.
	 * @param x The interpolation fraction X.
	 * @return The interpolated value.
	 **/
	public static final float lerp(float a, float b, float x)
	{
		return a + (x * (b - a));
	}
	
	/**
	 * Returns the linear interpolation of A and B trough X.
	 * @param a The value A.
	 * @param b The value B.
	 * @param x The interpolation fraction X.
	 * @return The interpolated value.
	 **/
	public static final double lerp(double a, double b, double x)
	{
		return a + (x * (b - a));
	}
	
	/**
	 * ???.
	 *
	 * @param a The value A.
	 * @param b The value B.
	 * @param x The fraction.
	 * @return The interpolated value.
	 */
	public static final float powlerp(float a, float b, float x)
	{
		return a + ((x*x) * (b - a));
	}
	
	/**
	 * Returns the trilinear interpolation of x/y/z.
	 * @param v000 ---
	 * @param v100 ---
	 * @param v010 ---
	 * @param v110 ---
	 * @param v001 ---
	 * @param v101 ---
	 * @param v011 ---
	 * @param v111 ---
	 * @param x The fraction on the x axis.
	 * @param y The fraction on the y axis.
	 * @param z The fraction on the z axis.
	 * @return The interpolated value.
	 **/
    public static final float trilerp(
    		float v000, float v100, float v010, float v110,
    		float v001, float v101, float v011, float v111,
    		float x, float y, float z)
    {
    	return(
    		(v000 * (1-x) * (1-y) * (1-z)) +
    		(v100 *  x    * (1-y) * (1-z)) +
    		(v010 * (1-x) *  y    * (1-z)) +
    		(v110 *  x    *  y    * (1-z)) +
    		(v001 * (1-x) * (1-y) *  z) +
    		(v101 *  x    * (1-y) *  z) +
    		(v011 * (1-x) *  y    *  z) +
    		(v111 *  x    *  y    *  z)
    	);
	}
	
	/**
	 * Returns the trilinear interpolation of x/y/z.
	 * @param v000 ---
	 * @param v100 ---
	 * @param v010 ---
	 * @param v110 ---
	 * @param v001 ---
	 * @param v101 ---
	 * @param v011 ---
	 * @param v111 ---
	 * @param x The fraction on the x axis.
	 * @param y The fraction on the y axis.
	 * @param z The fraction on the z axis.
	 * @return The interpolated value.
	 **/
    public static final double trilerp(
    		double v000, double v100, double v010, double v110,
    		double v001, double v101, double v011, double v111,
    		double x, double y, double z)
    {
    	return(
    		(v000 * (1-x) * (1-y) * (1-z)) +
    		(v100 *  x    * (1-y) * (1-z)) +
    		(v010 * (1-x) *  y    * (1-z)) +
    		(v110 *  x    *  y    * (1-z)) +
    		(v001 * (1-x) * (1-y) *  z) +
    		(v101 *  x    * (1-y) *  z) +
    		(v011 * (1-x) *  y    *  z) +
    		(v111 *  x    *  y    *  z)
    	);
	}
	
	/**
	 * Cosine interpolation of A and B trough X.
	 * @param a The value A.
	 * @param b The value B.
	 * @param x The interpolation fraction X.
	 * @return The interpolated value.
	 **/
	public static final float coslerp(float a, float b, float x)
	{
		float ft = x * PIf;
		float f = ((1f - (float)Math.cos(ft)) * 0.5f);
		return (a * (1 - f)) + (b * f);
	}
	
	/**
	 * Cosine interpolation of A and B trough X.
	 * @param a The value A.
	 * @param b The value B.
	 * @param x The interpolation fraction X.
	 * @return The interpolated value.
	 **/
	public static final double coslerp(double a, double b, double x)
	{
		double ft = x * PI;
		double f = ((1f - Math.cos(ft)) * 0.5f);
		return (a * (1 - f)) + (b * f);
	}
	
	/**
	 * Does a 'hermite interpolation' (also known as 'smoothstep' in GLSL).
	 * @param edge0 The edge A.
	 * @param edge1 The edge  B.
	 * @param x The fraction X.
	 * @return The interpolated value.
	 **/
	public static final float smoothstep(float edge0, float edge1, float x)
	{
	    float t = clamp(0f, 1f, (x - edge0) / (edge1 - edge0));
	    return t * t * (3f - 2f * t);
	}
	
	/**
	 * Does a 'hermite interpolation' (also known as 'smoothstep' in GLSL).
	 * @param edge0 The edge A.
	 * @param edge1 The edge  B.
	 * @param x The fraction X.
	 * @return The interpolated value.
	 **/
	public static final double smoothstep(double edge0, double edge1, double x)
	{
		double t = clamp(0f, 1f, (x - edge0) / (edge1 - edge0));
	    return t * t * (3f - 2f * t);
	}
	
	/**
	 * Clamps X so it is in between (inclusive) of MIN and MAX.
	 * @param min The minimum.
	 * @param max The maximum.
	 * @param x The number to clamp.
	 * @return The value X clamped into MIN and MAX.
	 **/
	public static final int clamp(int min, int max, int x)
	{
		return x < min ? min : (x > max ? max : x);
	}
	
	/**
	 * Clamps X so it is in between (inclusive) of MIN and MAX.
	 * @param min The minimum.
	 * @param max The maximum.
	 * @param x The number to clamp.
	 * @return The value X clamped into MIN and MAX.
	 **/
	public static final long clamp(long min, long max, long x)
	{
		return x < min ? min : (x > max ? max : x);
	}
	
	/**
	 * Clamps X so it is in between (inclusive) of MIN and MAX.
	 * @param min The minimum.
	 * @param max The maximum.
	 * @param x The number to clamp.
	 * @return The value X clamped into MIN and MAX.
	 **/
	public static final float clamp(float min, float max, float x)
	{
		return x < min ? min : (x > max ? max : x);
	}
	
	/**
	 * Clamps X so it is in between (inclusive) of MIN and MAX.
	 * @param min The minimum.
	 * @param max The maximum.
	 * @param x The number to clamp.
	 * @return The value X clamped into MIN and MAX.
	 **/
	public static final double clamp(double min, double max, double x)
	{
		return x < min ? min : (x > max ? max : x);
	}
	
    /**
     * Checks if the given variable X is within the given bounds MIN..MAX.
     * When it is return it, when not, return the number CLAMP.
	 * @param min The minimum.
	 * @param max The maximum.
	 * @param x The number to clamp.
	 * @param clamp The number to return if X is out of bounds.
	 * @return The value X if it is inside MIN and MAX, or CLAMP if not.
     **/
	public static final char clamp(char min, char max, char x, char clamp)
	{
		return x < min ? clamp : (x > max ? clamp : x);
	}
	
    /**
     * Checks if the given variable X is within the given bounds MIN..MAX.
     * When it is return it, when not, return the number CLAMP.
	 * @param min The minimum.
	 * @param max The maximum.
	 * @param x The number to clamp.
	 * @param clamp The number to return if X is out of bounds.
	 * @return The value X if it is inside MIN and MAX, or CLAMP if not.
     **/
	public static final int clamp(int min, int max, int x, int clamp)
	{
		return x < min ? clamp : (x > max ? clamp : x);
	}
	
    /**
     * Checks if the given variable X is within the given bounds MIN..MAX.
     * When it is return it, when not, return the number CLAMP.
	 * @param min The minimum.
	 * @param max The maximum.
	 * @param x The number to clamp.
	 * @param clamp The number to return if X is out of bounds.
	 * @return The value X if it is inside MIN and MAX, or CLAMP if not.
     **/
	public static final float clamp(float min, float max, float x, float clamp)
	{
		return x < min ? clamp : (x > max ? clamp : x);
	}
	
    /**
     * Checks if the given variable X is within the given bounds MIN..MAX.
     * When it is return it, when not, return the number CLAMP.
	 * @param min The minimum.
	 * @param max The maximum.
	 * @param x The number to clamp.
	 * @param clamp The number to return if X is out of bounds.
	 * @return The value X if it is inside MIN and MAX, or CLAMP if not.
     **/
	public static final double clamp(double min, double max, double x, double clamp)
	{
		return x < min ? clamp : (x > max ? clamp : x);
	}
	
	/**
	 * Rounds the given VALUE such as that its fraction can be evenly divided by SNAP.
	 * @param value The value to snap.
	 * @param snap The snap value.
	 * @return The value snapped to the given snap-value.
	 **/
	public static final float snap(float value, float snap)
	{
		return Math.round(value * snap) / snap;
	}
	
	/**
	 * Rounds the given VALUE such as that its fraction can be evenly divided by SNAP.
	 * @param value The value to snap.
	 * @param snap The snap value.
	 * @return The value snapped to the given snap-value.
	 **/
	public static final double snap(double value, double snap)
	{
		return Math.round(value * snap) / snap;
	}
	
	/**
	 * Returns the value that is in the middle of A and B.
	 * @param a First value.
	 * @param b First value.
	 * @return The median of A and B.
	 **/
	public static final float median(float a, float b)
	{
		return (a + b) / 2f;
	}
	
	/**
	 * Returns the value that is in the middle of A and B.
	 * @param a First value.
	 * @param b First value.
	 * @return The median of A and B.
	 **/
	public static final double median(double a, double b)
	{
		return (a + b) / 2d;
	}
	
	/**
	 * Returns the median of all the given numbers.
	 * @param fs All the numbers.
	 * @return The median of all the numbers.
	 **/
	public static final float median(float...fs)
	{
		float out = 0f;
		int len = fs.length;
		for(float f : fs)
			out += f;
		return out / (float)len;
	}
	
	/**
	 * Returns the median of all the given numbers.
	 * @param fs All the numbers.
	 * @return The median of all the numbers.
	 **/
	public static final double median(double...fs)
	{
		double out = 0f;
		int len = fs.length;
		for(double f : fs)
			out += f;
		return out / (double)len;
	}
	
	/**
	 * Checks if the given value X is in between MIN and MAX.
	 * @param min The minimum bounds.
	 * @param max The maximum bounds.
	 * @param x The value to check.
	 * @return True, if the given value is between MIN and MAX. False, if not.
	 **/
	public static final boolean inside(int min, int max, int x)
	{
		return x < min ? false : (x > max ? false : true);
	}
	
	/**
	 * Checks if the given value X is in between MIN and MAX.
	 * @param min The minimum bounds.
	 * @param max The maximum bounds.
	 * @param x The value to check.
	 * @return True, if the given value is between MIN and MAX. False, if not.
	 **/
	public static final boolean inside(float min, float max, float x)
	{
		return x < min ? false : (x > max ? false : true);
	}
	
	/**
	 * Checks if the given value X is in between MIN and MAX.
	 * @param min The minimum bounds.
	 * @param max The maximum bounds.
	 * @param x The value to check.
	 * @return True, if the given value is between MIN and MAX. False, if not.
	 **/
	public static final boolean inside(double min, double max, double x)
	{
		return x < min ? false : (x > max ? false : true);
	}
	
	/**
	 * Checks if the given vector XYZ is inside the cubic region MIN..MAX.
	 * @param minX Cubic Region Minimum X.
	 * @param minY Cubic Region Minimum Y.
	 * @param minZ Cubic Region Minimum Z.
	 * @param maxX Cubic Region Maximum X.
	 * @param maxY Cubic Region Maximum Y.
	 * @param maxZ Cubic Region Maximum Z.
	 * @param x Vector X.
	 * @param y Vector Y.
	 * @param z Vector Z.
	 * @return True, if the given vector is inside the given cubic region. False, if not.
	 **/
	public static final boolean inside(
			int minX, int minY, int minZ,
			int maxX, int maxY, int maxZ,
			int x, int y, int z)
	{
		return inside(minX, maxX, x) && inside(minY, maxY, y) && inside(minZ, maxZ, z);
	}
	
	/**
	 * Checks if the given vector XYZ is inside the cubic region MIN..MAX.
	 * @param minX Cubic Region Minimum X.
	 * @param minY Cubic Region Minimum Y.
	 * @param minZ Cubic Region Minimum Z.
	 * @param maxX Cubic Region Maximum X.
	 * @param maxY Cubic Region Maximum Y.
	 * @param maxZ Cubic Region Maximum Z.
	 * @param x Vector X.
	 * @param y Vector Y.
	 * @param z Vector Z.
	 * @return True, if the given vector is inside the given cubic region. False, if not.
	 **/
	public static final boolean inside(
			float minX, float minY, float minZ,
			float maxX, float maxY, float maxZ,
			float x, float y, float z)
	{
		return inside(minX, maxX, x) && inside(minY, maxY, y) && inside(minZ, maxZ, z);
	}
	
	/**
	 * Checks if the given vector XYZ is inside the cubic region MIN..MAX.
	 * @param minX Cubic Region Minimum X.
	 * @param minY Cubic Region Minimum Y.
	 * @param minZ Cubic Region Minimum Z.
	 * @param maxX Cubic Region Maximum X.
	 * @param maxY Cubic Region Maximum Y.
	 * @param maxZ Cubic Region Maximum Z.
	 * @param x Vector X.
	 * @param y Vector Y.
	 * @param z Vector Z.
	 * @return True, if the given vector is inside the given cubic region. False, if not.
	 **/
	public static final boolean inside(
			double minX, double minY, double minZ,
			double maxX, double maxY, double maxZ,
			double x, double y, double z)
	{
		return inside(minX, maxX, x) && inside(minY, maxY, y) && inside(minZ, maxZ, z);
	}
	
	/**
	 * Wraps a number X around between the given bounds MIN..MAX.
	 * <pre>
	 * WARNING:
	 * This method does what it is supposed to do in the WRONG way.
	 * The farther away X is out of the bounds MIN..MAX,
	 * the longer this method will take to complete,
	 * because it uses a while-loop internally to do the wrapping.
	 * Make absolutely sure that your number doesn't get too big.
	 * </pre>
	 * @param min The start(minimum) of the range.
	 * @param max The end(maximum) of the range.
	 * @param x The value X to wrap in between MIN and MAX.
	 * @return The wrapped value.
	 **/
	public static final float wrap(float min, float max, float x)
	{
		float len = max - min;
		while(x < min) x += len;
		while(x > max) x -= len;
		return x;
	}
	
	/**
	 * Returns the inverse squareroot for X: 1 / sqrt(x)<br>
	 * This method is MUCH faster than doing it the usual way, which is: {@code 1f / Math.sqrt(x)}.
	 * @param x The value to get the inverse squareroot for.
	 * @return The inverse squareroot: {@code 1 / sqrt(x)}
	 **/
	public static float invSqrt(float x)
	{
		float xhalf = 0.5f * x;
		int i = Float.floatToIntBits(x);
		i = 0x5f3759df - (i >> 1);
		x = Float.intBitsToFloat(i);
		x = x * (1.5f - xhalf * x * x);
		return x;
	}
	
	/**
	 * Returns the inverse squareroot for X: 1 / sqrt(x)<br>
	 * This method is MUCH faster than doing it the usual way, which is: {@code 1f / Math.sqrt(x)}.
	 * @param x The value to get the inverse squareroot for.
	 * @return The inverse squareroot: {@code 1 / sqrt(x)}
	 **/
	public static double invSqrt(double x)
	{
		double xhalf = 0.5d * x;
		long i = Double.doubleToLongBits(x);
		i = 0x5fe6ec85e7de30daL - (i >> 1);
		x = Double.longBitsToDouble(i);
		x = x * (1.5d - xhalf * x * x);
		return x;
	}
	
	/**
	 * Returns the length of the given vector XYZ.
	 * @param x Vector X
	 * @param y Vector Y
	 * @param z Vector Z
	 * @return The length of the given vector.
	 **/
	public static float length(float x, float y, float z)
	{
		return (float) Math.sqrt((x*x) + (y*y) + (z*z));
	}
	
	/**
	 * Returns the length of the given vector XYZ.
	 * @param x Vector X
	 * @param y Vector Y
	 * @param z Vector Z
	 * @return The length of the given vector.
	 **/
	public static double length(double x, double y, double z)
	{
		return Math.sqrt((x*x) + (y*y) + (z*z));
	}
	
	/**
	 * Returns the inverse length of the given vector XYZ, using the inverse squareroot function.
	 * @param x Vector X
	 * @param y Vector Y
	 * @param z Vector Z
	 * @return The inverse length of the given vector.
	 **/
	public static float invlength(float x, float y, float z)
	{
		return invSqrt((x*x) + (y*y) + (z*z));
	}
	
	/**
	 * Returns the inverse length of the given vector XYZ, using the inverse squareroot function.
	 * @param x Vector X
	 * @param y Vector Y
	 * @param z Vector Z
	 * @return The inverse length of the given vector.
	 **/
	public static double invlength(double x, double y, double z)
	{
		return invSqrt((x*x) + (y*y) + (z*z));
	}
	
	/**
	 * Returns the distance between the vectors A and B.
	 * @param x0 Vector A.x;
	 * @param y0 Vector A.y;
	 * @param x1 Vector B.x;
	 * @param y1 Vector B.y;
	 * @return The distance between A and B.
	 **/
	public static float distanceSquared(float x0, float y0, float x1, float y1)
	{
		float x2 = x1 - x0;
		float y2 = y1 - y0;
		return x2*x2 + y2*y2;
	}
	
	/**
	 * Returns the distance between the vectors A and B.
	 * @param x0 Vector A.x;
	 * @param y0 Vector A.y;
	 * @param x1 Vector B.x;
	 * @param y1 Vector B.y;
	 * @return The distance between A and B.
	 **/
	public static double distanceSquared(double x0, double y0, double x1, double y1)
	{
		double x2 = x1 - x0;
		double y2 = y1 - y0;
		return x2*x2 + y2*y2;
	}
	
	/**
	 * Returns the distance between the vectors A and B.
	 * @param x0 Vector A.x;
	 * @param y0 Vector A.y;
	 * @param x1 Vector B.x;
	 * @param y1 Vector B.y;
	 * @return The distance between A and B.
	 **/
	public static float distance(float x0, float y0, float x1, float y1)
	{
		float x2 = x1 - x0;
		float y2 = y1 - y0;
		return (float) Math.sqrt(x2*x2 + y2*y2);
	}
	
	/**
	 * Returns the distance between the vectors A and B.
	 * @param x0 Vector A.x;
	 * @param y0 Vector A.y;
	 * @param x1 Vector B.x;
	 * @param y1 Vector B.y;
	 * @return The distance between A and B.
	 **/
	public static double distance(double x0, double y0, double x1, double y1)
	{
		double x2 = x1 - x0;
		double y2 = y1 - y0;
		return Math.sqrt(x2*x2 + y2*y2);
	}
	
	/**
	 * Returns the distance between the vectors A and B.
	 * @param x0 Vector A.x;
	 * @param y0 Vector A.y;
	 * @param z0 Vector A.z;
	 * @param x1 Vector B.x;
	 * @param y1 Vector B.y;
	 * @param z1 Vector B.z;
	 * @return The distance between A and B.
	 **/
	public static float distance(float x0, float y0, float z0, float x1, float y1, float z1)
	{
		float x2 = x1 - x0;
		float y2 = y1 - y0;
		float z2 = z1 - z0;
		return (float) Math.sqrt(x2*x2 + y2*y2 + z2*z2);
	}
	
	/**
	 * Returns the distance between the vectors A and B.
	 * @param x0 Vector A.x;
	 * @param y0 Vector A.y;
	 * @param z0 Vector A.z;
	 * @param x1 Vector B.x;
	 * @param y1 Vector B.y;
	 * @param z1 Vector B.z;
	 * @return The distance between A and B.
	 **/
	public static double distance(double x0, double y0, double z0, double x1, double y1, double z1)
	{
		double x2 = x1 - x0;
		double y2 = y1 - y0;
		double z2 = z1 - z0;
		return Math.sqrt(x2*x2 + y2*y2 + z2*z2);
	}
	
	/**
	 * Returns the inverse distance ({@code 1 / distance(A,B)}) between the vectors A and B.
	 * @param x0 Vector A.x;
	 * @param y0 Vector A.y;
	 * @param z0 Vector A.z;
	 * @param x1 Vector B.x;
	 * @param y1 Vector B.y;
	 * @param z1 Vector B.z;
	 * @return The inverse distance between A and B.
	 **/
	public static float invdistance(float x0, float y0, float z0, float x1, float y1, float z1)
	{
		float x2 = x1 - x0;
		float y2 = y1 - y0;
		float z2 = z1 - z0;
		return invSqrt(x2*x2 + y2*y2 + z2*z2);
	}
	
	/**
	 * Returns the inverse distance ({@code 1 / distance(A,B)}) between the vectors A and B.
	 * @param x0 Vector A.x;
	 * @param y0 Vector A.y;
	 * @param z0 Vector A.z;
	 * @param x1 Vector B.x;
	 * @param y1 Vector B.y;
	 * @param z1 Vector B.z;
	 * @return The inverse distance between A and B.
	 **/
	public static double invdistance(double x0, double y0, double z0, double x1, double y1, double z1)
	{
		double x2 = x1 - x0;
		double y2 = y1 - y0;
		double z2 = z1 - z0;
		return invSqrt(x2*x2 + y2*y2 + z2*z2);
	}
    
	/**
	 * Calculates the 'd'-variable of the Cartesian plane-equation: {@code a*x + b*y + c*z + d = 0}.
	 *
	 * @param Px (The X-part of) The point P that lies on the plane.
	 * @param Py (The Y-part of) The point P that lies on the plane.
	 * @param Pz (The Z-part of) The point P that lies on the plane.
	 * @param Nx (The X-part of) The surface-normal/direction N of the Plane.
	 * @param Ny (The X-part of) The surface-normal/direction N of the Plane.
	 * @param Nz (The X-part of) The surface-normal/direction N of the Plane.
	 * @return The 'd'-variable of the plane-equation.
	 */
	public static float cartesianPlaneEquation(float Px, float Py, float Pz,float Nx, float Ny, float Nz)
	{
		return -((Px*Nx) + (Py*Ny) + (Pz*Nz));
	}
	
	/** The Constant random. */
	public static final Random random;
	static
	{
		random = new Random();
		random.nextBytes(new byte[100]); // warm up
	}
	
	/**
	 * Returns a random number in the range of MIN..MAX.
	 * @param min The minimum of the range.
	 * @param max The maximum of the range.
	 * @return A random number between MIN and MAX.
	 **/
	public static final int randomInt(int min, int max)
	{
		int diff = max - min;
		int rand = random.nextInt(diff);
		return min + rand;
	}
	
	/**
	 * Returns a random number in the range of MIN..MAX.
	 * @param min The minimum of the range.
	 * @param max The maximum of the range.
	 * @return A random number between MIN and MAX.
	 **/
	public static final long randomLong(long min, long max)
	{
		long diff = max - min;
		long rand = random.nextLong() % diff;
		return min + rand;
	}
	
	/**
	 * Returns a random number in the range of MIN..MAX.
	 * @param min The minimum of the range.
	 * @param max The maximum of the range.
	 * @return A random number between MIN and MAX.
	 **/
	public static final float randomFloat(float min, float max)
	{
		float diff = max - min;
		float rand = random.nextFloat();
		return min + (rand * diff);
	}
	
	/**
	 * Returns a random number in the range of MIN..MAX.
	 * @param min The minimum of the range.
	 * @param max The maximum of the range.
	 * @return A random number between MIN and MAX.
	 **/
	public static final double randomDouble(double min, double max)
	{
		double diff = max - min;
		double rand = random.nextDouble();
		return min + (rand * diff);
	}
	
	/**
	 * Returns a random number between -1 and +1.
	 * @return A random number between -1 and +1.
	 **/
	public static final float randomCenteredFloat()
	{
		return (random.nextFloat() * 2f - 1f);
	}
	
	/**
	 * Returns a random number between -1 and +1.
	 * @return A random number between -1 and +1.
	 **/
	public static final double randomCenteredDouble()
	{
		return (random.nextDouble() * 2f - 1f);
	}
	
	/**
	 * Returns a random number between -1 and +1 multiplied by the given range.
	 * @param range The range scalar to multiply the random number by.
	 * @return A random number between -1 and +1 multiplied by the given range.
	 **/
	public static final float randomCenteredFloat(float range)
	{
		return (random.nextFloat() * 2f - 1f) * range;
	}
	
	/**
	 * Returns a random number between -1 and +1 multiplied by the given range.
	 * @param range The range scalar to multiply the random number by.
	 * @return A random number between -1 and +1 multiplied by the given range.
	 **/
	public static final double randomCenteredDouble(double range)
	{
		return (random.nextDouble() * 2f - 1f) * range;
	}
	
	public static final int signum(int i)
	{
		if(i < 0)
			return -1;
		if(i > 0)
			return +1;
		return 0;
	}
	
}
