package de.longor.lmath.values;

/**
 * A float wrapped into a object that is mutable.
 */
public class MutableFloat implements IMutableNumber
{
	
	/** The data. */
	public float data;
	
	/**
	 * Instantiates a new mutable float.
	 */
	public MutableFloat()
	{
		data = 0;
	}
	
	/**
	 * Instantiates a new mutable float.
	 *
	 * @param value the value
	 */
	public MutableFloat(float value)
	{
		data = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.values.IMutableNumber#set(byte)
	 */
	@Override
	public void set(byte value) {
		data = value;
	}

	/* (non-Javadoc)
	 * @see de.longor.lmath.values.IMutableNumber#set(short)
	 */
	@Override
	public void set(short value) {
		data = value;
	}

	/* (non-Javadoc)
	 * @see de.longor.lmath.values.IMutableNumber#set(char)
	 */
	@Override
	public void set(char value) {
		data = value;
	}

	/* (non-Javadoc)
	 * @see de.longor.lmath.values.IMutableNumber#set(int)
	 */
	@Override
	public void set(int value) {
		data = value;
	}

	/* (non-Javadoc)
	 * @see de.longor.lmath.values.IMutableNumber#set(float)
	 */
	@Override
	public void set(float value) {
		data = value;
	}

	/* (non-Javadoc)
	 * @see de.longor.lmath.values.IMutableNumber#set(long)
	 */
	@Override
	public void set(long value) {
		data = (int) value;
	}

	/* (non-Javadoc)
	 * @see de.longor.lmath.values.IMutableNumber#set(double)
	 */
	@Override
	public void set(double value) {
		data = (int) value;
	}

	/* (non-Javadoc)
	 * @see de.longor.lmath.values.IMutableNumber#asByte()
	 */
	@Override
	public byte asByte() {
		return (byte) data;
	}

	/* (non-Javadoc)
	 * @see de.longor.lmath.values.IMutableNumber#asShort()
	 */
	@Override
	public short asShort() {
		return (short) data;
	}

	/* (non-Javadoc)
	 * @see de.longor.lmath.values.IMutableNumber#asChar()
	 */
	@Override
	public char asChar() {
		return (char) data;
	}

	/* (non-Javadoc)
	 * @see de.longor.lmath.values.IMutableNumber#asInt()
	 */
	@Override
	public int asInt() {
		return (int) data;
	}

	/* (non-Javadoc)
	 * @see de.longor.lmath.values.IMutableNumber#asFloat()
	 */
	@Override
	public float asFloat() {
		return data;
	}

	/* (non-Javadoc)
	 * @see de.longor.lmath.values.IMutableNumber#asLong()
	 */
	@Override
	public long asLong() {
		return (long) data;
	}

	/* (non-Javadoc)
	 * @see de.longor.lmath.values.IMutableNumber#asDouble()
	 */
	@Override
	public double asDouble() {
		return data;
	}

}
