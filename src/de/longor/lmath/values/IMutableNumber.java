package de.longor.lmath.values;

/**
 * A interface representing a single mutable primitive unit of a data-type.
 */
public interface IMutableNumber
{
	
	/**
	 * Sets the.
	 *
	 * @param value the value
	 */
	public void set(byte value);
	
	/**
	 * Sets the.
	 *
	 * @param value the value
	 */
	public void set(short value);
	
	/**
	 * Sets the.
	 *
	 * @param value the value
	 */
	public void set(char value);
	
	/**
	 * Sets the.
	 *
	 * @param value the value
	 */
	public void set(int value);
	
	/**
	 * Sets the.
	 *
	 * @param value the value
	 */
	public void set(float value);
	
	/**
	 * Sets the.
	 *
	 * @param value the value
	 */
	public void set(long value);
	
	/**
	 * Sets the.
	 *
	 * @param value the value
	 */
	public void set(double value);
	
	/**
	 * As byte.
	 *
	 * @return the byte
	 */
	public byte asByte();
	
	/**
	 * As short.
	 *
	 * @return the short
	 */
	public short asShort();
	
	/**
	 * As char.
	 *
	 * @return the char
	 */
	public char asChar();
	
	/**
	 * As int.
	 *
	 * @return the int
	 */
	public int asInt();
	
	/**
	 * As float.
	 *
	 * @return the float
	 */
	public float asFloat();
	
	/**
	 * As long.
	 *
	 * @return the long
	 */
	public long asLong();
	
	/**
	 * As double.
	 *
	 * @return the double
	 */
	public double asDouble();
}
