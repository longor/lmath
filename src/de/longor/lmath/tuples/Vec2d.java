package de.longor.lmath.tuples;

import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/**
 * A class representing a 2-component (double precision floating-point) vector.
 */
public class Vec2d implements Tuple2
{
	
	/** The x. */
	public double x;
	
	/** The y. */
	public double y;
	
	/**
	 * Instantiates a new vec2d.
	 */
	public Vec2d()
	{
		x = 0;
		y = 0;
	}
	
	/**
	 * Instantiates a new vec2d.
	 *
	 * @param XY the xy
	 */
	public Vec2d(int XY)
	{
		x = XY;
		y = XY;
	}
	
	/**
	 * Instantiates a new vec2d.
	 *
	 * @param X the x
	 * @param Y the y
	 */
	public Vec2d(int X, int Y)
	{
		x = X;
		y = Y;
	}
	
	/**
	 * Instantiates a new vec2d.
	 *
	 * @param XY the xy
	 */
	public Vec2d(float XY)
	{
		x = XY;
		y = XY;
	}
	
	/**
	 * Instantiates a new vec2d.
	 *
	 * @param X the x
	 * @param Y the y
	 */
	public Vec2d(float X, float Y)
	{
		x = X;
		y = Y;
	}
	
	/**
	 * Instantiates a new vec2d.
	 *
	 * @param XY the xy
	 */
	public Vec2d(double XY)
	{
		x = XY;
		y = XY;
	}
	
	/**
	 * Instantiates a new vec2d.
	 *
	 * @param X the x
	 * @param Y the y
	 */
	public Vec2d(double X, double Y)
	{
		x = X;
		y = Y;
	}
	
	/**
	 * Instantiates a new vec2d.
	 *
	 * @param vec the vec
	 */
	public Vec2d(Vec2d vec)
	{
		x = vec.x;
		y = vec.y;
	}
	
	/**
	 * Instantiates a new vec2d.
	 *
	 * @param vec the vec
	 */
	public Vec2d(Vec3f vec)
	{
		x = vec.x;
		y = vec.y;
	}
	
	/**
	 * Instantiates a new vec2d.
	 *
	 * @param vec the vec
	 */
	public Vec2d(Vec4f vec)
	{
		x = vec.x;
		y = vec.y;
	}
	
	/**
	 * Instantiates a new vec2d.
	 *
	 * @param vec the vec
	 */
	public Vec2d(Vec2f vec)
	{
		x = vec.x;
		y = vec.y;
	}
	
	/**
	 * Instantiates a new vec2d.
	 *
	 * @param vec the vec
	 */
	public Vec2d(Vec3d vec)
	{
		x = vec.x;
		y = vec.y;
	}
	
	/**
	 * Instantiates a new vec2d.
	 *
	 * @param vec the vec
	 */
	public Vec2d(Vec4d vec)
	{
		x = vec.x;
		y = vec.y;
	}
	
	/**
	 * Instantiates a new vec2d.
	 *
	 * @param tuple the tuple
	 */
	public Vec2d(Tuple2 tuple)
	{
		x = tuple.getXd();
		y = tuple.getYf();
	}
	
	/**
	 * Instantiates a new vec2d.
	 *
	 * @param tuple the tuple
	 */
	public Vec2d(Tuple3 tuple)
	{
		x = tuple.getXd();
		y = tuple.getYf();
	}
	
	/**
	 * Instantiates a new vec2d.
	 *
	 * @param tuple the tuple
	 */
	public Vec2d(Tuple4 tuple)
	{
		x = tuple.getXd();
		y = tuple.getYf();
	}
	
	/**
	 * Instantiates a new vec2d.
	 *
	 * @param buffer the buffer
	 */
	public Vec2d(IntBuffer buffer)
	{
		x = buffer.get(0);
		y = buffer.get(1);
	}
	
	/**
	 * Instantiates a new vec2d.
	 *
	 * @param buffer the buffer
	 */
	public Vec2d(FloatBuffer buffer)
	{
		x = buffer.get(0);
		y = buffer.get(1);
	}
	
	/**
	 * Instantiates a new vec2d.
	 *
	 * @param buffer the buffer
	 */
	public Vec2d(DoubleBuffer buffer)
	{
		x = buffer.get(0);
		y = buffer.get(1);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getXf()
	 */
	@Override
	public float getXf() {
		return (float) x;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getXd()
	 */
	@Override
	public double getXd() {
		return x;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getYf()
	 */
	@Override
	public float getYf() {
		return (float) y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getYd()
	 */
	@Override
	public double getYd() {
		return y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setX(float)
	 */
	@Override
	public void setX(float value) {
		x = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setX(double)
	 */
	@Override
	public void setX(double value) {
		x = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setY(float)
	 */
	@Override
	public void setY(float value) {
		y = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setY(double)
	 */
	@Override
	public void setY(double value) {
		y = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(float, float)
	 */
	@Override
	public void setXY(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(double, double)
	 */
	@Override
	public void setXY(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(float)
	 */
	@Override
	public void setXY(float xy) {
		this.x = xy;
		this.y = xy;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(double)
	 */
	@Override
	public void setXY(double xy) {
		this.x = xy;
		this.y = xy;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(de.longor.lmath.tuples.Tuple2)
	 */
	@Override
	public void setXY(Tuple2 tuple) {
		this.x = tuple.getXd();
		this.y = tuple.getYf();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(de.longor.lmath.tuples.Tuple3)
	 */
	@Override
	public void setXY(Tuple3 tuple) {
		this.x = tuple.getXd();
		this.y = tuple.getYf();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(de.longor.lmath.tuples.Tuple4)
	 */
	@Override
	public void setXY(Tuple4 tuple) {
		this.x = tuple.getXd();
		this.y = tuple.getYf();
	}
	
	@Override
	public String toString()
	{
		return "vec2d[" + x + ", " + y + "]";
	}
	
	/* Default Static OUT=IN-OP-IN Method
	public static final Vec2f add(Vec2f ain, Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2d
	 */
	public static final Vec2d add(Vec2d ain, Vec2d bin, Vec2d out)
	{
		out.x = ain.x + bin.x;
		out.y = ain.y + bin.y;
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2d
	 */
	public static final Vec2d sub(Vec2d ain, Vec2d bin, Vec2d out)
	{
		out.x = ain.x - bin.x;
		out.y = ain.y - bin.y;
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2d
	 */
	public static final Vec2d mul(Vec2d ain, Vec2d bin, Vec2d out)
	{
		out.x = ain.x * bin.x;
		out.y = ain.y * bin.y;
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2d
	 */
	public static final Vec2d div(Vec2d ain, Vec2d bin, Vec2d out)
	{
		out.x = ain.x / bin.x;
		out.y = ain.y / bin.y;
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2d
	 */
	public static final Vec2d mod(Vec2d ain, Vec2d bin, Vec2d out)
	{
		out.x = ain.x % bin.x;
		out.y = ain.y % bin.y;
		return out;
	}
	
	/**
	 * Distance.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @return the double
	 */
	public static final double distance(Vec2d ain, Vec2d bin)
	{
		double offx = bin.x - ain.x;
		double offy = bin.y - ain.y;
		return Math.sqrt(offx*offx + offy*offy);
	}
	
	/**
	 * Lerp.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param x the x
	 * @param out the out
	 * @return the vec2d
	 */
	public static final Vec2d lerp(Vec2d ain, Vec2d bin, double x, Vec2d out)
	{
		out.x = ain.x + (x * (bin.x - ain.x));
		out.y = ain.y + (x * (bin.y - ain.y));
		return out;
	}
	
	/* Default OUT=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2d
	 */
	public final Vec2d add(Vec2d bin, Vec2d out)
	{
		out.x = this.x + bin.x;
		out.y = this.y + bin.y;
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2d
	 */
	public final Vec2d sub(Vec2d bin, Vec2d out)
	{
		out.x = this.x - bin.x;
		out.y = this.y - bin.y;
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2d
	 */
	public final Vec2d mul(Vec2d bin, Vec2d out)
	{
		out.x = this.x * bin.x;
		out.y = this.y * bin.y;
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2d
	 */
	public final Vec2d div(Vec2d bin, Vec2d out)
	{
		out.x = this.x / bin.x;
		out.y = this.y / bin.y;
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2d
	 */
	public final Vec2d mod(Vec2d bin, Vec2d out)
	{
		out.x = this.x % bin.x;
		out.y = this.y % bin.y;
		return out;
	}
	
	/**
	 * Distance.
	 *
	 * @param bin the bin
	 * @return the double
	 */
	public final double distance(Vec2d bin)
	{
		double offx = this.x - bin.x;
		double offy = this.y - bin.y;
		return Math.sqrt(offx*offx + offy*offy);
	}
	
	/**
	 * Lerp.
	 *
	 * @param bin the bin
	 * @param x the x
	 * @param out the out
	 * @return the vec2d
	 */
	public final Vec2d lerp(Vec2d bin, double x, Vec2d out)
	{
		out.x = this.x + (x * (bin.x - this.x));
		out.y = this.y + (x * (bin.x - this.y));
		return out;
	}
	
	/* Default THIS=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @return the vec2d
	 */
	public final Vec2d add(Vec2d bin)
	{
		this.x = this.x + bin.x;
		this.y = this.y + bin.y;
		return this;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @return the vec2d
	 */
	public final Vec2d sub(Vec2d bin)
	{
		this.x = this.x - bin.x;
		this.y = this.y - bin.y;
		return this;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @return the vec2d
	 */
	public final Vec2d mul(Vec2d bin)
	{
		this.x = this.x * bin.x;
		this.y = this.y * bin.y;
		return this;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @return the vec2d
	 */
	public final Vec2d div(Vec2d bin)
	{
		this.x = this.x / bin.x;
		this.y = this.y / bin.y;
		return this;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @return the vec2d
	 */
	public final Vec2d mod(Vec2d bin)
	{
		this.x = this.x % bin.x;
		this.y = this.y % bin.y;
		return this;
	}
	
	/* Default THIS=THIS-OP-THIS Method
	public final Vec2f add(Vec2f bin)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @return the vec2d
	 */
	public final Vec2d add()
	{
		this.x = this.x + this.x;
		this.y = this.y + this.y;
		return this;
	}
	
	/**
	 * Sub.
	 *
	 * @return the vec2d
	 */
	public final Vec2d sub()
	{
		this.x = this.x - this.x;
		this.y = this.y - this.y;
		return this;
	}
	
	/**
	 * Mul.
	 *
	 * @return the vec2d
	 */
	public final Vec2d mul()
	{
		this.x = this.x * this.x;
		this.y = this.y * this.y;
		return this;
	}
	
	/**
	 * Div.
	 *
	 * @return the vec2d
	 */
	public final Vec2d div()
	{
		this.x = this.x / this.x;
		this.y = this.y / this.y;
		return this;
	}
	
	/**
	 * Mod.
	 *
	 * @return the vec2d
	 */
	public final Vec2d mod()
	{
		this.x = this.x % this.x;
		this.y = this.y % this.y;
		return this;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	/* Default Static OUT=IN-OP-IN Method
	public static final Vec2f add(Vec2f ain, Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2d
	 */
	public static final Vec2d add(Vec2d ain, Tuple2 bin, Vec2d out)
	{
		out.x = ain.x + bin.getXf();
		out.y = ain.y + bin.getYf();
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2d
	 */
	public static final Vec2d sub(Vec2d ain, Tuple2 bin, Vec2d out)
	{
		out.x = ain.x - bin.getXf();
		out.y = ain.y - bin.getYf();
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2d
	 */
	public static final Vec2d mul(Vec2d ain, Tuple2 bin, Vec2d out)
	{
		out.x = ain.x * bin.getXf();
		out.y = ain.y * bin.getYf();
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2d
	 */
	public static final Vec2d div(Vec2d ain, Tuple2 bin, Vec2d out)
	{
		out.x = ain.x / bin.getXf();
		out.y = ain.y / bin.getYf();
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2d
	 */
	public static final Vec2d mod(Vec2d ain, Tuple2 bin, Vec2d out)
	{
		out.x = ain.x % bin.getXf();
		out.y = ain.y % bin.getYf();
		return out;
	}
	
	/**
	 * Distance.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @return the double
	 */
	public static final double distance(Vec2d ain, Tuple2 bin)
	{
		double offx = bin.getXd() - ain.x;
		double offy = bin.getYf() - ain.y;
		return Math.sqrt(offx*offx + offy*offy);
	}
	
	/**
	 * Lerp.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param x the x
	 * @param out the out
	 * @return the vec2d
	 */
	public static final Vec2d lerp(Vec2d ain, Tuple2 bin, float x, Vec2d out)
	{
		out.x = ain.x + (x * (bin.getXd() - ain.x));
		out.y = ain.y + (x * (bin.getYf() - ain.y));
		return out;
	}
	
	/* Default OUT=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2d
	 */
	public final Vec2d add(Tuple2 bin, Vec2d out)
	{
		out.x = this.x + bin.getXf();
		out.y = this.y + bin.getYf();
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2d
	 */
	public final Vec2d sub(Tuple2 bin, Vec2d out)
	{
		out.x = this.x - bin.getXf();
		out.y = this.y - bin.getYf();
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2d
	 */
	public final Vec2d mul(Tuple2 bin, Vec2d out)
	{
		out.x = this.x * bin.getXf();
		out.y = this.y * bin.getYf();
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2d
	 */
	public final Vec2d div(Tuple2 bin, Vec2d out)
	{
		out.x = this.x / bin.getXf();
		out.y = this.y / bin.getYf();
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2d
	 */
	public final Vec2d mod(Tuple2 bin, Vec2d out)
	{
		out.x = this.x % bin.getXf();
		out.y = this.y % bin.getYf();
		return out;
	}
	
	/**
	 * Distance.
	 *
	 * @param bin the bin
	 * @return the double
	 */
	public final double distance(Tuple2 bin)
	{
		double offx = this.x - bin.getXd();
		double offy = this.y - bin.getYf();
		return Math.sqrt(offx*offx + offy*offy);
	}
	
	/* Default THIS=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @return the vec2d
	 */
	public final Vec2d add(Tuple2 bin)
	{
		this.x = this.x + bin.getXf();
		this.y = this.y + bin.getYf();
		return this;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @return the vec2d
	 */
	public final Vec2d sub(Tuple2 bin)
	{
		this.x = this.x - bin.getXf();
		this.y = this.y - bin.getYf();
		return this;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @return the vec2d
	 */
	public final Vec2d mul(Tuple2 bin)
	{
		this.x = this.x * bin.getXf();
		this.y = this.y * bin.getYf();
		return this;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @return the vec2d
	 */
	public final Vec2d div(Tuple2 bin)
	{
		this.x = this.x / bin.getXf();
		this.y = this.y / bin.getYf();
		return this;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @return the vec2d
	 */
	public final Vec2d mod(Tuple2 bin)
	{
		this.x = this.x % bin.getXf();
		this.y = this.y % bin.getYf();
		return this;
	}
	
	public final void normalize()
	{
		double len = Math.sqrt((x*x) + (y*y));
		x /= len;
		y /= len;
	}
	
	public final double length()
	{
		return Math.sqrt((x*x) + (y*y));
	}
	
	/**
	 * Returns the dot-product of this vector with itself.
	 * @return the dot product of this vector with itself.
	 **/
	public final double dotproduct()
	{
		return (this.x*this.x) + (this.y*this.y);
	}
	
	/**
	 * Returns the dot-product of this vector with the given one.
	 * @param v The vector to create a dot-product with this one from.
	 * @return the dot product of this vector with the given one.
	 **/
	public final double dotproduct(Vec2f v)
	{
		return (this.x*v.x) + (this.y*v.y);
	}
	
	/**
	 * Returns the dot-product of this vector with the given one.
	 * @param v The vector to create a dot-product with this one from.
	 * @return the dot product of this vector with the given one.
	 **/
	public final double dotproduct(Vec2d v)
	{
		return (this.x*v.x) + (this.y*v.y);
	}
	
}
