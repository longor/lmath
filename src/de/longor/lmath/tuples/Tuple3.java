package de.longor.lmath.tuples;

/**
 * A 3-component Tuple.
 * A Tuple always only holds data, and as such this interface only defines data access.
 *
 * @author Longor1996
 **/
public interface Tuple3 extends Tuple2
{
	/**
	 * Returns the z-component of this tuple as float.
	 * @return The z-component of this tuple as float.
	 **/
	public float getZf();
	
	/**
	 * Returns the z-component of this tuple as double.
	 * @return The z-component of this tuple as double.
	 **/
	public double getZd();
	
	
	
	
	/**
	 * Sets the z-component of this tuple to the given value.
	 * @param value The given value.
	 **/
	public void setZ(float value);
	
	/**
	 * Sets the z-component of this tuple to the given value.
	 * @param value The given value.
	 **/
	public void setZ(double value);
	
	/**
	 * Sets the x-, y- and z-components of this tuple to the given values.
	 * @param X The given value for the x-component.
	 * @param Y The given value for the y-component.
	 * @param Z The given value for the z-component.
	 **/
	public void setXYZ(float X, float Y, float Z);
	
	/**
	 * Sets the x-, y- and z-components of this tuple to the given values.
	 * @param X The given value for the x-component.
	 * @param Y The given value for the y-component.
	 * @param Z The given value for the z-component.
	 **/
	public void setXYZ(double X, double Y, double Z);
	
	/**
	 * Sets the x-, y- and z-components of this tuple to the given value.
	 * @param XYZ The given value.
	 **/
	public void setXYZ(float XYZ);
	
	/**
	 * Sets the x-, y- and z-components of this tuple to the given value.
	 * @param XYZ The given value.
	 **/
	public void setXYZ(double XYZ);
	
	/**
	 * Copies the X,Y and Z values of the given tuple into this Tuple3.
	 * @param tuple The tuple to copy the values from.
	 **/
	public void setXYZ(Tuple3 tuple);
	
	/**
	 * Copies the X,Y and Z values of the given tuple into this Tuple3.
	 * @param tuple The tuple to copy the values from.
	 **/
	public void setXYZ(Tuple4 tuple);
}
