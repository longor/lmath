package de.longor.lmath.tuples;

import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/**
 * A class representing a 4-component (double precision floating-point) vector.
 */
public class Vec4d implements Tuple4
{
	
	/** The x. */
	public double x;
	
	/** The y. */
	public double y;
	
	/** The z. */
	public double z;
	
	/** The w. */
	public double w;
	
	/**
	 * Instantiates a new vec4d.
	 */
	public Vec4d()
	{
		x = 0;
		y = 0;
		z = 0;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param XYZW the xyzw
	 */
	public Vec4d(int XYZW)
	{
		x = XYZW;
		y = XYZW;
		z = XYZW;
		w = XYZW;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param X the x
	 * @param Y the y
	 */
	public Vec4d(int X, int Y)
	{
		x = X;
		y = Y;
		z = 0;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 */
	public Vec4d(int X, int Y, int Z)
	{
		x = X;
		y = Y;
		z = Z;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 * @param W the w
	 */
	public Vec4d(int X, int Y, int Z, int W)
	{
		x = X;
		y = Y;
		z = Z;
		w = W;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param XYZW the xyzw
	 */
	public Vec4d(float XYZW)
	{
		x = XYZW;
		y = XYZW;
		z = XYZW;
		w = XYZW;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param X the x
	 * @param Y the y
	 */
	public Vec4d(float X, float Y)
	{
		x = X;
		y = Y;
		z = 0;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 */
	public Vec4d(float X, float Y, float Z)
	{
		x = X;
		y = Y;
		z = Z;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 * @param W the w
	 */
	public Vec4d(float X, float Y, float Z, float W)
	{
		x = X;
		y = Y;
		z = Z;
		w = W;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param XYZ the xyz
	 */
	public Vec4d(double XYZ)
	{
		x = XYZ;
		y = XYZ;
		z = XYZ;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param X the x
	 * @param Y the y
	 */
	public Vec4d(double X, double Y)
	{
		x = X;
		y = Y;
		z = 0;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 */
	public Vec4d(double X, double Y, double Z)
	{
		x = X;
		y = Y;
		z = Z;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 * @param W the w
	 */
	public Vec4d(double X, double Y, double Z, double W)
	{
		x = X;
		y = Y;
		z = Z;
		w = W;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param vec the vec
	 */
	public Vec4d(Vec2f vec)
	{
		x = vec.x;
		y = vec.y;
		z = 0;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param vec the vec
	 */
	public Vec4d(Vec3f vec)
	{
		x = vec.x;
		y = vec.y;
		z = vec.z;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param vec the vec
	 */
	public Vec4d(Vec4f vec)
	{
		x = vec.x;
		y = vec.y;
		z = vec.z;
		w = vec.w;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param vec the vec
	 */
	public Vec4d(Vec2d vec)
	{
		x = vec.x;
		y = vec.y;
		z = 0;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param vec the vec
	 */
	public Vec4d(Vec3d vec)
	{
		x = vec.x;
		y = vec.y;
		z = vec.z;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param vec the vec
	 */
	public Vec4d(Vec4d vec)
	{
		x = vec.x;
		y = vec.y;
		z = vec.z;
		w = vec.w;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param tuple the tuple
	 */
	public Vec4d(Tuple2 tuple)
	{
		x = tuple.getXd();
		y = tuple.getYd();
		z = 0;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param tuple the tuple
	 */
	public Vec4d(Tuple3 tuple)
	{
		x = tuple.getXd();
		y = tuple.getYd();
		z = tuple.getZd();
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param tuple the tuple
	 */
	public Vec4d(Tuple4 tuple)
	{
		x = tuple.getXd();
		y = tuple.getYd();
		z = tuple.getZd();
		w = tuple.getWd();
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param buffer the buffer
	 */
	public Vec4d(IntBuffer buffer)
	{
		x = buffer.get(0);
		y = buffer.get(1);
		z = buffer.get(2);
		w = buffer.get(2);
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param buffer the buffer
	 */
	public Vec4d(FloatBuffer buffer)
	{
		x = buffer.get(0);
		y = buffer.get(1);
		z = buffer.get(2);
		w = buffer.get(2);
	}
	
	/**
	 * Instantiates a new vec4d.
	 *
	 * @param buffer the buffer
	 */
	public Vec4d(DoubleBuffer buffer)
	{
		x = buffer.get(0);
		y = buffer.get(1);
		z = buffer.get(2);
		w = buffer.get(2);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getXf()
	 */
	@Override
	public float getXf() {
		return (float) x;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getXd()
	 */
	@Override
	public double getXd() {
		return x;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getYf()
	 */
	@Override
	public float getYf() {
		return (float) y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getYd()
	 */
	@Override
	public double getYd() {
		return y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#getZf()
	 */
	@Override
	public float getZf() {
		return (float) z;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#getZd()
	 */
	@Override
	public double getZd() {
		return z;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#getWf()
	 */
	@Override
	public float getWf() {
		return (float) w;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#getWd()
	 */
	@Override
	public double getWd() {
		return w;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setX(float)
	 */
	@Override
	public void setX(float value) {
		x = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setX(double)
	 */
	@Override
	public void setX(double value) {
		x =  value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setY(float)
	 */
	@Override
	public void setY(float value) {
		y = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setY(double)
	 */
	@Override
	public void setY(double value) {
		y =  value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setZ(float)
	 */
	@Override
	public void setZ(float value) {
		z = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setZ(double)
	 */
	@Override
	public void setZ(double value) {
		z =  value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#setW(float)
	 */
	@Override
	public void setW(float value) {
		w = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#setW(double)
	 */
	@Override
	public void setW(double value) {
		w =  value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(float, float)
	 */
	@Override
	public void setXY(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(double, double)
	 */
	@Override
	public void setXY(double x, double y) {
		this.x =  x;
		this.y =  y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(float, float, float)
	 */
	@Override
	public void setXYZ(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(double, double, double)
	 */
	@Override
	public void setXYZ(double x, double y, double z) {
		this.x =  x;
		this.y =  y;
		this.z =  z;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#setXYZW(float, float, float, float)
	 */
	@Override
	public void setXYZW(float x, float y, float z, float w) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#setXYZW(double, double, double, double)
	 */
	@Override
	public void setXYZW(double x, double y, double z, double w) {
		this.x =  x;
		this.y =  y;
		this.z =  z;
		this.w =  w;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(float)
	 */
	@Override
	public void setXY(float xy) {
		this.x = xy;
		this.y = xy;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(double)
	 */
	@Override
	public void setXY(double xy) {
		this.x =  xy;
		this.y =  xy;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(float)
	 */
	@Override
	public void setXYZ(float xyz) {
		this.x = xyz;
		this.y = xyz;
		this.z = xyz;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(double)
	 */
	@Override
	public void setXYZ(double xyz) {
		this.x =  xyz;
		this.y =  xyz;
		this.z =  xyz;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#setXYZW(float)
	 */
	@Override
	public void setXYZW(float xyzw) {
		this.x = xyzw;
		this.y = xyzw;
		this.z = xyzw;
		this.w = xyzw;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#setXYZW(double)
	 */
	@Override
	public void setXYZW(double xyzw) {
		this.x =  xyzw;
		this.y =  xyzw;
		this.z =  xyzw;
		this.w =  xyzw;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#setXYZW(float, float)
	 */
	@Override
	public void setXYZW(float xyz, float w) {
		this.x = xyz;
		this.y = xyz;
		this.z = xyz;
		this.w = w;
	}

	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#setXYZW(double, double)
	 */
	@Override
	public void setXYZW(double xyz, double w) {
		this.x = xyz;
		this.y = xyz;
		this.z = xyz;
		this.w = w;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(de.longor.lmath.tuples.Tuple2)
	 */
	@Override
	public void setXY(Tuple2 tuple) {
		this.x = tuple.getXd();
		this.y = tuple.getYd();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(de.longor.lmath.tuples.Tuple4)
	 */
	@Override
	public void setXY(Tuple4 tuple) {
		this.x = tuple.getXd();
		this.y = tuple.getYd();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(de.longor.lmath.tuples.Tuple3)
	 */
	@Override
	public void setXY(Tuple3 tuple) {
		this.x = tuple.getXd();
		this.y = tuple.getYd();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(de.longor.lmath.tuples.Tuple3)
	 */
	@Override
	public void setXYZ(Tuple3 tuple) {
		this.x = tuple.getXd();
		this.y = tuple.getYd();
		this.z = tuple.getZd();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(de.longor.lmath.tuples.Tuple4)
	 */
	@Override
	public void setXYZ(Tuple4 tuple) {
		this.x = tuple.getXd();
		this.y = tuple.getYd();
		this.z = tuple.getZd();
	}

	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#setXYZW(de.longor.lmath.tuples.Tuple4)
	 */
	@Override
	public void setXYZW(Tuple4 tuple) {
		this.x = tuple.getXd();
		this.y = tuple.getYd();
		this.z = tuple.getZd();
		this.w = tuple.getWd();
	}
	
	@Override
	public String toString()
	{
		return "vec4d[" + x + ", " + y + ", " + z + ", " + w + "]";
	}
	
	/* Default Static OUT=IN-OP-IN Method
	public static final Vec2f add(Vec2f ain, Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4d
	 */
	public static final Vec4d add(Vec4d ain, Vec4d bin, Vec4d out)
	{
		out.x = ain.x + bin.x;
		out.y = ain.y + bin.y;
		out.z = ain.z + bin.z;
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4d
	 */
	public static final Vec4d sub(Vec4d ain, Vec4d bin, Vec4d out)
	{
		out.x = ain.x - bin.x;
		out.y = ain.y - bin.y;
		out.z = ain.z - bin.z;
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4d
	 */
	public static final Vec4d mul(Vec4d ain, Vec4d bin, Vec4d out)
	{
		out.x = ain.x * bin.x;
		out.y = ain.y * bin.y;
		out.z = ain.z * bin.z;
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4d
	 */
	public static final Vec4d div(Vec4d ain, Vec4d bin, Vec4d out)
	{
		out.x = ain.x / bin.x;
		out.y = ain.y / bin.y;
		out.z = ain.z / bin.z;
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4d
	 */
	public static final Vec4d mod(Vec4d ain, Vec4d bin, Vec4d out)
	{
		out.x = ain.x % bin.x;
		out.y = ain.y % bin.y;
		out.z = ain.z % bin.z;
		return out;
	}
	
	/**
	 * Distance.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @return the double
	 */
	public static final double distance(Vec4d ain, Vec4d bin)
	{
		double offx = bin.x - ain.x;
		double offy = bin.y - ain.y;
		double offz = bin.z - ain.z;
		return Math.sqrt(offx*offx + offy*offy + offz*offz);
	}
	
	/* Default OUT=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4d
	 */
	public final Vec4d add(Vec4d bin, Vec4d out)
	{
		out.x = this.x + bin.x;
		out.y = this.y + bin.y;
		out.z = this.z + bin.z;
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4d
	 */
	public final Vec4d sub(Vec4d bin, Vec4d out)
	{
		out.x = this.x - bin.x;
		out.y = this.y - bin.y;
		out.z = this.z - bin.z;
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4d
	 */
	public final Vec4d mul(Vec4d bin, Vec4d out)
	{
		out.x = this.x * bin.x;
		out.y = this.y * bin.y;
		out.z = this.z * bin.z;
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4d
	 */
	public final Vec4d div(Vec4d bin, Vec4d out)
	{
		out.x = this.x / bin.x;
		out.y = this.y / bin.y;
		out.z = this.z / bin.z;
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4d
	 */
	public final Vec4d mod(Vec4d bin, Vec4d out)
	{
		out.x = this.x % bin.x;
		out.y = this.y % bin.y;
		out.z = this.z % bin.z;
		return out;
	}
	
	/**
	 * Distance.
	 *
	 * @param bin the bin
	 * @return the double
	 */
	public final double distance(Vec4d bin)
	{
		double offx = bin.x - this.x;
		double offy = bin.y - this.y;
		double offz = bin.z - this.z;
		return Math.sqrt(offx*offx + offy*offy + offz*offz);
	}
	
	/* Default THIS=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @return the vec4d
	 */
	public final Vec4d add(Vec4d bin)
	{
		this.x = this.x + bin.x;
		this.y = this.y + bin.y;
		this.z = this.z + bin.z;
		return this;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @return the vec4d
	 */
	public final Vec4d sub(Vec4d bin)
	{
		this.x = this.x - bin.x;
		this.y = this.y - bin.y;
		this.z = this.z - bin.z;
		return this;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @return the vec4d
	 */
	public final Vec4d mul(Vec4d bin)
	{
		this.x = this.x * bin.x;
		this.y = this.y * bin.y;
		this.z = this.z * bin.z;
		return this;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @return the vec4d
	 */
	public final Vec4d div(Vec4d bin)
	{
		this.x = this.x / bin.x;
		this.y = this.y / bin.y;
		this.z = this.z / bin.z;
		return this;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @return the vec4d
	 */
	public final Vec4d mod(Vec4d bin)
	{
		this.x = this.x % bin.x;
		this.y = this.y % bin.y;
		this.z = this.z % bin.z;
		return this;
	}
	
	/* Default THIS=THIS-OP-THIS Method
	public final Vec2f add(Vec2f bin)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @return the vec4d
	 */
	public final Vec4d add()
	{
		this.x = this.x + this.x;
		this.y = this.y + this.y;
		this.z = this.z + this.z;
		return this;
	}
	
	/**
	 * Mul.
	 *
	 * @return the vec4d
	 */
	public final Vec4d mul()
	{
		this.x = this.x * this.x;
		this.y = this.y * this.y;
		this.z = this.z * this.z;
		return this;
	}
	
	/* Default Static OUT=IN-OP-IN Method
	public static final Vec2f add(Vec2f ain, Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4d
	 */
	public static final Vec4d add(Vec4d ain, Tuple4 bin, Vec4d out)
	{
		out.x = ain.x + bin.getXf();
		out.y = ain.y + bin.getYf();
		out.z = ain.z + bin.getZf();
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4d
	 */
	public static final Vec4d sub(Vec4d ain, Tuple4 bin, Vec4d out)
	{
		out.x = ain.x - bin.getXf();
		out.y = ain.y - bin.getYf();
		out.z = ain.z - bin.getZf();
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4d
	 */
	public static final Vec4d mul(Vec4d ain, Tuple4 bin, Vec4d out)
	{
		out.x = ain.x * bin.getXf();
		out.y = ain.y * bin.getYf();
		out.z = ain.z * bin.getZf();
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4d
	 */
	public static final Vec4d div(Vec4d ain, Tuple4 bin, Vec4d out)
	{
		out.x = ain.x / bin.getXf();
		out.y = ain.y / bin.getYf();
		out.z = ain.z / bin.getZf();
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4d
	 */
	public static final Vec4d mod(Vec4d ain, Tuple4 bin, Vec4d out)
	{
		out.x = ain.x % bin.getXf();
		out.y = ain.y % bin.getYf();
		out.z = ain.z % bin.getZf();
		return out;
	}
	
	/* Default OUT=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4d
	 */
	public final Vec4d add(Tuple4 bin, Vec4d out)
	{
		out.x = this.x + bin.getXf();
		out.y = this.y + bin.getYf();
		out.z = this.z + bin.getZf();
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4d
	 */
	public final Vec4d sub(Tuple4 bin, Vec4d out)
	{
		out.x = this.x - bin.getXf();
		out.y = this.y - bin.getYf();
		out.z = this.z - bin.getZf();
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4d
	 */
	public final Vec4d mul(Tuple4 bin, Vec4d out)
	{
		out.x = this.x * bin.getXf();
		out.y = this.y * bin.getYf();
		out.z = this.z * bin.getZf();
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4d
	 */
	public final Vec4d div(Tuple4 bin, Vec4d out)
	{
		out.x = this.x / bin.getXf();
		out.y = this.y / bin.getYf();
		out.z = this.z / bin.getZf();
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4d
	 */
	public final Vec4d mod(Tuple4 bin, Vec4d out)
	{
		out.x = this.x % bin.getXf();
		out.y = this.y % bin.getYf();
		out.z = this.z % bin.getZf();
		return out;
	}
	
	/**
	 * Distance.
	 *
	 * @param bin the bin
	 * @return the double
	 */
	public final double distance(Tuple4 bin)
	{
		double offx = bin.getXd() - this.x;
		double offy = bin.getYd() - this.y;
		double offz = bin.getZd() - this.z;
		return Math.sqrt(offx*offx + offy*offy + offz*offz);
	}
	
	/* Default THIS=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @return the vec4d
	 */
	public final Vec4d add(Tuple4 bin)
	{
		this.x = this.x + bin.getXf();
		this.y = this.y + bin.getYf();
		this.z = this.z + bin.getZf();
		return this;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @return the vec4d
	 */
	public final Vec4d sub(Tuple4 bin)
	{
		this.x = this.x - bin.getXf();
		this.y = this.y - bin.getYf();
		this.z = this.z - bin.getZf();
		return this;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @return the vec4d
	 */
	public final Vec4d mul(Tuple4 bin)
	{
		this.x = this.x * bin.getXf();
		this.y = this.y * bin.getYf();
		this.z = this.z * bin.getZf();
		return this;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @return the vec4d
	 */
	public final Vec4d div(Tuple4 bin)
	{
		this.x = this.x / bin.getXf();
		this.y = this.y / bin.getYf();
		this.z = this.z / bin.getZf();
		return this;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @return the vec4d
	 */
	public final Vec4d mod(Tuple4 bin)
	{
		this.x = this.x % bin.getXf();
		this.y = this.y % bin.getYf();
		this.z = this.z % bin.getZf();
		return this;
	}
	
	/* Default Static OUT=IN-OP-IN Method
	public static final Vec2f add(Vec2f ain, Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the tuple4
	 */
	public static final Tuple4 add(Vec4d ain, Tuple4 bin, Tuple4 out)
	{
		out.setX(ain.x + bin.getXf());
		out.setY(ain.y + bin.getYf());
		out.setZ(ain.z + bin.getZf());
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the tuple4
	 */
	public static final Tuple4 sub(Vec4d ain, Tuple4 bin, Tuple4 out)
	{
		out.setX(ain.x - bin.getXf());
		out.setY(ain.y - bin.getYf());
		out.setZ(ain.z - bin.getZf());
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the tuple4
	 */
	public static final Tuple4 mul(Vec4d ain, Tuple4 bin, Tuple4 out)
	{
		out.setX(ain.x * bin.getXf());
		out.setY(ain.y * bin.getYf());
		out.setZ(ain.z * bin.getZf());
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the tuple4
	 */
	public static final Tuple4 div(Vec4d ain, Tuple4 bin, Tuple4 out)
	{
		out.setX(ain.x / bin.getXf());
		out.setY(ain.y / bin.getYf());
		out.setZ(ain.z / bin.getZf());
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the tuple4
	 */
	public static final Tuple4 mod(Vec4d ain, Tuple4 bin, Tuple4 out)
	{
		out.setX(ain.x % bin.getXf());
		out.setY(ain.y % bin.getYf());
		out.setZ(ain.z % bin.getZf());
		return out;
	}
	
	public final void normalize()
	{
		double len = Math.sqrt((x*x) + (y*y) + (z*z));
		x /= len;
		y /= len;
		z /= len;
		w = 1;
	}
	
	public final double length()
	{
		return Math.sqrt((x*x) + (y*y) + (z*z));
	}
	
	/**
	 * Returns the dot-product of this vector with itself.
	 * @return the dot product of this vector with itself.
	 **/
	public final double dotproduct()
	{
		return (this.x*this.x) + (this.y*this.y) + (this.z*this.z);
	}
	
	/**
	 * Returns the dot-product of this vector with the given one.
	 * @param v The vector to create a dot-product with this one from.
	 * @return the dot product of this vector with the given one.
	 **/
	public final double dotproduct(Vec3f v)
	{
		return (this.x*v.x) + (this.y*v.y) + (this.z*v.z);
	}
	
	/**
	 * Returns the dot-product of this vector with the given one.
	 * @param v The vector to create a dot-product with this one from.
	 * @return the dot product of this vector with the given one.
	 **/
	public final double dotproduct(Vec3d v)
	{
		return (this.x*v.x) + (this.y*v.y) + (this.z*v.z);
	}
	
	/**
	 * Returns the dot-product of this vector with the given one.
	 * @param v The vector to create a dot-product with this one from.
	 * @return the dot product of this vector with the given one.
	 **/
	public final double dotproduct(Vec4f v)
	{
		return (this.x*v.x) + (this.y*v.y) + (this.z*v.z);
	}
	
	/**
	 * Returns the dot-product of this vector with the given one.
	 * @param v The vector to create a dot-product with this one from.
	 * @return the dot product of this vector with the given one.
	 **/
	public final double dotproduct(Vec4d v)
	{
		return (this.x*v.x) + (this.y*v.y) + (this.z*v.z);
	}
	
	
	
}
