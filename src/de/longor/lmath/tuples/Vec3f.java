package de.longor.lmath.tuples;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import de.longor.lmath.LMath;

/**
 * A class representing a 3-component (floating-point) vector.
 */
public class Vec3f implements Tuple3
{
	
	/** The x. */
	public float x;
	
	/** The y. */
	public float y;
	
	/** The z. */
	public float z;
	
	/**
	 * Instantiates a new vec3f.
	 */
	public Vec3f()
	{
		x = 0;
		y = 0;
		z = 0;
	}
	
	/**
	 * Instantiates a new vec3f.
	 *
	 * @param XYZ the xyz
	 */
	public Vec3f(int XYZ)
	{
		x = XYZ;
		y = XYZ;
		z = XYZ;
	}
	
	/**
	 * Instantiates a new vec3f.
	 *
	 * @param X the x
	 * @param Y the y
	 */
	public Vec3f(int X, int Y)
	{
		x = X;
		y = Y;
		z = 0;
	}
	
	/**
	 * Instantiates a new vec3f.
	 *
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 */
	public Vec3f(int X, int Y, int Z)
	{
		x = X;
		y = Y;
		z = Z;
	}
	
	/**
	 * Instantiates a new vec3f.
	 *
	 * @param XYZ the xyz
	 */
	public Vec3f(float XYZ)
	{
		x = XYZ;
		y = XYZ;
		z = XYZ;
	}
	
	/**
	 * Instantiates a new vec3f.
	 *
	 * @param X the x
	 * @param Y the y
	 */
	public Vec3f(float X, float Y)
	{
		x = X;
		y = Y;
		z = 0;
	}
	
	/**
	 * Instantiates a new vec3f.
	 *
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 */
	public Vec3f(float X, float Y, float Z)
	{
		x = X;
		y = Y;
		z = Z;
	}
	
	/**
	 * Instantiates a new vec3f.
	 *
	 * @param XYZ the xyz
	 */
	public Vec3f(double XYZ)
	{
		x = (float) XYZ;
		y = (float) XYZ;
		z = (float) XYZ;
	}
	
	/**
	 * Instantiates a new vec3f.
	 *
	 * @param X the x
	 * @param Y the y
	 */
	public Vec3f(double X, double Y)
	{
		x = (float) X;
		y = (float) Y;
		z = 0;
	}
	
	/**
	 * Instantiates a new vec3f.
	 *
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 */
	public Vec3f(double X, double Y, double Z)
	{
		x = (float) X;
		y = (float) Y;
		z = (float) Z;
	}
	
	/**
	 * Instantiates a new vec3f.
	 *
	 * @param vec the vec
	 */
	public Vec3f(Vec2f vec)
	{
		x = vec.x;
		y = vec.y;
		z = 0;
	}
	
	/**
	 * Instantiates a new vec3f.
	 *
	 * @param vec the vec
	 */
	public Vec3f(Vec3f vec)
	{
		x = vec.x;
		y = vec.y;
		z = vec.z;
	}
	
	/**
	 * Instantiates a new vec3f.
	 *
	 * @param vec the vec
	 */
	public Vec3f(Vec4f vec)
	{
		x = vec.x;
		y = vec.y;
		z = vec.z;
	}
	
	/**
	 * Instantiates a new vec3f.
	 *
	 * @param vec the vec
	 */
	public Vec3f(Vec2d vec)
	{
		x = (float) vec.x;
		y = (float) vec.y;
		z = 0;
	}
	
	/**
	 * Instantiates a new vec3f.
	 *
	 * @param vec the vec
	 */
	public Vec3f(Vec3d vec)
	{
		x = (float) vec.x;
		y = (float) vec.y;
		z = (float) vec.z;
	}
	
	/**
	 * Instantiates a new vec3f.
	 *
	 * @param vec the vec
	 */
	public Vec3f(Vec4d vec)
	{
		x = (float) vec.x;
		y = (float) vec.y;
		z = (float) vec.z;
	}
	
	/**
	 * Instantiates a new vec3f.
	 *
	 * @param tuple the tuple
	 */
	public Vec3f(Tuple2 tuple)
	{
		x = tuple.getXf();
		y = tuple.getYf();
		z = 0;
	}
	
	/**
	 * Instantiates a new vec3f.
	 *
	 * @param tuple the tuple
	 */
	public Vec3f(Tuple3 tuple)
	{
		x = tuple.getXf();
		y = tuple.getYf();
		z = tuple.getZf();
	}
	
	/**
	 * Instantiates a new vec3f.
	 *
	 * @param tuple the tuple
	 */
	public Vec3f(Tuple4 tuple)
	{
		x = tuple.getXf();
		y = tuple.getYf();
		z = tuple.getZf();
	}
	
	/**
	 * Instantiates a new vec3f.
	 *
	 * @param buffer the buffer
	 */
	public Vec3f(IntBuffer buffer)
	{
		x = buffer.get(0);
		y = buffer.get(1);
		z = buffer.get(2);
	}
	
	/**
	 * Instantiates a new vec3f.
	 *
	 * @param buffer the buffer
	 */
	public Vec3f(FloatBuffer buffer)
	{
		x = buffer.get(0);
		y = buffer.get(1);
		z = buffer.get(2);
	}
	
	public Vec3f copy()
	{
		return new Vec3f(this);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getXf()
	 */
	@Override
	public float getXf() {
		return x;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getXd()
	 */
	@Override
	public double getXd() {
		return x;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getYf()
	 */
	@Override
	public float getYf() {
		return y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getYd()
	 */
	@Override
	public double getYd() {
		return y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#getZf()
	 */
	@Override
	public float getZf() {
		return z;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#getZd()
	 */
	@Override
	public double getZd() {
		return z;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setX(float)
	 */
	@Override
	public void setX(float value) {
		x = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setX(double)
	 */
	@Override
	public void setX(double value) {
		x = (float) value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setY(float)
	 */
	@Override
	public void setY(float value) {
		y = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setY(double)
	 */
	@Override
	public void setY(double value) {
		y = (float) value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setZ(float)
	 */
	@Override
	public void setZ(float value) {
		z = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setZ(double)
	 */
	@Override
	public void setZ(double value) {
		z = (float) value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(float, float)
	 */
	@Override
	public void setXY(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(double, double)
	 */
	@Override
	public void setXY(double x, double y) {
		this.x = (float) x;
		this.y = (float) y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(float, float, float)
	 */
	@Override
	public void setXYZ(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(double, double, double)
	 */
	@Override
	public void setXYZ(double x, double y, double z) {
		this.x = (float) x;
		this.y = (float) y;
		this.z = (float) z;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(float)
	 */
	@Override
	public void setXY(float xy) {
		this.x = xy;
		this.y = xy;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(double)
	 */
	@Override
	public void setXY(double xy) {
		this.x = (float) xy;
		this.y = (float) xy;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(float)
	 */
	@Override
	public void setXYZ(float xyz) {
		this.x = xyz;
		this.y = xyz;
		this.z = xyz;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(double)
	 */
	@Override
	public void setXYZ(double xyz) {
		this.x = (float) xyz;
		this.y = (float) xyz;
		this.z = (float) xyz;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(de.longor.lmath.tuples.Tuple2)
	 */
	@Override
	public void setXY(Tuple2 tuple) {
		this.x = tuple.getXf();
		this.y = tuple.getYf();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(de.longor.lmath.tuples.Tuple3)
	 */
	@Override
	public void setXY(Tuple3 tuple) {
		this.x = tuple.getXf();
		this.y = tuple.getYf();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(de.longor.lmath.tuples.Tuple4)
	 */
	@Override
	public void setXY(Tuple4 tuple) {
		this.x = tuple.getXf();
		this.y = tuple.getYf();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(de.longor.lmath.tuples.Tuple3)
	 */
	@Override
	public void setXYZ(Tuple3 tuple) {
		this.x = tuple.getXf();
		this.y = tuple.getYf();
		this.z = tuple.getZf();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(de.longor.lmath.tuples.Tuple4)
	 */
	@Override
	public void setXYZ(Tuple4 tuple) {
		this.x = tuple.getXf();
		this.y = tuple.getYf();
		this.z = tuple.getZf();
	}
	
	@Override
	public String toString()
	{
		return "vec3f[" + x + ", " + y + ", " + z + "]";
	}
	
	/* Default Static OUT=IN-OP-IN Method
	public static final Vec2f add(Vec2f ain, Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public static final Vec3f add(Vec3f ain, Vec3f bin, Vec3f out)
	{
		out.x = ain.x + bin.x;
		out.y = ain.y + bin.y;
		out.z = ain.z + bin.z;
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public static final Vec3f sub(Vec3f ain, Vec3f bin, Vec3f out)
	{
		out.x = ain.x - bin.x;
		out.y = ain.y - bin.y;
		out.z = ain.z - bin.z;
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public static final Vec3f mul(Vec3f ain, Vec3f bin, Vec3f out)
	{
		out.x = ain.x * bin.x;
		out.y = ain.y * bin.y;
		out.z = ain.z * bin.z;
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public static final Vec3f mul(Vec3f ain, float scalar, Vec3f out)
	{
		out.x = ain.x * scalar;
		out.y = ain.y * scalar;
		out.z = ain.z * scalar;
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public static final Vec3f div(Vec3f ain, Vec3f bin, Vec3f out)
	{
		out.x = ain.x / bin.x;
		out.y = ain.y / bin.y;
		out.z = ain.z / bin.z;
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public static final Vec3f mod(Vec3f ain, Vec3f bin, Vec3f out)
	{
		out.x = ain.x % bin.x;
		out.y = ain.y % bin.y;
		out.z = ain.z % bin.z;
		return out;
	}
	
	/**
	 * Distance.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @return the float
	 */
	public static final float distance(Vec3f ain, Vec3f bin)
	{
		float offx = bin.x - ain.x;
		float offy = bin.y - ain.y;
		float offz = bin.z - ain.z;
		return (float) Math.sqrt(offx*offx + offy*offy + offz*offz);
	}
	
	/**
	 * Lerp.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param fraction the fraction
	 * @param out the out
	 * @return the vec3f
	 */
	public static final Vec3f lerp(Vec3f ain, Vec3f bin, float fraction, Vec3f out)
	{
		out.x = ain.x + (fraction * (bin.x - ain.x));
		out.y = ain.y + (fraction * (bin.y - ain.y));
		out.z = ain.z + (fraction * (bin.z - ain.z));
		return out;
	}
	
	/**
	 * The result is the reflection of I on N.
	 * result = I - (N * (2.0 * dot(N,I)));
	 *
	 * @param I The vector to be reflected.
	 * @param N The surface to be reflected on.
	 * @param out The vector in which the result will be stored.
	 **/
	public static final void reflect(Vec3f I, Vec3f N, Vec3f out)
	{
		float dot = 2 * I.dotproduct(N);
		out.x = I.x - (dot * N.x);
		out.y = I.y - (dot * N.y);
		out.z = I.z - (dot * N.z);
	}
	
	/**
	 * Clamps all values to the given range.
	 */
	public static final Vec3f clamp(Vec3f in, float min, float max, Vec3f out)
	{
		out.x = LMath.clamp(min, max, in.x);
		out.y = LMath.clamp(min, max, in.y);
		out.z = LMath.clamp(min, max, in.z);
		return out;
	}
	
	/*
	public static final Vec3f lerp(Vec3f ain, Vec3f bin, float fraction, Vec3f out)
	{
		out.x = LMath.lerp(ain.x, bin.x, fraction);
		out.y = LMath.lerp(ain.y, bin.y, fraction);
		out.z = LMath.lerp(ain.z, bin.z, fraction);
		return out;
	}
	*/
	
	/* Default OUT=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public final Vec3f add(Vec3f bin, Vec3f out)
	{
		out.x = this.x + bin.x;
		out.y = this.y + bin.y;
		out.z = this.z + bin.z;
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public final Vec3f sub(Vec3f bin, Vec3f out)
	{
		out.x = this.x - bin.x;
		out.y = this.y - bin.y;
		out.z = this.z - bin.z;
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public final Vec3f mul(Vec3f bin, Vec3f out)
	{
		out.x = this.x * bin.x;
		out.y = this.y * bin.y;
		out.z = this.z * bin.z;
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public final Vec3f mul(float scalar, Vec3f out)
	{
		out.x = this.x * scalar;
		out.y = this.y * scalar;
		out.z = this.z * scalar;
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public final Vec3f div(Vec3f bin, Vec3f out)
	{
		out.x = this.x / bin.x;
		out.y = this.y / bin.y;
		out.z = this.z / bin.z;
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public final Vec3f mod(Vec3f bin, Vec3f out)
	{
		out.x = this.x % bin.x;
		out.y = this.y % bin.y;
		out.z = this.z % bin.z;
		return out;
	}
	
	/**
	 * Distance.
	 *
	 * @param bin the bin
	 * @return the float
	 */
	public final float distance(Vec3f bin)
	{
		float offx = bin.x - this.x;
		float offy = bin.y - this.y;
		float offz = bin.z - this.z;
		return (float) Math.sqrt(offx*offx + offy*offy + offz*offz);
	}
	
	/**
	 * Lerp.
	 *
	 * @param bin the bin
	 * @param fraction the fraction
	 * @param out the out
	 * @return the vec3f
	 */
	public final Vec3f lerp(Vec3f bin, float fraction, Vec3f out)
	{
		out.x = this.x + (fraction * (bin.x - this.x));
		out.y = this.y + (fraction * (bin.y - this.y));
		out.z = this.z + (fraction * (bin.z - this.z));
		return out;
	}
	
	/**
	 * @param I The vector to be reflected.
	 * @param N The surface to be reflected on.
	 **/
	public final void reflect(Vec3f I, Vec3f N)
	{
		float dot = 2 * I.dotproduct(N);
		this.x = I.x - (dot * N.x);
		this.y = I.y - (dot * N.y);
		this.z = I.z - (dot * N.z);
	}
	
	/* Default THIS=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @return the vec3f
	 */
	public final Vec3f add(Vec3f bin)
	{
		this.x = this.x + bin.x;
		this.y = this.y + bin.y;
		this.z = this.z + bin.z;
		return this;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @return the vec3f
	 */
	public final Vec3f sub(Vec3f bin)
	{
		this.x = this.x - bin.x;
		this.y = this.y - bin.y;
		this.z = this.z - bin.z;
		return this;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @return the vec3f
	 */
	public final Vec3f mul(Vec3f bin)
	{
		this.x = this.x * bin.x;
		this.y = this.y * bin.y;
		this.z = this.z * bin.z;
		return this;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @return the vec3f
	 */
	public final Vec3f mul(float scalar)
	{
		this.x = this.x * scalar;
		this.y = this.y * scalar;
		this.z = this.z * scalar;
		return this;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @return the vec3f
	 */
	public final Vec3f div(Vec3f bin)
	{
		this.x = this.x / bin.x;
		this.y = this.y / bin.y;
		this.z = this.z / bin.z;
		return this;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @return the vec3f
	 */
	public final Vec3f mod(Vec3f bin)
	{
		this.x = this.x % bin.x;
		this.y = this.y % bin.y;
		this.z = this.z % bin.z;
		return this;
	}
	
	/**
	 * Lerp.
	 *
	 * @param bin the bin
	 * @param fraction the fraction
	 * @return the vec3f
	 */
	public final Vec3f lerp(Vec3f bin, float fraction)
	{
		this.x = this.x + (fraction * (bin.x - this.x));
		this.y = this.y + (fraction * (bin.y - this.y));
		this.z = this.z + (fraction * (bin.z - this.z));
		return this;
	}
	
	/* Default THIS=THIS-OP-THIS Method
	public final Vec2f add(Vec2f bin)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @return the vec3f
	 */
	public final Vec3f add()
	{
		this.x = this.x + this.x;
		this.y = this.y + this.y;
		this.z = this.z + this.z;
		return this;
	}
	
	/**
	 * Mul.
	 *
	 * @return the vec3f
	 */
	public final Vec3f mul()
	{
		this.x = this.x * this.x;
		this.y = this.y * this.y;
		this.z = this.z * this.z;
		return this;
	}
	
	/* Default Static OUT=IN-OP-IN Method
	public static final Vec2f add(Vec2f ain, Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public static final Vec3f add(Vec3f ain, Tuple3 bin, Vec3f out)
	{
		out.x = ain.x + bin.getXf();
		out.y = ain.y + bin.getYf();
		out.z = ain.z + bin.getZf();
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public static final Vec3f sub(Vec3f ain, Tuple3 bin, Vec3f out)
	{
		out.x = ain.x - bin.getXf();
		out.y = ain.y - bin.getYf();
		out.z = ain.z - bin.getZf();
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public static final Vec3f mul(Vec3f ain, Tuple3 bin, Vec3f out)
	{
		out.x = ain.x * bin.getXf();
		out.y = ain.y * bin.getYf();
		out.z = ain.z * bin.getZf();
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public static final Vec3f div(Vec3f ain, Tuple3 bin, Vec3f out)
	{
		out.x = ain.x / bin.getXf();
		out.y = ain.y / bin.getYf();
		out.z = ain.z / bin.getZf();
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public static final Vec3f mod(Vec3f ain, Tuple3 bin, Vec3f out)
	{
		out.x = ain.x % bin.getXf();
		out.y = ain.y % bin.getYf();
		out.z = ain.z % bin.getZf();
		return out;
	}
	
	/**
	 * Lerp.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param fraction the fraction
	 * @param out the out
	 * @return the vec3f
	 */
	public static final Vec3f lerp(Vec3f ain, Tuple3 bin, float fraction, Vec3f out)
	{
		out.x = ain.x + (fraction * (bin.getXf() - ain.x));
		out.y = ain.y + (fraction * (bin.getYf() - ain.y));
		out.z = ain.z + (fraction * (bin.getZf() - ain.z));
		return out;
	}
	
	/**
	 * Lerp.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param fraction the fraction
	 * @param out the out
	 * @return the vec3f
	 */
	public static final Vec3f lerp(Tuple3 ain, Vec3f bin, float fraction, Vec3f out)
	{
		out.x = ain.getXf() + (fraction * (bin.x - ain.getXf()));
		out.y = ain.getYf() + (fraction * (bin.y - ain.getYf()));
		out.z = ain.getZf() + (fraction * (bin.z - ain.getZf()));
		return out;
	}
	
	/* Default OUT=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public final Vec3f add(Tuple3 bin, Vec3f out)
	{
		out.x = this.x + bin.getXf();
		out.y = this.y + bin.getYf();
		out.z = this.z + bin.getZf();
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public final Vec3f sub(Tuple3 bin, Vec3f out)
	{
		out.x = this.x - bin.getXf();
		out.y = this.y - bin.getYf();
		out.z = this.z - bin.getZf();
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public final Vec3f mul(Tuple3 bin, Vec3f out)
	{
		out.x = this.x * bin.getXf();
		out.y = this.y * bin.getYf();
		out.z = this.z * bin.getZf();
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public final Vec3f div(Tuple3 bin, Vec3f out)
	{
		out.x = this.x / bin.getXf();
		out.y = this.y / bin.getYf();
		out.z = this.z / bin.getZf();
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3f
	 */
	public final Vec3f mod(Tuple3 bin, Vec3f out)
	{
		out.x = this.x % bin.getXf();
		out.y = this.y % bin.getYf();
		out.z = this.z % bin.getZf();
		return out;
	}
	
	/**
	 * Distance.
	 *
	 * @param bin the bin
	 * @return the float
	 */
	public final float distance(Tuple3 bin)
	{
		float offx = bin.getXf() - this.x;
		float offy = bin.getYf() - this.y;
		float offz = bin.getZf() - this.z;
		return (float) Math.sqrt(offx*offx + offy*offy + offz*offz);
	}
	
	/**
	 * Lerp.
	 *
	 * @param bin the bin
	 * @param fraction the fraction
	 * @param out the out
	 * @return the vec3f
	 */
	public final Vec3f lerp(Tuple3 bin, float fraction, Vec3f out)
	{
		out.x = this.x + (fraction * (bin.getXf() - this.x));
		out.y = this.y + (fraction * (bin.getYf() - this.y));
		out.z = this.z + (fraction * (bin.getZf() - this.z));
		return out;
	}
	
	/* Default THIS=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @return the vec3f
	 */
	public final Vec3f add(Tuple3 bin)
	{
		this.x = this.x + bin.getXf();
		this.y = this.y + bin.getYf();
		this.z = this.z + bin.getZf();
		return this;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @return the vec3f
	 */
	public final Vec3f sub(Tuple3 bin)
	{
		this.x = this.x - bin.getXf();
		this.y = this.y - bin.getYf();
		this.z = this.z - bin.getZf();
		return this;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @return the vec3f
	 */
	public final Vec3f mul(Tuple3 bin)
	{
		this.x = this.x * bin.getXf();
		this.y = this.y * bin.getYf();
		this.z = this.z * bin.getZf();
		return this;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @return the vec3f
	 */
	public final Vec3f div(Tuple3 bin)
	{
		this.x = this.x / bin.getXf();
		this.y = this.y / bin.getYf();
		this.z = this.z / bin.getZf();
		return this;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @return the vec3f
	 */
	public final Vec3f mod(Tuple3 bin)
	{
		this.x = this.x % bin.getXf();
		this.y = this.y % bin.getYf();
		this.z = this.z % bin.getZf();
		return this;
	}
	
	/**
	 * Lerp.
	 *
	 * @param bin the bin
	 * @param fraction the fraction
	 * @return the vec3f
	 */
	public final Vec3f lerp(Tuple3 bin, float fraction)
	{
		this.x = this.x + (fraction * (bin.getXf() - this.x));
		this.y = this.y + (fraction * (bin.getYf() - this.y));
		this.z = this.z + (fraction * (bin.getZf() - this.z));
		return this;
	}
	
	/* Default Static OUT=IN-OP-IN Method
	public static final Vec2f add(Vec2f ain, Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the tuple3
	 */
	public static final Tuple3 add(Vec3f ain, Tuple3 bin, Tuple3 out)
	{
		out.setX(ain.x + bin.getXf());
		out.setY(ain.y + bin.getYf());
		out.setZ(ain.z + bin.getZf());
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the tuple3
	 */
	public static final Tuple3 sub(Vec3f ain, Tuple3 bin, Tuple3 out)
	{
		out.setX(ain.x - bin.getXf());
		out.setY(ain.y - bin.getYf());
		out.setZ(ain.z - bin.getZf());
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the tuple3
	 */
	public static final Tuple3 mul(Vec3f ain, Tuple3 bin, Tuple3 out)
	{
		out.setX(ain.x * bin.getXf());
		out.setY(ain.y * bin.getYf());
		out.setZ(ain.z * bin.getZf());
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the tuple3
	 */
	public static final Tuple3 div(Vec3f ain, Tuple3 bin, Tuple3 out)
	{
		out.setX(ain.x / bin.getXf());
		out.setY(ain.y / bin.getYf());
		out.setZ(ain.z / bin.getZf());
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the tuple3
	 */
	public static final Tuple3 mod(Vec3f ain, Tuple3 bin, Tuple3 out)
	{
		out.setX(ain.x % bin.getXf());
		out.setY(ain.y % bin.getYf());
		out.setZ(ain.z % bin.getZf());
		return out;
	}
	
	/**
	 * Lerp.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param fraction the fraction
	 * @param out the out
	 * @return the tuple3
	 */
	public static final Tuple3 lerp(Vec3f ain, Tuple3 bin, float fraction, Tuple3 out)
	{
		out.setX(ain.x + (fraction * (bin.getXf() - ain.x)));
		out.setY(ain.y + (fraction * (bin.getYf() - ain.y)));
		out.setZ(ain.z + (fraction * (bin.getZf() - ain.z)));
		return out;
	}
	
	public final Vec3f normalize()
	{
		float len = (float) Math.sqrt((x*x) + (y*y) + (z*z));
		this.x /= len;
		this.y /= len;
		this.z /= len;
		return this;
	}
	
	public final Vec3f normalize(Vec3f out)
	{
		float len = (float) Math.sqrt((x*x) + (y*y) + (z*z));
		out.x = x / len;
		out.y = y / len;
		out.z = z / len;
		return out;
	}
	
	public final float length()
	{
		return (float) Math.sqrt((x*x) + (y*y) + (z*z));
	}
	
	public final float minimum()
	{
		return Math.min(x, Math.min(y, z));
	}
	
	public final float maximum()
	{
		return Math.max(x, Math.max(y, z));
	}
	
	/**
	 * Returns the dot-product of this vector with itself.
	 * @return the dot product of this vector with itself.
	 **/
	public final float dotproduct()
	{
		return (this.x*this.x) + (this.y*this.y) + (this.z*this.z);
	}
	
	/**
	 * Returns the dot-product of this vector with the given one.
	 * @param v The vector to create a dot-product with this one from.
	 * @return the dot product of this vector with the given one.
	 **/
	public final float dotproduct(Vec3f v)
	{
		return (this.x*v.x) + (this.y*v.y) + (this.z*v.z);
	}
	
	/**
	 * Returns the dot-product of this vector with the given one.
	 * @param v The vector to create a dot-product with this one from.
	 * @return the dot product of this vector with the given one.
	 **/
	public final double dotproduct(Vec3d v)
	{
		return (this.x*v.x) + (this.y*v.y) + (this.z*v.z);
	}
	
	/**
	 * Returns the dot-product of this vector with the given one.
	 * @param v The vector to create a dot-product with this one from.
	 * @return the dot product of this vector with the given one.
	 **/
	public final float dotproduct(Vec4f v)
	{
		return (this.x*v.x) + (this.y*v.y) + (this.z*v.z);
	}
	
	/**
	 * Returns the dot-product of this vector with the given one.
	 * @param v The vector to create a dot-product with this one from.
	 * @return the dot product of this vector with the given one.
	 **/
	public final double dotproduct(Vec4d v)
	{
		return (this.x*v.x) + (this.y*v.y) + (this.z*v.z);
	}
	
	/**
	 * Calculates the cross-product of this vector with the given vector and stores the result in this vector.
	 * @param v The right vector in the calculation.
	 * @return This vector with the result inside.
	 **/
	public final Vec3f crossproduct(Vec3f v)
	{
		float X = (this.y * v.z) - (this.z * v.y);
	    float Y = (this.z * v.x) - (this.x * v.z);
	    float Z = (this.x * v.y) - (this.y * v.x);
	    
	    this.x = X;
	    this.y = Y;
	    this.z = Z;
	    
	    return this;
	}
	
	/**
	 * Calculates the cross-product of this vector with the given vector and stores the result in the OUT vector.
	 * @param v The right vector in the calculation.
	 * @return The vector with the result inside.
	 **/
	public final Vec3f crossproduct(Vec3f v, Vec3f out)
	{
		float X = (this.y * v.z) - (this.z * v.y);
	    float Y = (this.z * v.x) - (this.x * v.z);
	    float Z = (this.x * v.y) - (this.y * v.x);
	    
	    out.x = X;
	    out.y = Y;
	    out.z = Z;
	    
	    return out;
	}
	
	/**
	 * Calculates the cross-product of this vector with the given vector and stores the result in the OUT vector.
	 * @param v The right vector in the calculation.
	 * @return The vector with the result inside.
	 **/
	public final Tuple3 crossproduct(Vec3f v, Tuple3 out)
	{
		double X = (this.y * v.z) - (this.z * v.y);
		double Y = (this.z * v.x) - (this.x * v.z);
		double Z = (this.x * v.y) - (this.y * v.x);
	    
	    out.setX(X);
	    out.setY(Y);
	    out.setZ(Z);
	    
	    return out;
	}

	public Vec3f rotateAroundX(float theta)
	{
		float ny = (float) ((this.y*Math.cos(theta)) - (this.z*Math.sin(theta)));
		float nz = (float) ((this.y*Math.sin(theta)) + (this.z*Math.cos(theta)));
		float nx = this.z;
		this.x = nx;
		this.y = ny;
		this.z = nz;
		return this;
	}

	public Vec3f rotateAroundY(float theta)
	{
		float nz = (float) ((this.z*Math.cos(theta)) - (this.x*Math.sin(theta)));
		float nx = (float) ((this.z*Math.sin(theta)) + (this.x*Math.cos(theta)));
		float ny = this.z;
		this.x = nx;
		this.y = ny;
		this.z = nz;
		return this;
	}
	
	public Vec3f rotateAroundZ(float theta)
	{
		float nx = (float) ((this.x*Math.cos(theta)) - (this.y*Math.sin(theta)));
		float ny = (float) ((this.x*Math.sin(theta)) + (this.y*Math.cos(theta)));
		float nz = this.z;
		this.x = nx;
		this.y = ny;
		this.z = nz;
		return this;
	}

	public Vec3f rotateAroundX(float theta, Vec3f out)
	{
		float ny = (float) ((this.y*Math.cos(theta)) - (this.z*Math.sin(theta)));
		float nz = (float) ((this.y*Math.sin(theta)) + (this.z*Math.cos(theta)));
		float nx = this.z;
		out.x = nx;
		out.y = ny;
		out.z = nz;
		return out;
	}

	public Vec3f rotateAroundY(float theta, Vec3f out)
	{
		float nz = (float) ((this.z*Math.cos(theta)) - (this.x*Math.sin(theta)));
		float nx = (float) ((this.z*Math.sin(theta)) + (this.x*Math.cos(theta)));
		float ny = this.z;
		out.x = nx;
		out.y = ny;
		out.z = nz;
		return out;
	}
	
	public Vec3f rotateAroundZ(float theta, Vec3f out)
	{
		float nx = (float) ((this.x*Math.cos(theta)) - (this.y*Math.sin(theta)));
		float ny = (float) ((this.x*Math.sin(theta)) + (this.y*Math.cos(theta)));
		float nz = this.z;
		out.x = nx;
		out.y = ny;
		out.z = nz;
		return out;
	}
	
	public void clamp(float min, float max)
	{
		Vec3f.clamp(this, min, max, this);
	}
	
}
