package de.longor.lmath.tuples;

import java.nio.FloatBuffer;

import de.longor.lmath.LMath;
import de.longor.lmath.matrices.Mat4f;












public class Quat {
	private static final float PI = (float) Math.PI;
	
	private static final float sin(float omega) {
		return (float) Math.sin(omega);
	}
	
	private static final float cos(float omega) {
		return (float) Math.cos(omega);
	}
	
	private static final float abs(float s) {
		return Math.abs(s);
	}
	
	private static final float sqrt(float f) {
		return (float) Math.sqrt(f);
	}
	
	private static final float sin(double d) {
		return (float) Math.sin(d);
	}
	
	private static final float acos(float cosom) {
		return (float) Math.acos(cosom);
	}
	
	private static final float atan(float f) {
		return (float) Math.atan(f);
	}
	
	
	
	
	
	
	
	// components of a quaternion
	public float w, x, y, z;
	
	/** default constructor **/
	public Quat() {
		this.w = 1.0f;
		this.x = 0.0f;
		this.y = 0.0f;
		this.z = 0.0f;
	}
	
	/** copy constructor **/
	public Quat(Quat quat) {
		this.w = quat.w;
		this.x = quat.x;
		this.y = quat.y;
		this.z = quat.z;
	}
	
	/** initialized constructor **/
	public Quat(float w, float x, float y, float z) {
		this.w = w;
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/** initialized constructor **/
	public Quat(double w, double x, double y, double z) {
		this.w = (float) w;
		this.x = (float) x;
		this.y = (float) y;
		this.z = (float) z;
	}
	
	public final Quat set(float x, float y, float z, float w)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
		return this;
	}
	
	/**
	 * Multiplies this quaternion and the given one together.
	 * Saves result in this Quaternion and returns it.
	 **/
	public Quat mul(Quat q)
	{
		float ow = (this.w * q.w) - ((this.x * q.x) + (this.y * q.y) + (this.z * q.z));
		float ox = ((this.w * q.x) + (q.w * this.x) + (this.y * q.z)) - (this.z * q.y);
		float oy = ((this.w * q.y) + (q.w * this.y) + (this.z * q.x)) - (this.x * q.z);
		float oz = ((this.w * q.z) + (q.w * this.z) + (this.x * q.y)) - (this.y * q.x);
		
		this.x = ox;
		this.y = oy;
		this.z = oz;
		this.w = ow;
		
		return this;
	}
	
	/**
	 * Multiplies this quaternion and the given one together.
	 * Saves result in given Quaternion and returns it.
	 **/
	public Quat mul(Quat q, Quat qo)
	{
		float ow = (this.w * q.w) - ((this.x * q.x) + (this.y * q.y) + (this.z * q.z));
		float ox = ((this.w * q.x) + (q.w * this.x) + (this.y * q.z)) - (this.z * q.y);
		float oy = ((this.w * q.y) + (q.w * this.y) + (this.z * q.x)) - (this.x * q.z);
		float oz = ((this.w * q.z) + (q.w * this.z) + (this.x * q.y)) - (this.y * q.x);
		
		qo.x = ox;
		qo.y = oy;
		qo.z = oz;
		qo.w = ow;
		
		return qo;
	}
	
	/** Conjungates this quaternion, and saves the result in this Quaternion. **/
	public Quat conjugate()
	{
		this.x = -this.x;
		this.y = -this.y;
		this.z = -this.z;
		return this;
	}
	
	/** Conjungates this quaternion, and saves the result in the given Quaternion. **/
	public Quat conjugate(Quat q)
	{
		q.x = -this.x;
		q.y = -this.y;
		q.z = -this.z;
		q.w = this.w;
		return q;
	}
	
	/** Inverts this Quaternion, saves result in this Quaternion. **/
	public Quat invert()
	{
		float norme = sqrt((this.w * this.w) + (this.x * this.x) + (this.y * this.y) + (this.z * this.z));
		
		if (norme == 0.0) {
			norme = 1.0f;
		}
		
		float recip = 1.0f / norme;
		
		this.w = this.w * recip;
		this.x = -this.x * recip;
		this.y = -this.y * recip;
		this.z = -this.z * recip;
		
		return this;
	}
	
	/** Inverts this Quaternion, saves result in the given Quaternion. **/
	public Quat invert(Quat qo)
	{
		float norme = sqrt((this.w * this.w) + (this.x * this.x) + (this.y * this.y) + (this.z * this.z));
		
		if (norme == 0.0) {
			norme = 1.0f;
		}
		
		float recip = 1.0f / norme;
		
		qo.w = this.w * recip;
		qo.x = -this.x * recip;
		qo.y = -this.y * recip;
		qo.z = -this.z * recip;
		
		return qo;
	}
	
	/** Normalizes this quaternion, and saves the result in this Quaternion. **/
	public Quat normalize()
	{
		float norme = sqrt((this.w * this.w) + (this.x * this.x) + (this.y * this.y) + (this.z * this.z));
		
		if (norme == 0.0) {
			this.w = 1.0f;
			this.x = this.y = this.z = 0.0f;
		} else {
			float recip = 1.0f / norme;
			
			this.w = this.x * recip;
			this.x = this.x * recip;
			this.y = this.y * recip;
			this.z = this.z * recip;
		}
		
		return this;
	}
	
	/** Normalizes this quaternion, and saves the result in the given Quaternion. **/
	public Quat normalize(Quat qo)
	{
		float norme = sqrt((this.w * this.w) + (this.x * this.x) + (this.y * this.y) + (this.z * this.z));
		
		if (norme == 0.0) {
			qo.w = 1.0f;
			qo.x = qo.y = qo.z = 0.0f;
		} else {
			float recip = 1.0f / norme;
			
			qo.w = this.x * recip;
			qo.x = this.x * recip;
			qo.y = this.y * recip;
			qo.z = this.z * recip;
		}
		
		return qo;
	}
	
	/**
	 * Dot-Product between this and the given quaternion.
	 *
	 * @param other The quaternion to calculate the dot-product with.
	 */
	public float dotproduct(Quat other)
	{
		return (this.x * other.x) + (this.y * other.y) + (this.z * other.z) + (this.w * other.w);
	}
	
	/** Makes quaternion from axis. (wat?)**/
	public Quat fromAxis(float Angle, float x, float y, float z)
	{
		float omega, s, c;
		
		s = sqrt((x * x) + (y * y) + (z * z));
		
		if (abs(s) > Float.MIN_VALUE) {
			c = 1.0f / s;
			
			x *= c;
			y *= c;
			z *= c;
			
			omega = -0.5f * Angle;
			s = sin(omega);
			
			this.x = s * x;
			this.y = s * y;
			this.z = s * z;
			this.w = cos(omega);
		} else {
			this.x = this.y = 0.0f;
			this.z = 0.0f;
			this.w = 1.0f;
		}
		
		this.normalize();
		return this;
	}
	
	public Quat fromAxis(float Angle, Vec3f axis)
	{
		return this.fromAxis(Angle, axis.x, axis.y, axis.z);
	}
	
	public Quat fromAxis(float Angle, Vec4f axis)
	{
		return this.fromAxis(Angle, axis.x, axis.y, axis.z);
	}
	
	/** Rotates quaternion towards other quaternion by the given fraction, saves result in this quaternion. **/
	public void slerp(Quat a, Quat b, float t)
	{
		float omega, cosom, sinom, sclp, sclq;
		// int i;
		
		cosom = (a.x * b.x) + (a.y * b.y) + (a.z * b.z) + (a.w * b.w);
		
		if ((1.0f + cosom) > Float.MIN_VALUE) {
			if ((1.0f - cosom) > Float.MIN_VALUE) {
				omega = acos(cosom);
				sinom = sin(omega);
				sclp = sin((1.0f - t) * omega) / sinom;
				sclq = sin(t * omega) / sinom;
			} else {
				sclp = 1.0f - t;
				sclq = t;
			}
			
			this.x = (sclp * a.x) + (sclq * b.x);
			this.y = (sclp * a.y) + (sclq * b.y);
			this.z = (sclp * a.z) + (sclq * b.z);
			this.w = (sclp * a.w) + (sclq * b.w);
		} else {
			this.x = -a.y;
			this.y = a.x;
			this.z = -a.w;
			this.w = a.z;
			
			sclp = sin((1.0f - t) * PI * 0.5);
			sclq = sin(t * PI * 0.5);
			
			this.x = (sclp * a.x) + (sclq * b.x);
			this.y = (sclp * a.y) + (sclq * b.y);
			this.z = (sclp * a.z) + (sclq * b.z);
		}
	}
	
	/** Rotates quaternion towards other quaternion by the given fraction, saves result in the given quaternion. **/
	public void slerp(Quat a, Quat b, float t, Quat qout)
	{
		float omega, cosom, sinom, sclp, sclq;
		// int i;
		
		cosom = (a.x * b.x) + (a.y * b.y) + (a.z * b.z) + (a.w * b.w);
		
		if ((1.0f + cosom) > Float.MIN_VALUE) {
			if ((1.0f - cosom) > Float.MIN_VALUE) {
				omega = acos(cosom);
				sinom = sin(omega);
				sclp = sin((1.0f - t) * omega) / sinom;
				sclq = sin(t * omega) / sinom;
			} else {
				sclp = 1.0f - t;
				sclq = t;
			}
			
			qout.x = (sclp * a.x) + (sclq * b.x);
			qout.y = (sclp * a.y) + (sclq * b.y);
			qout.z = (sclp * a.z) + (sclq * b.z);
			qout.w = (sclp * a.w) + (sclq * b.w);
		} else {
			qout.x = -a.y;
			qout.y = a.x;
			qout.z = -a.w;
			qout.w = a.z;
			
			sclp = sin((1.0f - t) * PI * 0.5);
			sclq = sin(t * PI * 0.5);
			
			qout.x = (sclp * a.x) + (sclq * b.x);
			qout.y = (sclp * a.y) + (sclq * b.y);
			qout.z = (sclp * a.z) + (sclq * b.z);
		}
	}

	public Quat exp()
	{
		float Mul;
		float Length = sqrt((this.x * this.x) + (this.y * this.y) + (this.z * this.z));
		
		if (Length > 1.0e-4) {
			Mul = sin(Length) / Length;
		} else {
			Mul = 1.0f;
		}
		
		this.w = cos(Length);
		this.x *= Mul;
		this.y *= Mul;
		this.z *= Mul;
		
		return this;
	}

	public Quat exp(Quat qout)
	{
		float Mul;
		float Length = sqrt((this.x * this.x) + (this.y * this.y) + (this.z * this.z));
		
		if (Length > 1.0e-4) {
			Mul = sin(Length) / Length;
		} else {
			Mul = 1.0f;
		}
		
		qout.w = cos(Length);
		qout.x *= Mul;
		qout.y *= Mul;
		qout.z *= Mul;
		
		return this;
	}

	public Quat log()
	{
		float Length;
		
		Length = sqrt((this.x * this.x) + (this.y * this.y) + (this.z * this.z));
		Length = atan(Length / this.w);
		
		this.w = 0.0f;
		
		this.x *= Length;
		this.y *= Length;
		this.z *= Length;
		
		return this;
	}

	public Quat log(Quat qout)
	{
		float Length;
		
		Length = sqrt((this.x * this.x) + (this.y * this.y) + (this.z * this.z));
		Length = atan(Length / this.w);
		
		qout.w = 0.0f;
		
		qout.x = this.x * Length;
		qout.y = this.y * Length;
		qout.z = this.z * Length;
		
		return this;
	}
	
	public final Quat rotationFromRadians(float yaw, float pitch, float roll)
	{
	    // Assuming the angles are in radians.
	    double c1 = Math.cos(yaw/2);
	    double s1 = Math.sin(yaw/2);
	    double c2 = Math.cos(pitch/2);
	    double s2 = Math.sin(pitch/2);
	    double c3 = Math.cos(roll/2);
	    double s3 = Math.sin(roll/2);
	    double c1c2 = c1*c2;
	    double s1s2 = s1*s2;
	    
	    this.w = (float) ((c1c2*c3) - (s1s2*s3));
	  	this.x = (float) ((c1c2*s3) + (s1s2*c3));
		this.y = (float) ((s1*c2*c3) + (c1*s2*s3));
		this.z = (float) ((c1*s2*c3) - (s1*c2*s3));
		
		return this;
	}
	
	public final Quat rotationFromDegrees(float yaw, float pitch, float roll)
	{
		yaw *= LMath.PI180;
		pitch *= LMath.PI180;
		roll *= LMath.PI180;
		
	    // Assuming the angles are in radians.
	    double c1 = Math.cos(yaw/2);
	    double s1 = Math.sin(yaw/2);
	    double c2 = Math.cos(pitch/2);
	    double s2 = Math.sin(pitch/2);
	    double c3 = Math.cos(roll/2);
	    double s3 = Math.sin(roll/2);
	    double c1c2 = c1*c2;
	    double s1s2 = s1*s2;
	    
	    this.w = (float) ((c1c2*c3) - (s1s2*s3));
	  	this.x = (float) ((c1c2*s3) + (s1s2*c3));
		this.y = (float) ((s1*c2*c3) + (c1*s2*s3));
		this.z = (float) ((c1*s2*c3) - (s1*c2*s3));
		
		return this;
	}
	
	public final Quat rotationFromRadians(double yaw, double pitch, double roll)
	{
	    // Assuming the angles are in radians.
	    double c1 = Math.cos(yaw/2);
	    double s1 = Math.sin(yaw/2);
	    double c2 = Math.cos(pitch/2);
	    double s2 = Math.sin(pitch/2);
	    double c3 = Math.cos(roll/2);
	    double s3 = Math.sin(roll/2);
	    double c1c2 = c1*c2;
	    double s1s2 = s1*s2;
	    
	    this.w = (float) ((c1c2*c3) - (s1s2*s3));
	  	this.x = (float) ((c1c2*s3) + (s1s2*c3));
		this.y = (float) ((s1*c2*c3) + (c1*s2*s3));
		this.z = (float) ((c1*s2*c3) - (s1*c2*s3));
		
		return this;
	}
	
	public final Quat rotationFromDegrees(double yaw, double pitch, double roll)
	{
		yaw *= LMath.PI180;
		pitch *= LMath.PI180;
		roll *= LMath.PI180;
		
	    // Assuming the angles are in radians.
	    double c1 = Math.cos(yaw/2);
	    double s1 = Math.sin(yaw/2);
	    double c2 = Math.cos(pitch/2);
	    double s2 = Math.sin(pitch/2);
	    double c3 = Math.cos(roll/2);
	    double s3 = Math.sin(roll/2);
	    double c1c2 = c1*c2;
	    double s1s2 = s1*s2;
	    
	    this.w = (float) ((c1c2*c3) - (s1s2*s3));
	  	this.x = (float) ((c1c2*s3) + (s1s2*c3));
		this.y = (float) ((s1*c2*c3) + (c1*s2*s3));
		this.z = (float) ((c1*s2*c3) - (s1*c2*s3));
		
		return this;
	}
	
	public Vec3f rotateVectorAroundQuaternion(Vec3f in, Vec3f out)
	{
		Quat qvec = new Quat(0, in.x, in.y, in.z);
		Quat qinverse = this.invert(new Quat());
		Quat result = new Quat();
		
		qvec.mul(qinverse, result);
		this.mul(result, result);
		
		out.setXYZ(result.x,result.y,result.z);
		return out;
	}
	
	public Vec4f rotateVectorAroundQuaternion(Vec4f in, Vec4f out)
	{
		Quat qvec = new Quat(0, in.x, in.y, in.z);
		Quat qinverse = this.invert(new Quat());
		Quat result = new Quat();
		
		qvec.mul(qinverse, result);
		this.mul(result, result);
		
		out.setXYZ(result.x,result.y,result.z);
		return out;
	}
	
	/**
	 * Converts a quaternion rotation operator into a matrix.
	 */
	public void toMatrix(FloatBuffer buf)
	{
		float x2, y2, z2, xx, xy, xz, yy, yz, zz, wx, wy, wz;
		// float[] _data = new float[16];
		
		// calculate coefficients
		x2 = this.x + this.x;
		y2 = this.y + this.y;
		z2 = this.z + this.z;
		
		xx = this.x * x2;   xy = this.x * y2;   xz = this.x * z2;
		yy = this.y * y2;   yz = this.y * z2;   zz = this.z * z2;
		wx = this.w * x2;   wy = this.w * y2;   wz = this.w * z2;
		
		buf.put(1.0f - (yy + zz));
		buf.put(xy - wz);
		buf.put(xz + wy);
		buf.put(0.0f);
		
		buf.put(xy + wz);
		buf.put(1.0f - (xx + zz));
		buf.put(yz - wx);
		buf.put(0.0f);
		
		buf.put(xz - wy);
		buf.put(yz + wx);
		buf.put(1.0f - (xx + yy));
		buf.put(0.0f);
		
		buf.put(0.0f);
		buf.put(0.0f);
		buf.put(0.0f);
		buf.put(1.0f);
	}
	
	/**
	 * Converts a quaternion rotation operator into a matrix.
	 */
	public float[] toMatrix()
	{
		float x2, y2, z2, xx, xy, xz, yy, yz, zz, wx, wy, wz;
		float[] _data = new float[16];
		
		// calculate coefficients
		x2 = this.x + this.x;
		y2 = this.y + this.y;
		z2 = this.z + this.z;
		
		xx = this.x * x2;   xy = this.x * y2;   xz = this.x * z2;
		yy = this.y * y2;   yz = this.y * z2;   zz = this.z * z2;
		wx = this.w * x2;   wy = this.w * y2;   wz = this.w * z2;
		
		_data[0] = 1.0f - (yy + zz);
		_data[1] = xy - wz;
		_data[2] = xz + wy;
		_data[3] = 0.0f;
		
		_data[4] = xy + wz;
		_data[5] = 1.0f - (xx + zz);
		_data[6] = yz - wx;
		_data[7] = 0.0f;
		
		_data[8] = xz - wy;
		_data[9] = yz + wx;
		_data[10] = 1.0f - (xx + yy);
		_data[11] = 0.0f;
		
		_data[12] = 0.0f;
		_data[13] = 0.0f;
		_data[14] = 0.0f;
		_data[15] = 1.0f;
		
		return _data;
	}
	
	/**
	 * Converts a quaternion rotation operator into a matrix.
	 */
	public float[] toMatrix(float[] _data)
	{
		float x2, y2, z2, xx, xy, xz, yy, yz, zz, wx, wy, wz;
		
		// calculate coefficients
		x2 = this.x + this.x;
		y2 = this.y + this.y;
		z2 = this.z + this.z;
		
		xx = this.x * x2;   xy = this.x * y2;   xz = this.x * z2;
		yy = this.y * y2;   yz = this.y * z2;   zz = this.z * z2;
		wx = this.w * x2;   wy = this.w * y2;   wz = this.w * z2;
		
		_data[0] = 1.0f - (yy + zz);
		_data[1] = xy - wz;
		_data[2] = xz + wy;
		_data[3] = 0.0f;
		
		_data[4] = xy + wz;
		_data[5] = 1.0f - (xx + zz);
		_data[6] = yz - wx;
		_data[7] = 0.0f;
		
		_data[8] = xz - wy;
		_data[9] = yz + wx;
		_data[10] = 1.0f - (xx + yy);
		_data[11] = 0.0f;
		
		_data[12] = 0.0f;
		_data[13] = 0.0f;
		_data[14] = 0.0f;
		_data[15] = 1.0f;
		
		return _data;
	}
	
	/**
	 * Converts a quaternion rotation operator into a matrix.
	 */
	public Mat4f toMatrix(Mat4f out)
	{
		float x2, y2, z2, xx, xy, xz, yy, yz, zz, wx, wy, wz;
		
		// calculate coefficients
		x2 = this.x + this.x;
		y2 = this.y + this.y;
		z2 = this.z + this.z;
		
		xx = this.x * x2;   xy = this.x * y2;   xz = this.x * z2;
		yy = this.y * y2;   yz = this.y * z2;   zz = this.z * z2;
		wx = this.w * x2;   wy = this.w * y2;   wz = this.w * z2;
		
		out.m00 = 1.0f - (yy + zz);
		out.m01 = xy - wz;
		out.m02 = xz + wy;
		out.m03 = 0.0f;
		
		out.m10 = xy + wz;
		out.m11 = 1.0f - (xx + zz);
		out.m12 = yz - wx;
		out.m13 = 0.0f;
		
		out.m20 = xz - wy;
		out.m21 = yz + wx;
		out.m22 = 1.0f - (xx + yy);
		out.m23 = 0.0f;
		
		out.m30 = 0.0f;
		out.m31 = 0.0f;
		out.m32 = 0.0f;
		out.m33 = 1.0f;
		
		return out;
	}

}
