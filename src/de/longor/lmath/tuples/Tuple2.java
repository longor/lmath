package de.longor.lmath.tuples;

/**
 * A 2-component Tuple.
 * A Tuple always only holds data, and as such this interface only defines data access.
 *
 * @author Longor1996
 **/
public interface Tuple2
{
	/**
	 * Returns the x-component of this tuple as float.
	 * @return The x-component of this tuple as float.
	 **/
	public float getXf();

	/**
	 * Returns the x-component of this tuple as double.
	 * @return The x-component of this tuple as double.
	 **/
	public double getXd();
	/**
	 * Returns the y-component of this tuple as float.
	 * @return The y-component of this tuple as float.
	 **/
	public float getYf();
	/**
	 * Returns the y-component of this tuple as double.
	 * @return The y-component of this tuple as double.
	 **/
	public double getYd();
	
	
	
	
	/**
	 * Sets the x-component of this tuple to the given value.
	 * @param value The given value.
	 **/
	public void setX(float value);
	
	/**
	 * Sets the x-component of this tuple to the given value.
	 * @param value The given value.
	 **/
	public void setX(double value);
	
	/**
	 * Sets the y-component of this tuple to the given value.
	 * @param value The given value.
	 **/
	public void setY(float value);
	
	/**
	 * Sets the y-component of this tuple to the given value.
	 * @param value The given value.
	 **/
	public void setY(double value);
	
	/**
	 * Sets the x- and y-components of this tuple to the given values.
	 * @param X The given value for the x-component.
	 * @param Y The given value for the y-component.
	 **/
	public void setXY(float X, float Y);
	
	/**
	 * Sets the x- and y-components of this tuple to the given values.
	 * @param X The given value for the x-component.
	 * @param Y The given value for the y-component.
	 **/
	public void setXY(double X, double Y);
	
	/**
	 * Sets the x- and y-components of this tuple to the given value.
	 * @param XY The given value.
	 **/
	public void setXY(float XY);
	
	/**
	 * Sets the x- and y-components of this tuple to the given value.
	 * @param XY The given value.
	 **/
	public void setXY(double XY);
	
	/**
	 * Copies the X and Y values of the given tuple into this Tuple2.
	 * @param tuple The tuple to copy the values from.
	 **/
	public void setXY(Tuple2 tuple);
	
	/**
	 * Copies the X and Y values of the given tuple into this Tuple2.
	 * @param tuple The tuple to copy the values from.
	 **/
	public void setXY(Tuple3 tuple);
	
	/**
	 * Copies the X and Y values of the given tuple into this Tuple2.
	 * @param tuple The tuple to copy the values from.
	 **/
	public void setXY(Tuple4 tuple);
}
