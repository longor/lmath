package de.longor.lmath.tuples;

import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.util.Iterator;

/**
 * A utility-class that handles the unwrapping of data from various kinds of tuples.
 */
public class TupleUnwrap
{
	
	/**
	 * Unwrap2f to buffer.
	 *
	 * @param vec the vec
	 * @param buffer the buffer
	 * @return the float buffer
	 */
	public static final FloatBuffer unwrap2fToBuffer(Vec2f vec, FloatBuffer buffer)
	{
		buffer.put(vec.x);
		buffer.put(vec.y);
		return buffer;
	}
	
	/**
	 * Unwrap2d to buffer.
	 *
	 * @param vec the vec
	 * @param buffer the buffer
	 * @return the double buffer
	 */
	public static final DoubleBuffer unwrap2dToBuffer(Vec2d vec, DoubleBuffer buffer)
	{
		buffer.put(vec.x);
		buffer.put(vec.y);
		return buffer;
	}
	
	/**
	 * Unwrap3f to buffer.
	 *
	 * @param vec the vec
	 * @param buffer the buffer
	 * @return the float buffer
	 */
	public static final FloatBuffer unwrap3fToBuffer(Vec3f vec, FloatBuffer buffer)
	{
		buffer.put(vec.x);
		buffer.put(vec.y);
		buffer.put(vec.z);
		return buffer;
	}
	
	/**
	 * Unwrap3d to buffer.
	 *
	 * @param vec the vec
	 * @param buffer the buffer
	 * @return the double buffer
	 */
	public static final DoubleBuffer unwrap3dToBuffer(Vec3d vec, DoubleBuffer buffer)
	{
		buffer.put(vec.x);
		buffer.put(vec.y);
		buffer.put(vec.z);
		return buffer;
	}
	
	/**
	 * Unwrap4f to buffer.
	 *
	 * @param vec the vec
	 * @param buffer the buffer
	 * @return the float buffer
	 */
	public static final FloatBuffer unwrap4fToBuffer(Vec4f vec, FloatBuffer buffer)
	{
		buffer.put(vec.x);
		buffer.put(vec.y);
		buffer.put(vec.z);
		buffer.put(vec.w);
		return buffer;
	}
	
	/**
	 * Unwrap4d to buffer.
	 *
	 * @param vec the vec
	 * @param buffer the buffer
	 * @return the double buffer
	 */
	public static final DoubleBuffer unwrap4dToBuffer(Vec4d vec, DoubleBuffer buffer)
	{
		buffer.put(vec.x);
		buffer.put(vec.y);
		buffer.put(vec.z);
		buffer.put(vec.w);
		return buffer;
	}
	
	/**
	 * Unwrap2f to stream.
	 *
	 * @param vec the vec
	 * @param output the output
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final void unwrap2fToStream(Vec2f vec, DataOutputStream output) throws IOException
	{
		output.writeFloat(vec.x);
		output.writeFloat(vec.y);
	}
	
	/**
	 * Unwrap2d to stream.
	 *
	 * @param vec the vec
	 * @param output the output
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final void unwrap2dToStream(Vec2d vec, DataOutputStream output) throws IOException
	{
		output.writeDouble(vec.x);
		output.writeDouble(vec.y);
	}
	
	/**
	 * Unwrap3f to stream.
	 *
	 * @param vec the vec
	 * @param output the output
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final void unwrap3fToStream(Vec3f vec, DataOutputStream output) throws IOException
	{
		output.writeFloat(vec.x);
		output.writeFloat(vec.y);
		output.writeFloat(vec.z);
	}
	
	/**
	 * Unwrap3d to stream.
	 *
	 * @param vec the vec
	 * @param output the output
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final void unwrap3dToStream(Vec3d vec, DataOutputStream output) throws IOException
	{
		output.writeDouble(vec.x);
		output.writeDouble(vec.y);
		output.writeDouble(vec.z);
	}
	
	/**
	 * Unwrap4f to stream.
	 *
	 * @param vec the vec
	 * @param output the output
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final void unwrap4fToStream(Vec4f vec, DataOutputStream output) throws IOException
	{
		output.writeFloat(vec.x);
		output.writeFloat(vec.y);
		output.writeFloat(vec.z);
		output.writeFloat(vec.w);
	}
	
	/**
	 * Unwrap4d to stream.
	 *
	 * @param vec the vec
	 * @param output the output
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final void unwrap4dToStream(Vec4d vec, DataOutputStream output) throws IOException
	{
		output.writeDouble(vec.x);
		output.writeDouble(vec.y);
		output.writeDouble(vec.z);
		output.writeDouble(vec.w);
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Unwrap2f to array.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap2fToArray(Vec2f[] in, float[] out)
	{
		int vectors = in.length;
		Vec2f vec = null;
		
		for(int i = 0, j = 0; i < vectors; i++)
		{
			vec = in[i];
			out[j++] = vec.x;
			out[j++] = vec.y;
		}
	}
	
	/**
	 * Unwrap2f to buffer.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap2fToBuffer(Vec2f[] in, FloatBuffer out)
	{
		int vectors = in.length;
		Vec2f vec = null;
		
		for(int i = 0; i < vectors; i++)
		{
			vec = in[i];
			out.put(vec.x);
			out.put(vec.y);
		}
	}
	
	/**
	 * Unwrap2f to stream.
	 *
	 * @param in the in
	 * @param out the out
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final void unwrap2fToStream(Vec2f[] in, DataOutputStream out) throws IOException
	{
		int vectors = in.length;
		Vec2f vec = null;
		
		for(int i = 0; i < vectors; i++)
		{
			vec = in[i];
			out.writeFloat(vec.x);
			out.writeFloat(vec.y);
		}
	}
	
	/**
	 * Unwrap2d to array.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap2dToArray(Vec2d[] in, double[] out)
	{
		int vectors = in.length;
		Vec2d vec = null;
		
		for(int i = 0, j = 0; i < vectors; i++)
		{
			vec = in[i];
			out[j++] = vec.x;
			out[j++] = vec.y;
		}
	}
	
	/**
	 * Unwrap2d to buffer.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap2dToBuffer(Vec2d[] in, DoubleBuffer out)
	{
		int vectors = in.length;
		Vec2d vec = null;
		
		for(int i = 0; i < vectors; i++)
		{
			vec = in[i];
			out.put(vec.x);
			out.put(vec.y);
		}
	}
	
	/**
	 * Unwrap2d to stream.
	 *
	 * @param in the in
	 * @param out the out
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final void unwrap2dToStream(Vec2d[] in, DataOutputStream out) throws IOException
	{
		int vectors = in.length;
		Vec2d vec = null;
		
		for(int i = 0; i < vectors; i++)
		{
			vec = in[i];
			out.writeDouble(vec.x);
			out.writeDouble(vec.y);
		}
	}
	
	/**
	 * Unwrap3f to array.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap3fToArray(Vec3f[] in, float[] out)
	{
		int vectors = in.length;
		Vec3f vec = null;
		
		for(int i = 0, j = 0; i < vectors; i++)
		{
			vec = in[i];
			out[j++] = vec.x;
			out[j++] = vec.y;
			out[j++] = vec.z;
		}
	}
	
	/**
	 * Unwrap3f to buffer.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap3fToBuffer(Vec3f[] in, FloatBuffer out)
	{
		int vectors = in.length;
		Vec3f vec = null;
		
		for(int i = 0; i < vectors; i++)
		{
			vec = in[i];
			out.put(vec.x);
			out.put(vec.y);
			out.put(vec.z);
		}
	}
	
	/**
	 * Unwrap3f to stream.
	 *
	 * @param in the in
	 * @param out the out
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final void unwrap3fToStream(Vec3f[] in, DataOutputStream out) throws IOException
	{
		int vectors = in.length;
		Vec3f vec = null;
		
		for(int i = 0; i < vectors; i++)
		{
			vec = in[i];
			out.writeFloat(vec.x);
			out.writeFloat(vec.y);
			out.writeFloat(vec.z);
		}
	}
	
	/**
	 * Unwrap3d to array.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap3dToArray(Vec3d[] in, double[] out)
	{
		int vectors = in.length;
		Vec3d vec = null;
		
		for(int i = 0, j = 0; i < vectors; i++)
		{
			vec = in[i];
			out[j++] = vec.x;
			out[j++] = vec.y;
			out[j++] = vec.z;
		}
	}
	
	/**
	 * Unwrap3d to buffer.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap3dToBuffer(Vec3d[] in, DoubleBuffer out)
	{
		int vectors = in.length;
		Vec3d vec = null;
		
		for(int i = 0; i < vectors; i++)
		{
			vec = in[i];
			out.put(vec.x);
			out.put(vec.y);
			out.put(vec.z);
		}
	}
	
	/**
	 * Unwrap3d to buffer.
	 *
	 * @param in the in
	 * @param out the out
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final void unwrap3dToBuffer(Vec3d[] in, DataOutputStream out) throws IOException
	{
		int vectors = in.length;
		Vec3d vec = null;
		
		for(int i = 0; i < vectors; i++)
		{
			vec = in[i];
			out.writeDouble(vec.x);
			out.writeDouble(vec.y);
			out.writeDouble(vec.z);
		}
	}
	
	/**
	 * Unwrap4f to buffer.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap4fToBuffer(Vec4f[] in, float[] out)
	{
		int vectors = in.length;
		Vec4f vec = null;
		
		for(int i = 0, j = 0; i < vectors; i++)
		{
			vec = in[i];
			out[j++] = vec.x;
			out[j++] = vec.y;
			out[j++] = vec.z;
			out[j++] = vec.w;
		}
	}
	
	/**
	 * Unwrap4f to buffer.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap4fToBuffer(Vec4f[] in, FloatBuffer out)
	{
		int vectors = in.length;
		Vec4f vec = null;
		
		for(int i = 0; i < vectors; i++)
		{
			vec = in[i];
			out.put(vec.x);
			out.put(vec.y);
			out.put(vec.z);
			out.put(vec.w);
		}
	}
	
	/**
	 * Unwrap4f to stream.
	 *
	 * @param in the in
	 * @param out the out
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final void unwrap4fToStream(Vec4f[] in, DataOutputStream out) throws IOException
	{
		int vectors = in.length;
		Vec4f vec = null;
		
		for(int i = 0; i < vectors; i++)
		{
			vec = in[i];
			out.writeFloat(vec.x);
			out.writeFloat(vec.y);
			out.writeFloat(vec.z);
			out.writeFloat(vec.w);
		}
	}
	
	/**
	 * Unwrap4d to array.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap4dToArray(Vec4d[] in, double[] out)
	{
		int vectors = in.length;
		Vec4d vec = null;
		
		for(int i = 0, j = 0; i < vectors; i++)
		{
			vec = in[i];
			out[j++] = vec.x;
			out[j++] = vec.y;
			out[j++] = vec.z;
			out[j++] = vec.w;
		}
	}
	
	/**
	 * Unwrap4d to buffer.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap4dToBuffer(Vec4d[] in, DoubleBuffer out)
	{
		int vectors = in.length;
		Vec4d vec = null;
		
		for(int i = 0; i < vectors; i++)
		{
			vec = in[i];
			out.put(vec.x);
			out.put(vec.y);
			out.put(vec.z);
			out.put(vec.w);
		}
	}
	
	/**
	 * Unwrap4d to stream.
	 *
	 * @param in the in
	 * @param out the out
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final void unwrap4dToStream(Vec4d[] in, DataOutputStream out) throws IOException
	{
		int vectors = in.length;
		Vec4d vec = null;
		
		for(int i = 0; i < vectors; i++)
		{
			vec = in[i];
			out.writeDouble(vec.x);
			out.writeDouble(vec.y);
			out.writeDouble(vec.z);
			out.writeDouble(vec.w);
		}
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Unwrap2 to array.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap2ToArray(Iterable<Vec2f> in, float[] out)
	{
		Iterator<Vec2f> iterator = in.iterator();
		Vec2f vec = null;
		
		for(int j = 0; iterator.hasNext(); j++, vec = iterator.next())
		{
			out[j++] = vec.x;
			out[j++] = vec.y;
		}
	}
	
	/**
	 * Unwrap2 to buffer.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap2ToBuffer(Iterable<Vec2f> in, FloatBuffer out)
	{
		for(Vec2f vec : in)
		{
			out.put(vec.x);
			out.put(vec.y);
		}
	}
	
	/**
	 * Unwrap2f to stream.
	 *
	 * @param in the in
	 * @param out the out
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final void unwrap2fToStream(Iterable<Vec2f> in, DataOutputStream out) throws IOException
	{
		for(Vec2f vec : in)
		{
			out.writeFloat(vec.x);
			out.writeFloat(vec.y);
		}
	}
	
	/**
	 * Unwrap2 to array.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap2ToArray(Iterable<Vec2d> in, double[] out)
	{
		Iterator<Vec2d> iterator = in.iterator();
		Vec2d vec = null;
		
		for(int j = 0; iterator.hasNext(); j++, vec = iterator.next())
		{
			out[j++] = vec.x;
			out[j++] = vec.y;
		}
	}
	
	/**
	 * Unwrap2 to buffer.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap2ToBuffer(Iterable<Vec2d> in, DoubleBuffer out)
	{
		for(Vec2d vec : in)
		{
			out.put(vec.x);
			out.put(vec.y);
		}
	}
	
	/**
	 * Unwrap2d to stream.
	 *
	 * @param in the in
	 * @param out the out
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final void unwrap2dToStream(Iterable<Vec2d> in, DataOutputStream out) throws IOException
	{
		for(Vec2d vec : in)
		{
			out.writeDouble(vec.x);
			out.writeDouble(vec.y);
		}
	}
	
	/**
	 * Unwrap3 to array.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap3ToArray(Iterable<Vec3f> in, float[] out)
	{
		Iterator<Vec3f> iterator = in.iterator();
		Vec3f vec = null;
		
		for(int j = 0; iterator.hasNext(); j++, vec = iterator.next())
		{
			out[j++] = vec.x;
			out[j++] = vec.y;
			out[j++] = vec.z;
		}
	}
	
	/**
	 * Unwrap3 to buffer.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap3ToBuffer(Iterable<Vec3f> in, FloatBuffer out)
	{
		for(Vec3f vec : in)
		{
			out.put(vec.x);
			out.put(vec.y);
			out.put(vec.z);
		}
	}
	
	/**
	 * Unwrap3f to stream.
	 *
	 * @param in the in
	 * @param out the out
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final void unwrap3fToStream(Iterable<Vec3f> in, DataOutputStream out) throws IOException
	{
		for(Vec3f vec : in)
		{
			out.writeFloat(vec.x);
			out.writeFloat(vec.y);
			out.writeFloat(vec.z);
		}
	}
	
	/**
	 * Unwrap3 to array.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap3ToArray(Iterable<Vec3d> in, double[] out)
	{
		Iterator<Vec3d> iterator = in.iterator();
		Vec3d vec = null;
		
		for(int j = 0; iterator.hasNext(); j++, vec = iterator.next())
		{
			out[j++] = vec.x;
			out[j++] = vec.y;
			out[j++] = vec.z;
		}
	}
	
	/**
	 * Unwrap3 to buffer.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap3ToBuffer(Iterable<Vec3d> in, DoubleBuffer out)
	{
		for(Vec3d vec : in)
		{
			out.put(vec.x);
			out.put(vec.y);
			out.put(vec.z);
		}
	}
	
	/**
	 * Unwrap3d to stream.
	 *
	 * @param in the in
	 * @param out the out
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final void unwrap3dToStream(Iterable<Vec3d> in, DataOutputStream out) throws IOException
	{
		for(Vec3d vec : in)
		{
			out.writeDouble(vec.x);
			out.writeDouble(vec.y);
			out.writeDouble(vec.z);
		}
	}
	
	/**
	 * Unwrap4 to array.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap4ToArray(Iterable<Vec4f> in, float[] out)
	{
		Iterator<Vec4f> iterator = in.iterator();
		Vec4f vec = null;
		
		for(int j = 0; iterator.hasNext(); j++, vec = iterator.next())
		{
			out[j++] = vec.x;
			out[j++] = vec.y;
			out[j++] = vec.z;
			out[j++] = vec.w;
		}
	}
	
	/**
	 * Unwrap4 to buffer.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap4ToBuffer(Iterable<Vec4f> in, FloatBuffer out)
	{
		for(Vec4f vec : in)
		{
			out.put(vec.x);
			out.put(vec.y);
			out.put(vec.z);
			out.put(vec.w);
		}
	}
	
	/**
	 * Unwrap4f to stream.
	 *
	 * @param in the in
	 * @param out the out
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final void unwrap4fToStream(Iterable<Vec4f> in, DataOutputStream out) throws IOException
	{
		for(Vec4f vec : in)
		{
			out.writeFloat(vec.x);
			out.writeFloat(vec.y);
			out.writeFloat(vec.z);
			out.writeFloat(vec.w);
		}
	}
	
	/**
	 * Unwrap4 to array.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap4ToArray(Iterable<Vec4d> in, double[] out)
	{
		Iterator<Vec4d> iterator = in.iterator();
		Vec4d vec = null;
		
		for(int j = 0; iterator.hasNext(); j++, vec = iterator.next())
		{
			out[j++] = vec.x;
			out[j++] = vec.y;
			out[j++] = vec.z;
			out[j++] = vec.w;
		}
	}
	
	/**
	 * Unwrap4 to buffer.
	 *
	 * @param in the in
	 * @param out the out
	 */
	public static final void unwrap4ToBuffer(Iterable<Vec4d> in, DoubleBuffer out)
	{
		for(Vec4d vec : in)
		{
			out.put(vec.x);
			out.put(vec.y);
			out.put(vec.z);
			out.put(vec.w);
		}
	}
	
	/**
	 * Unwrap4d to stream.
	 *
	 * @param in the in
	 * @param out the out
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final void unwrap4dToStream(Iterable<Vec4d> in, DataOutputStream out) throws IOException
	{
		for(Vec4d vec : in)
		{
			out.writeDouble(vec.x);
			out.writeDouble(vec.y);
			out.writeDouble(vec.z);
			out.writeDouble(vec.w);
		}
	}
	
}
