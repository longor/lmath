package de.longor.lmath.tuples;

/**
 * A 4-component Tuple.
 * A Tuple always only holds data, and as such this interface only defines data access.
 *
 * @author Longor1996
 **/
public interface Tuple4 extends Tuple3
{
	/**
	 * Returns the w-component of this tuple as float.
	 * @return The w-component of this tuple as float.
	 **/
	public float getWf();
	/**
	 * Returns the w-component of this tuple as double.
	 * @return The w-component of this tuple as double.
	 **/
	public double getWd();
	
	/**
	 * Sets the w-component of this tuple to the given value.
	 * @param value The given value.
	 **/
	public void setW(float value);
	
	/**
	 * Sets the w-component of this tuple to the given value.
	 * @param value The given value.
	 **/
	public void setW(double value);
	
	/**
	 * Sets the x-, y-, z- and w-components of this tuple to the given values.
	 * @param X The given value for the x-component.
	 * @param Y The given value for the y-component.
	 * @param Z The given value for the z-component.
	 * @param W The given value for the w-component.
	 **/
	public void setXYZW(float X, float Y, float Z, float W);
	
	/**
	 * Sets the x-, y-, z- and w-components of this tuple to the given values.
	 * @param X The given value for the x-component.
	 * @param Y The given value for the y-component.
	 * @param Z The given value for the z-component.
	 * @param W The given value for the w-component.
	 **/
	public void setXYZW(double X, double Y, double Z, double W);
	
	/**
	 * Sets the x-, y-, z- and w-components of this tuple to the given value.
	 * @param XYZW The given value.
	 **/
	public void setXYZW(float XYZW);
	
	/**
	 * Sets the x-, y-, z- and w-components of this tuple to the given value.
	 * @param XYZW The given value.
	 **/
	public void setXYZW(double XYZW);
	
	/**
	 * Sets the x-, y-, z- and w-components of this tuple to the given values.
	 * @param XYZ The value to set X, Y and Z to.
	 * @param W The value to set W to.
	 **/
	public void setXYZW(float XYZ, float W);
	
	/**
	 * Sets the x-, y-, z- and w-components of this tuple to the given values.
	 * @param XYZ The value to set X, Y and Z to.
	 * @param W The value to set W to.
	 **/
	public void setXYZW(double XYZ, double W);
	
	/**
	 * Copies the X, Y, Z and W values of the given tuple into this Tuple4.
	 * @param tuple The tuple to copy the values from.
	 **/
	public void setXYZW(Tuple4 tuple);
}
