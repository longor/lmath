package de.longor.lmath.tuples;

import de.longor.lmath.LMath;

/**
 * Class for abstracted interaction between tuples.
 **/
public class Vec
{
	
	/**
	 * Adds the.
	 *
	 * @param a the a
	 * @param b the b
	 * @param out the out
	 */
	public static final void add(Tuple2 a, Tuple2 b, Tuple2 out)
	{
		out.setX(a.getXf() + b.getXf());
		out.setY(a.getYf() + b.getYf());
	}
	
	/**
	 * Adds the.
	 *
	 * @param a the a
	 * @param b the b
	 * @param out the out
	 */
	public static final void add(Tuple3 a, Tuple3 b, Tuple3 out)
	{
		out.setX(a.getXf() + b.getXf());
		out.setY(a.getYf() + b.getYf());
		out.setZ(a.getZf() + b.getZf());
	}
	
	/**
	 * Adds the.
	 *
	 * @param a the a
	 * @param b the b
	 * @param out the out
	 */
	public static final void add(Tuple4 a, Tuple4 b, Tuple4 out)
	{
		out.setX(a.getXf() + b.getXf());
		out.setY(a.getYf() + b.getYf());
		out.setZ(a.getZf() + b.getZf());
		out.setW(a.getWf() + b.getWf());
	}
	
	/**
	 * Subtract.
	 *
	 * @param a the a
	 * @param b the b
	 * @param out the out
	 */
	public static final void subtract(Tuple2 a, Tuple2 b, Tuple2 out)
	{
		out.setX(a.getXf() - b.getXf());
		out.setY(a.getYf() - b.getYf());
	}
	
	/**
	 * Subtract.
	 *
	 * @param a the a
	 * @param b the b
	 * @param out the out
	 */
	public static final void subtract(Tuple3 a, Tuple3 b, Tuple3 out)
	{
		out.setX(a.getXf() - b.getXf());
		out.setY(a.getYf() - b.getYf());
		out.setZ(a.getZf() - b.getZf());
	}
	
	/**
	 * Subtract.
	 *
	 * @param a the a
	 * @param b the b
	 * @param out the out
	 */
	public static final void subtract(Tuple4 a, Tuple4 b, Tuple4 out)
	{
		out.setX(a.getXf() - b.getXf());
		out.setY(a.getYf() - b.getYf());
		out.setZ(a.getZf() - b.getZf());
		out.setW(a.getWf() - b.getWf());
	}
	
	/**
	 * Multiply.
	 *
	 * @param a the a
	 * @param scalar the scalar
	 * @param out the out
	 */
	public static final void multiply(Tuple2 a, float scalar, Tuple2 out)
	{
		out.setX(a.getXf() * scalar);
		out.setY(a.getYf() * scalar);
	}
	
	/**
	 * Multiply.
	 *
	 * @param a the a
	 * @param scalar the scalar
	 * @param out the out
	 */
	public static final void multiply(Tuple3 a, float scalar, Tuple3 out)
	{
		out.setX(a.getXf() * scalar);
		out.setY(a.getYf() * scalar);
		out.setZ(a.getZf() * scalar);
	}
	
	/**
	 * Multiply.
	 *
	 * @param a the a
	 * @param scalar the scalar
	 * @param out the out
	 */
	public static final void multiply(Tuple4 a, float scalar, Tuple4 out)
	{
		out.setX(a.getXf() * scalar);
		out.setY(a.getYf() * scalar);
		out.setZ(a.getZf() * scalar);
		out.setW(a.getWf() * scalar);
	}
	
	/**
	 * Divide.
	 *
	 * @param a the a
	 * @param scalar the scalar
	 * @param out the out
	 */
	public static final void divide(Tuple2 a, float scalar, Tuple2 out)
	{
		scalar = 1F / scalar;
		out.setX(a.getXf() * scalar);
		out.setY(a.getYf() * scalar);
	}
	
	/**
	 * Divide.
	 *
	 * @param a the a
	 * @param scalar the scalar
	 * @param out the out
	 */
	public static final void divide(Tuple3 a, float scalar, Tuple3 out)
	{
		scalar = 1F / scalar;
		out.setX(a.getXf() * scalar);
		out.setY(a.getYf() * scalar);
		out.setZ(a.getZf() * scalar);
	}
	
	/**
	 * Divide.
	 *
	 * @param a the a
	 * @param scalar the scalar
	 * @param out the out
	 */
	public static final void divide(Tuple4 a, float scalar, Tuple4 out)
	{
		scalar = 1F / scalar;
		out.setX(a.getXf() * scalar);
		out.setY(a.getYf() * scalar);
		out.setZ(a.getZf() * scalar);
		out.setW(a.getWf() * scalar);
	}
	
	/**
	 * Lerp.
	 *
	 * @param a the a
	 * @param b the b
	 * @param x the x
	 * @param out the out
	 */
	public static final void lerp(Tuple2 a, Tuple2 b, float x, Tuple2 out)
	{
		out.setX(LMath.lerp(a.getXf(), b.getXf(), x));
		out.setY(LMath.lerp(a.getYf(), b.getYf(), x));
	}
	
	/**
	 * Lerp.
	 *
	 * @param a the a
	 * @param b the b
	 * @param x the x
	 * @param out the out
	 */
	public static final void lerp(Tuple3 a, Tuple3 b, float x, Tuple3 out)
	{
		out.setX(LMath.lerp(a.getXf(), b.getXf(), x));
		out.setY(LMath.lerp(a.getYf(), b.getYf(), x));
		out.setZ(LMath.lerp(a.getZf(), b.getZf(), x));
	}
	
	/**
	 * Lerp.
	 *
	 * @param a the a
	 * @param b the b
	 * @param x the x
	 * @param out the out
	 */
	public static final void lerp(Tuple4 a, Tuple4 b, float x, Tuple4 out)
	{
		out.setX(LMath.lerp(a.getXf(), b.getXf(), x));
		out.setY(LMath.lerp(a.getYf(), b.getYf(), x));
		out.setZ(LMath.lerp(a.getZf(), b.getZf(), x));
		out.setW(LMath.lerp(a.getWf(), b.getWf(), x));
	}
	
	/**
	 * Distance.
	 *
	 * @param a the a
	 * @param b the b
	 * @return the float
	 */
	public static final float distance(Tuple2 a, Tuple2 b)
	{
		float offx = b.getXf() - a.getXf();
		float offy = b.getYf() - a.getYf();
		return (float) Math.sqrt(offx*offx + offy*offy);
	}
	
	/**
	 * Distance.
	 *
	 * @param a the a
	 * @param b the b
	 * @return the float
	 */
	public static final float distance(Tuple3 a, Tuple3 b)
	{
		float offx = b.getXf() - a.getXf();
		float offy = b.getYf() - a.getYf();
		float offz = b.getZf() - a.getZf();
		return (float) Math.sqrt(offx*offx + offy*offy + offz*offz);
	}
	
	/**
	 * Distance.
	 *
	 * @param a the a
	 * @param b the b
	 * @return the float
	 */
	public static final float distance(Tuple4 a, Tuple4 b)
	{
		float offx = b.getXf()*b.getWf() - a.getXf()*a.getWf();
		float offy = b.getYf()*b.getWf() - a.getYf()*a.getWf();
		float offz = b.getZf()*b.getWf() - a.getZf()*a.getWf();
		return (float) Math.sqrt(offx*offx + offy*offy + offz*offz);
	}
	
	/**
	 * Travelerp.
	 *
	 * @param start the start
	 * @param end the end
	 * @param travel the travel
	 * @param out the out
	 * @return the tuple3
	 */
	public static final Tuple3 travelerp(Tuple3 start, Tuple3 end, float travel, Tuple3 out)
	{
		float offx = end.getXf() - start.getXf();
		float offy = end.getYf() - start.getYf();
		float offz = end.getZf() - start.getZf();
		float length = (float) Math.sqrt((offx*offx) + (offy*offy) + (offz*offz));
		offx /= length;
		offy /= length;
		offz /= length;
		offx *= travel;
		offy *= travel;
		offz *= travel;
		out.setX(offx);
		out.setY(offy);
		out.setZ(offz);
		return out;
	}
	
	/**
	 * Travelerp.
	 *
	 * @param start the start
	 * @param end the end
	 * @param travel the travel
	 * @param out the out
	 * @return the tuple4
	 */
	public static final Tuple4 travelerp(Tuple4 start, Tuple4 end, float travel, Tuple4 out)
	{
		float offx = end.getXf() - start.getXf();
		float offy = end.getYf() - start.getYf();
		float offz = end.getZf() - start.getZf();
		float length = (float) Math.sqrt((offx*offx) + (offy*offy) + (offz*offz));
		offx /= length;
		offy /= length;
		offz /= length;
		offx *= travel;
		offy *= travel;
		offz *= travel;
		out.setX(offx);
		out.setY(offy);
		out.setZ(offz);
		return out;
	}
	
}
