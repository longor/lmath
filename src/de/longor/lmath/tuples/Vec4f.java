package de.longor.lmath.tuples;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import de.longor.lmath.LMath;

/**
 * A class representing a 4-component (floating-point) vector.
 */
public class Vec4f implements Tuple4
{
	
	/** The x. */
	public float x;
	
	/** The y. */
	public float y;
	
	/** The z. */
	public float z;
	
	/** The w. */
	public float w;
	
	/**
	 * Instantiates a new vec4f.
	 */
	public Vec4f()
	{
		x = 0;
		y = 0;
		z = 0;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param XYZW the xyzw
	 */
	public Vec4f(int XYZW)
	{
		x = XYZW;
		y = XYZW;
		z = XYZW;
		w = XYZW;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param X the x
	 * @param Y the y
	 */
	public Vec4f(int X, int Y)
	{
		x = X;
		y = Y;
		z = 0;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 */
	public Vec4f(int X, int Y, int Z)
	{
		x = X;
		y = Y;
		z = Z;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 * @param W the w
	 */
	public Vec4f(int X, int Y, int Z, int W)
	{
		x = X;
		y = Y;
		z = Z;
		w = W;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param XYZW the xyzw
	 */
	public Vec4f(float XYZW)
	{
		x = XYZW;
		y = XYZW;
		z = XYZW;
		w = XYZW;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param X the x
	 * @param Y the y
	 */
	public Vec4f(float X, float Y)
	{
		x = X;
		y = Y;
		z = 0;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 */
	public Vec4f(float X, float Y, float Z)
	{
		x = X;
		y = Y;
		z = Z;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 * @param W the w
	 */
	public Vec4f(float X, float Y, float Z, float W)
	{
		x = X;
		y = Y;
		z = Z;
		w = W;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param XYZ the xyz
	 */
	public Vec4f(double XYZ)
	{
		x = (float) XYZ;
		y = (float) XYZ;
		z = (float) XYZ;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param X the x
	 * @param Y the y
	 */
	public Vec4f(double X, double Y)
	{
		x = (float) X;
		y = (float) Y;
		z = 0;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 */
	public Vec4f(double X, double Y, double Z)
	{
		x = (float) X;
		y = (float) Y;
		z = (float) Z;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 * @param W the w
	 */
	public Vec4f(double X, double Y, double Z, double W)
	{
		x = (float) X;
		y = (float) Y;
		z = (float) Z;
		w = (float) W;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param vec the vec
	 */
	public Vec4f(Vec2f vec)
	{
		x = vec.x;
		y = vec.y;
		z = 0;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param vec the vec
	 */
	public Vec4f(Vec3f vec)
	{
		x = vec.x;
		y = vec.y;
		z = vec.z;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param vec the vec
	 */
	public Vec4f(Vec4f vec)
	{
		x = vec.x;
		y = vec.y;
		z = vec.z;
		w = vec.w;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param vec the vec
	 */
	public Vec4f(Vec2d vec)
	{
		x = (float) vec.x;
		y = (float) vec.y;
		z = 0;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param vec the vec
	 */
	public Vec4f(Vec3d vec)
	{
		x = (float) vec.x;
		y = (float) vec.y;
		z = (float) vec.z;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param vec the vec
	 */
	public Vec4f(Vec4d vec)
	{
		x = (float) vec.x;
		y = (float) vec.y;
		z = (float) vec.z;
		w = (float) vec.w;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param tuple the tuple
	 */
	public Vec4f(Tuple2 tuple)
	{
		x = tuple.getXf();
		y = tuple.getYf();
		z = 0;
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param tuple the tuple
	 */
	public Vec4f(Tuple3 tuple)
	{
		x = tuple.getXf();
		y = tuple.getYf();
		z = tuple.getZf();
		w = 0;
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param tuple the tuple
	 */
	public Vec4f(Tuple4 tuple)
	{
		x = tuple.getXf();
		y = tuple.getYf();
		z = tuple.getZf();
		w = tuple.getWf();
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param buffer the buffer
	 */
	public Vec4f(IntBuffer buffer)
	{
		x = buffer.get(0);
		y = buffer.get(1);
		z = buffer.get(2);
		w = buffer.get(2);
	}
	
	/**
	 * Instantiates a new vec4f.
	 *
	 * @param buffer the buffer
	 */
	public Vec4f(FloatBuffer buffer)
	{
		x = buffer.get(0);
		y = buffer.get(1);
		z = buffer.get(2);
		w = buffer.get(2);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getXf()
	 */
	@Override
	public float getXf() {
		return x;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getXd()
	 */
	@Override
	public double getXd() {
		return x;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getYf()
	 */
	@Override
	public float getYf() {
		return y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getYd()
	 */
	@Override
	public double getYd() {
		return y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#getZf()
	 */
	@Override
	public float getZf() {
		return z;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#getZd()
	 */
	@Override
	public double getZd() {
		return z;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#getWf()
	 */
	@Override
	public float getWf() {
		return w;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#getWd()
	 */
	@Override
	public double getWd() {
		return w;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setX(float)
	 */
	@Override
	public void setX(float value) {
		x = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setX(double)
	 */
	@Override
	public void setX(double value) {
		x = (float) value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setY(float)
	 */
	@Override
	public void setY(float value) {
		y = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setY(double)
	 */
	@Override
	public void setY(double value) {
		y = (float) value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setZ(float)
	 */
	@Override
	public void setZ(float value) {
		z = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setZ(double)
	 */
	@Override
	public void setZ(double value) {
		z = (float) value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#setW(float)
	 */
	@Override
	public void setW(float value) {
		w = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#setW(double)
	 */
	@Override
	public void setW(double value) {
		w = (float) value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(float, float)
	 */
	@Override
	public void setXY(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(double, double)
	 */
	@Override
	public void setXY(double x, double y) {
		this.x = (float) x;
		this.y = (float) y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(float, float, float)
	 */
	@Override
	public void setXYZ(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(double, double, double)
	 */
	@Override
	public void setXYZ(double x, double y, double z) {
		this.x = (float) x;
		this.y = (float) y;
		this.z = (float) z;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#setXYZW(float, float, float, float)
	 */
	@Override
	public void setXYZW(float x, float y, float z, float w) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#setXYZW(double, double, double, double)
	 */
	@Override
	public void setXYZW(double x, double y, double z, double w) {
		this.x = (float) x;
		this.y = (float) y;
		this.z = (float) z;
		this.w = (float) w;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(float)
	 */
	@Override
	public void setXY(float xy) {
		this.x = xy;
		this.y = xy;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(double)
	 */
	@Override
	public void setXY(double xy) {
		this.x = (float) xy;
		this.y = (float) xy;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(float)
	 */
	@Override
	public void setXYZ(float xyz) {
		this.x = xyz;
		this.y = xyz;
		this.z = xyz;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(double)
	 */
	@Override
	public void setXYZ(double xyz) {
		this.x = (float) xyz;
		this.y = (float) xyz;
		this.z = (float) xyz;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#setXYZW(float)
	 */
	@Override
	public void setXYZW(float xyzw) {
		this.x = xyzw;
		this.y = xyzw;
		this.z = xyzw;
		this.w = xyzw;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#setXYZW(double)
	 */
	@Override
	public void setXYZW(double xyzw) {
		this.x = (float) xyzw;
		this.y = (float) xyzw;
		this.z = (float) xyzw;
		this.w = (float) xyzw;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#setXYZW(float, float)
	 */
	@Override
	public void setXYZW(float xyz, float w) {
		this.x = (float) xyz;
		this.y = (float) xyz;
		this.z = (float) xyz;
		this.w = (float) w;
	}

	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#setXYZW(double, double)
	 */
	@Override
	public void setXYZW(double xyz, double w) {
		this.x = (float) xyz;
		this.y = (float) xyz;
		this.z = (float) xyz;
		this.w = (float) w;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(de.longor.lmath.tuples.Tuple2)
	 */
	@Override
	public void setXY(Tuple2 tuple) {
		this.x = tuple.getXf();
		this.y = tuple.getYf();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(de.longor.lmath.tuples.Tuple3)
	 */
	@Override
	public void setXY(Tuple3 tuple) {
		this.x = tuple.getXf();
		this.y = tuple.getYf();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(de.longor.lmath.tuples.Tuple4)
	 */
	@Override
	public void setXY(Tuple4 tuple) {
		this.x = tuple.getXf();
		this.y = tuple.getYf();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(de.longor.lmath.tuples.Tuple3)
	 */
	@Override
	public void setXYZ(Tuple3 tuple) {
		this.x = tuple.getXf();
		this.y = tuple.getYf();
		this.z = tuple.getZf();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(de.longor.lmath.tuples.Tuple4)
	 */
	@Override
	public void setXYZ(Tuple4 tuple) {
		this.x = tuple.getXf();
		this.y = tuple.getYf();
		this.z = tuple.getZf();
	}

	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple4#setXYZW(de.longor.lmath.tuples.Tuple4)
	 */
	@Override
	public void setXYZW(Tuple4 tuple) {
		this.x = tuple.getXf();
		this.y = tuple.getYf();
		this.z = tuple.getZf();
		this.w = tuple.getWf();
	}
	
	@Override
	public String toString()
	{
		return "vec4f[" + x + ", " + y + ", " + z + ", " + w + "]";
	}
	
	/* Default Static OUT=IN-OP-IN Method
	public static final Vec2f add(Vec2f ain, Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4f
	 */
	public static final Vec4f add(Vec4f ain, Vec4f bin, Vec4f out)
	{
		out.x = ain.x + bin.x;
		out.y = ain.y + bin.y;
		out.z = ain.z + bin.z;
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4f
	 */
	public static final Vec4f sub(Vec4f ain, Vec4f bin, Vec4f out)
	{
		out.x = ain.x - bin.x;
		out.y = ain.y - bin.y;
		out.z = ain.z - bin.z;
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4f
	 */
	public static final Vec4f mul(Vec4f ain, Vec4f bin, Vec4f out)
	{
		out.x = ain.x * bin.x;
		out.y = ain.y * bin.y;
		out.z = ain.z * bin.z;
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4f
	 */
	public static final Vec4f div(Vec4f ain, Vec4f bin, Vec4f out)
	{
		out.x = ain.x / bin.x;
		out.y = ain.y / bin.y;
		out.z = ain.z / bin.z;
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4f
	 */
	public static final Vec4f mod(Vec4f ain, Vec4f bin, Vec4f out)
	{
		out.x = ain.x % bin.x;
		out.y = ain.y % bin.y;
		out.z = ain.z % bin.z;
		return out;
	}
	
	/**
	 * Distance.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @return the double
	 */
	public static final double distance(Vec4f ain, Vec4f bin)
	{
		double offx = bin.x - ain.x;
		double offy = bin.y - ain.y;
		double offz = bin.z - ain.z;
		return Math.sqrt(offx*offx + offy*offy + offz*offz);
	}
	
	/* Default OUT=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4f
	 */
	public final Vec4f add(Vec4f bin, Vec4f out)
	{
		out.x = this.x + bin.x;
		out.y = this.y + bin.y;
		out.z = this.z + bin.z;
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4f
	 */
	public final Vec4f sub(Vec4f bin, Vec4f out)
	{
		out.x = this.x - bin.x;
		out.y = this.y - bin.y;
		out.z = this.z - bin.z;
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4f
	 */
	public final Vec4f mul(Vec4f bin, Vec4f out)
	{
		out.x = this.x * bin.x;
		out.y = this.y * bin.y;
		out.z = this.z * bin.z;
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4f
	 */
	public final Vec4f div(Vec4f bin, Vec4f out)
	{
		out.x = this.x / bin.x;
		out.y = this.y / bin.y;
		out.z = this.z / bin.z;
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4f
	 */
	public final Vec4f mod(Vec4f bin, Vec4f out)
	{
		out.x = this.x % bin.x;
		out.y = this.y % bin.y;
		out.z = this.z % bin.z;
		return out;
	}
	
	/**
	 * Distance.
	 *
	 * @param bin the bin
	 * @return the double
	 */
	public final double distance(Vec4f bin)
	{
		double offx = bin.x - this.x;
		double offy = bin.y - this.y;
		double offz = bin.z - this.z;
		return Math.sqrt(offx*offx + offy*offy + offz*offz);
	}
	
	/* Default THIS=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @return the vec4f
	 */
	public final Vec4f add(Vec4f bin)
	{
		this.x = this.x + bin.x;
		this.y = this.y + bin.y;
		this.z = this.z + bin.z;
		return this;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @return the vec4f
	 */
	public final Vec4f sub(Vec4f bin)
	{
		this.x = this.x - bin.x;
		this.y = this.y - bin.y;
		this.z = this.z - bin.z;
		return this;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @return the vec4f
	 */
	public final Vec4f mul(Vec4f bin)
	{
		this.x = this.x * bin.x;
		this.y = this.y * bin.y;
		this.z = this.z * bin.z;
		return this;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @return the vec4f
	 */
	public final Vec4f div(Vec4f bin)
	{
		this.x = this.x / bin.x;
		this.y = this.y / bin.y;
		this.z = this.z / bin.z;
		return this;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @return the vec4f
	 */
	public final Vec4f mod(Vec4f bin)
	{
		this.x = this.x % bin.x;
		this.y = this.y % bin.y;
		this.z = this.z % bin.z;
		return this;
	}
	
	/* Default THIS=THIS-OP-THIS Method
	public final Vec2f add(Vec2f bin)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @return the vec4f
	 */
	public final Vec4f add()
	{
		this.x = this.x + this.x;
		this.y = this.y + this.y;
		this.z = this.z + this.z;
		return this;
	}
	
	/**
	 * Mul.
	 *
	 * @return the vec4f
	 */
	public final Vec4f mul()
	{
		this.x = this.x * this.x;
		this.y = this.y * this.y;
		this.z = this.z * this.z;
		return this;
	}
	
	/* Default Static OUT=IN-OP-IN Method
	public static final Vec2f add(Vec2f ain, Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4f
	 */
	public static final Vec4f add(Vec4f ain, Tuple4 bin, Vec4f out)
	{
		out.x = ain.x + bin.getXf();
		out.y = ain.y + bin.getYf();
		out.z = ain.z + bin.getZf();
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4f
	 */
	public static final Vec4f sub(Vec4f ain, Tuple4 bin, Vec4f out)
	{
		out.x = ain.x - bin.getXf();
		out.y = ain.y - bin.getYf();
		out.z = ain.z - bin.getZf();
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4f
	 */
	public static final Vec4f mul(Vec4f ain, Tuple4 bin, Vec4f out)
	{
		out.x = ain.x * bin.getXf();
		out.y = ain.y * bin.getYf();
		out.z = ain.z * bin.getZf();
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4f
	 */
	public static final Vec4f div(Vec4f ain, Tuple4 bin, Vec4f out)
	{
		out.x = ain.x / bin.getXf();
		out.y = ain.y / bin.getYf();
		out.z = ain.z / bin.getZf();
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4f
	 */
	public static final Vec4f mod(Vec4f ain, Tuple4 bin, Vec4f out)
	{
		out.x = ain.x % bin.getXf();
		out.y = ain.y % bin.getYf();
		out.z = ain.z % bin.getZf();
		return out;
	}
	
	/* Default OUT=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4f
	 */
	public final Vec4f add(Tuple4 bin, Vec4f out)
	{
		out.x = this.x + bin.getXf();
		out.y = this.y + bin.getYf();
		out.z = this.z + bin.getZf();
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4f
	 */
	public final Vec4f sub(Tuple4 bin, Vec4f out)
	{
		out.x = this.x - bin.getXf();
		out.y = this.y - bin.getYf();
		out.z = this.z - bin.getZf();
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4f
	 */
	public final Vec4f mul(Tuple4 bin, Vec4f out)
	{
		out.x = this.x * bin.getXf();
		out.y = this.y * bin.getYf();
		out.z = this.z * bin.getZf();
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4f
	 */
	public final Vec4f div(Tuple4 bin, Vec4f out)
	{
		out.x = this.x / bin.getXf();
		out.y = this.y / bin.getYf();
		out.z = this.z / bin.getZf();
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec4f
	 */
	public final Vec4f mod(Tuple4 bin, Vec4f out)
	{
		out.x = this.x % bin.getXf();
		out.y = this.y % bin.getYf();
		out.z = this.z % bin.getZf();
		return out;
	}
	
	/**
	 * Distance.
	 *
	 * @param bin the bin
	 * @return the double
	 */
	public final double distance(Tuple4 bin)
	{
		double offx = bin.getXd() - this.x;
		double offy = bin.getYd() - this.y;
		double offz = bin.getZd() - this.z;
		return Math.sqrt(offx*offx + offy*offy + offz*offz);
	}
	
	/* Default THIS=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @return the vec4f
	 */
	public final Vec4f add(Tuple4 bin)
	{
		this.x = this.x + bin.getXf();
		this.y = this.y + bin.getYf();
		this.z = this.z + bin.getZf();
		return this;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @return the vec4f
	 */
	public final Vec4f sub(Tuple4 bin)
	{
		this.x = this.x - bin.getXf();
		this.y = this.y - bin.getYf();
		this.z = this.z - bin.getZf();
		return this;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @return the vec4f
	 */
	public final Vec4f mul(Tuple4 bin)
	{
		this.x = this.x * bin.getXf();
		this.y = this.y * bin.getYf();
		this.z = this.z * bin.getZf();
		return this;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @return the vec4f
	 */
	public final Vec4f div(Tuple4 bin)
	{
		this.x = this.x / bin.getXf();
		this.y = this.y / bin.getYf();
		this.z = this.z / bin.getZf();
		return this;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @return the vec4f
	 */
	public final Vec4f mod(Tuple4 bin)
	{
		this.x = this.x % bin.getXf();
		this.y = this.y % bin.getYf();
		this.z = this.z % bin.getZf();
		return this;
	}
	
	/* Default Static OUT=IN-OP-IN Method
	public static final Vec2f add(Vec2f ain, Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the tuple4
	 */
	public static final Tuple4 add(Vec4f ain, Tuple4 bin, Tuple4 out)
	{
		out.setX(ain.x + bin.getXf());
		out.setY(ain.y + bin.getYf());
		out.setZ(ain.z + bin.getZf());
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the tuple4
	 */
	public static final Tuple4 sub(Vec4f ain, Tuple4 bin, Tuple4 out)
	{
		out.setX(ain.x - bin.getXf());
		out.setY(ain.y - bin.getYf());
		out.setZ(ain.z - bin.getZf());
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the tuple4
	 */
	public static final Tuple4 mul(Vec4f ain, Tuple4 bin, Tuple4 out)
	{
		out.setX(ain.x * bin.getXf());
		out.setY(ain.y * bin.getYf());
		out.setZ(ain.z * bin.getZf());
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the tuple4
	 */
	public static final Tuple4 div(Vec4f ain, Tuple4 bin, Tuple4 out)
	{
		out.setX(ain.x / bin.getXf());
		out.setY(ain.y / bin.getYf());
		out.setZ(ain.z / bin.getZf());
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the tuple4
	 */
	public static final Tuple4 mod(Vec4f ain, Tuple4 bin, Tuple4 out)
	{
		out.setX(ain.x % bin.getXf());
		out.setY(ain.y % bin.getYf());
		out.setZ(ain.z % bin.getZf());
		return out;
	}
	
	public final void normalize()
	{
		float len = LMath.length(x, y, z);
		x /= len;
		y /= len;
		z /= len;
		w = 1;
	}
	
	public final float length()
	{
		return (float) Math.sqrt((x*x) + (y*y) + (z*z));
	}
	
	/**
	 * Returns the dot-product of this vector with itself.
	 * @return the dot product of this vector with itself.
	 **/
	public final float dotproduct()
	{
		return (this.x*this.x) + (this.y*this.y) + (this.z*this.z);
	}
	
	/**
	 * Returns the dot-product of this vector with the given one.
	 * @param v The vector to create a dot-product with this one from.
	 * @return the dot product of this vector with the given one.
	 **/
	public final float dotproduct(Vec3f v)
	{
		return (this.x*v.x) + (this.y*v.y) + (this.z*v.z);
	}
	
	/**
	 * Returns the dot-product of this vector with the given one.
	 * @param v The vector to create a dot-product with this one from.
	 * @return the dot product of this vector with the given one.
	 **/
	public final double dotproduct(Vec3d v)
	{
		return (this.x*v.x) + (this.y*v.y) + (this.z*v.z);
	}
	
	/**
	 * Returns the dot-product of this vector with the given one.
	 * @param v The vector to create a dot-product with this one from.
	 * @return the dot product of this vector with the given one.
	 **/
	public final float dotproduct(Vec4f v)
	{
		return (this.x*v.x) + (this.y*v.y) + (this.z*v.z);
	}
	
	/**
	 * Returns the dot-product of this vector with the given one.
	 * @param v The vector to create a dot-product with this one from.
	 * @return the dot product of this vector with the given one.
	 **/
	public final double dotproduct(Vec4d v)
	{
		return (this.x*v.x) + (this.y*v.y) + (this.z*v.z);
	}
	
}
