package de.longor.lmath.tuples;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/**
 * A class representing a 2-component (floating-point) vector.
 */
public class Vec2f implements Tuple2
{
	
	/** The x. */
	public float x;
	
	/** The y. */
	public float y;
	
	/**
	 * Instantiates a new vec2f.
	 */
	public Vec2f()
	{
		x = 0;
		y = 0;
	}
	
	/**
	 * Instantiates a new vec2f.
	 *
	 * @param XY the xy
	 */
	public Vec2f(int XY)
	{
		x = XY;
		y = XY;
	}
	
	/**
	 * Instantiates a new vec2f.
	 *
	 * @param X the x
	 * @param Y the y
	 */
	public Vec2f(int X, int Y)
	{
		x = X;
		y = Y;
	}
	
	/**
	 * Instantiates a new vec2f.
	 *
	 * @param XY the xy
	 */
	public Vec2f(float XY)
	{
		x = XY;
		y = XY;
	}
	
	/**
	 * Instantiates a new vec2f.
	 *
	 * @param X the x
	 * @param Y the y
	 */
	public Vec2f(float X, float Y)
	{
		x = X;
		y = Y;
	}
	
	/**
	 * Instantiates a new vec2f.
	 *
	 * @param XY the xy
	 */
	public Vec2f(double XY)
	{
		x = (float) XY;
		y = (float) XY;
	}
	
	/**
	 * Instantiates a new vec2f.
	 *
	 * @param X the x
	 * @param Y the y
	 */
	public Vec2f(double X, double Y)
	{
		x = (float) X;
		y = (float) Y;
	}
	
	/**
	 * Instantiates a new vec2f.
	 *
	 * @param vec the vec
	 */
	public Vec2f(Vec2f vec)
	{
		x = vec.x;
		y = vec.y;
	}
	
	/**
	 * Instantiates a new vec2f.
	 *
	 * @param vec the vec
	 */
	public Vec2f(Vec3f vec)
	{
		x = vec.x;
		y = vec.y;
	}
	
	/**
	 * Instantiates a new vec2f.
	 *
	 * @param vec the vec
	 */
	public Vec2f(Vec4f vec)
	{
		x = vec.x;
		y = vec.y;
	}
	
	/**
	 * Instantiates a new vec2f.
	 *
	 * @param vec the vec
	 */
	public Vec2f(Vec2d vec)
	{
		x = (float) vec.x;
		y = (float) vec.y;
	}
	
	/**
	 * Instantiates a new vec2f.
	 *
	 * @param vec the vec
	 */
	public Vec2f(Vec3d vec)
	{
		x = (float) vec.x;
		y = (float) vec.y;
	}
	
	/**
	 * Instantiates a new vec2f.
	 *
	 * @param vec the vec
	 */
	public Vec2f(Vec4d vec)
	{
		x = (float) vec.x;
		y = (float) vec.y;
	}
	
	/**
	 * Instantiates a new vec2f.
	 *
	 * @param tuple the tuple
	 */
	public Vec2f(Tuple2 tuple)
	{
		x = tuple.getXf();
		y = tuple.getYf();
	}
	
	/**
	 * Instantiates a new vec2f.
	 *
	 * @param tuple the tuple
	 */
	public Vec2f(Tuple3 tuple)
	{
		x = tuple.getXf();
		y = tuple.getYf();
	}
	
	/**
	 * Instantiates a new vec2f.
	 *
	 * @param tuple the tuple
	 */
	public Vec2f(Tuple4 tuple)
	{
		x = tuple.getXf();
		y = tuple.getYf();
	}
	
	/**
	 * Instantiates a new vec2f.
	 *
	 * @param buffer the buffer
	 */
	public Vec2f(IntBuffer buffer)
	{
		x = buffer.get(0);
		y = buffer.get(1);
	}
	
	/**
	 * Instantiates a new vec2f.
	 *
	 * @param buffer the buffer
	 */
	public Vec2f(FloatBuffer buffer)
	{
		x = buffer.get(0);
		y = buffer.get(1);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getXf()
	 */
	@Override
	public float getXf() {
		return x;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getXd()
	 */
	@Override
	public double getXd() {
		return x;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getYf()
	 */
	@Override
	public float getYf() {
		return y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getYd()
	 */
	@Override
	public double getYd() {
		return y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setX(float)
	 */
	@Override
	public void setX(float value) {
		x = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setX(double)
	 */
	@Override
	public void setX(double value) {
		x = (float) value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setY(float)
	 */
	@Override
	public void setY(float value) {
		y = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setY(double)
	 */
	@Override
	public void setY(double value) {
		y = (float) value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(float, float)
	 */
	@Override
	public void setXY(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(double, double)
	 */
	@Override
	public void setXY(double x, double y) {
		this.x = (float) x;
		this.y = (float) y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(float)
	 */
	@Override
	public void setXY(float xy) {
		this.x = xy;
		this.y = xy;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(double)
	 */
	@Override
	public void setXY(double xy) {
		this.x = (float) xy;
		this.y = (float) xy;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(de.longor.lmath.tuples.Tuple2)
	 */
	@Override
	public void setXY(Tuple2 tuple) {
		this.x = tuple.getXf();
		this.y = tuple.getYf();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(de.longor.lmath.tuples.Tuple3)
	 */
	@Override
	public void setXY(Tuple3 tuple) {
		this.x = tuple.getXf();
		this.y = tuple.getYf();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(de.longor.lmath.tuples.Tuple4)
	 */
	@Override
	public void setXY(Tuple4 tuple) {
		this.x = tuple.getXf();
		this.y = tuple.getYf();
	}
	
	@Override
	public String toString()
	{
		return "vec2f[" + x + ", " + y + "]";
	}
	
	/* Default Static OUT=IN-OP-IN Method
	public static final Vec2f add(Vec2f ain, Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2f
	 */
	public static final Vec2f add(Vec2f ain, Vec2f bin, Vec2f out)
	{
		out.x = ain.x + bin.x;
		out.y = ain.y + bin.y;
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2f
	 */
	public static final Vec2f sub(Vec2f ain, Vec2f bin, Vec2f out)
	{
		out.x = ain.x - bin.x;
		out.y = ain.y - bin.y;
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2f
	 */
	public static final Vec2f mul(Vec2f ain, Vec2f bin, Vec2f out)
	{
		out.x = ain.x * bin.x;
		out.y = ain.y * bin.y;
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2f
	 */
	public static final Vec2f div(Vec2f ain, Vec2f bin, Vec2f out)
	{
		out.x = ain.x / bin.x;
		out.y = ain.y / bin.y;
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2f
	 */
	public static final Vec2f mod(Vec2f ain, Vec2f bin, Vec2f out)
	{
		out.x = ain.x % bin.x;
		out.y = ain.y % bin.y;
		return out;
	}
	
	/**
	 * Distance.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @return the float
	 */
	public static final float distance(Vec2f ain, Vec2f bin)
	{
		float offx = bin.x - ain.x;
		float offy = bin.y - ain.y;
		return (float) Math.sqrt(offx*offx + offy*offy);
	}
	
	/**
	 * Lerp.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param x the x
	 * @param out the out
	 * @return the vec2f
	 */
	public static final Vec2f lerp(Vec2f ain, Vec2f bin, float x, Vec2f out)
	{
		out.x = ain.x + (x * (bin.x - ain.x));
		out.y = ain.y + (x * (bin.y - ain.y));
		return out;
	}
	
	/* Default OUT=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2f
	 */
	public final Vec2f add(Vec2f bin, Vec2f out)
	{
		out.x = this.x + bin.x;
		out.y = this.y + bin.y;
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2f
	 */
	public final Vec2f sub(Vec2f bin, Vec2f out)
	{
		out.x = this.x - bin.x;
		out.y = this.y - bin.y;
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2f
	 */
	public final Vec2f mul(Vec2f bin, Vec2f out)
	{
		out.x = this.x * bin.x;
		out.y = this.y * bin.y;
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2f
	 */
	public final Vec2f div(Vec2f bin, Vec2f out)
	{
		out.x = this.x / bin.x;
		out.y = this.y / bin.y;
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2f
	 */
	public final Vec2f mod(Vec2f bin, Vec2f out)
	{
		out.x = this.x % bin.x;
		out.y = this.y % bin.y;
		return out;
	}
	
	/**
	 * Distance.
	 *
	 * @param bin the bin
	 * @return the float
	 */
	public final float distance(Vec2f bin)
	{
		float offx = this.x - bin.x;
		float offy = this.y - bin.y;
		return (float) Math.sqrt(offx*offx + offy*offy);
	}
	
	/**
	 * Lerp.
	 *
	 * @param bin the bin
	 * @param x the x
	 * @param out the out
	 * @return the vec2f
	 */
	public final Vec2f lerp(Vec2f bin, float x, Vec2f out)
	{
		out.x = this.x + (x * (bin.x - this.x));
		out.y = this.y + (x * (bin.x - this.y));
		return out;
	}
	
	/* Default THIS=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @return the vec2f
	 */
	public final Vec2f add(Vec2f bin)
	{
		this.x = this.x + bin.x;
		this.y = this.y + bin.y;
		return this;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @return the vec2f
	 */
	public final Vec2f sub(Vec2f bin)
	{
		this.x = this.x - bin.x;
		this.y = this.y - bin.y;
		return this;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @return the vec2f
	 */
	public final Vec2f mul(Vec2f bin)
	{
		this.x = this.x * bin.x;
		this.y = this.y * bin.y;
		return this;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @return the vec2f
	 */
	public final Vec2f div(Vec2f bin)
	{
		this.x = this.x / bin.x;
		this.y = this.y / bin.y;
		return this;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @return the vec2f
	 */
	public final Vec2f mod(Vec2f bin)
	{
		this.x = this.x % bin.x;
		this.y = this.y % bin.y;
		return this;
	}
	
	/* Default THIS=THIS-OP-THIS Method
	public final Vec2f add(Vec2f bin)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @return the vec2f
	 */
	public final Vec2f add()
	{
		this.x = this.x + this.x;
		this.y = this.y + this.y;
		return this;
	}
	
	/**
	 * Sub.
	 *
	 * @return the vec2f
	 */
	public final Vec2f sub()
	{
		this.x = this.x - this.x;
		this.y = this.y - this.y;
		return this;
	}
	
	/**
	 * Mul.
	 *
	 * @return the vec2f
	 */
	public final Vec2f mul()
	{
		this.x = this.x * this.x;
		this.y = this.y * this.y;
		return this;
	}
	
	/**
	 * Div.
	 *
	 * @return the vec2f
	 */
	public final Vec2f div()
	{
		this.x = this.x / this.x;
		this.y = this.y / this.y;
		return this;
	}
	
	/**
	 * Mod.
	 *
	 * @return the vec2f
	 */
	public final Vec2f mod()
	{
		this.x = this.x % this.x;
		this.y = this.y % this.y;
		return this;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	/* Default Static OUT=IN-OP-IN Method
	public static final Vec2f add(Vec2f ain, Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2f
	 */
	public static final Vec2f add(Vec2f ain, Tuple2 bin, Vec2f out)
	{
		out.x = ain.x + bin.getXf();
		out.y = ain.y + bin.getYf();
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2f
	 */
	public static final Vec2f sub(Vec2f ain, Tuple2 bin, Vec2f out)
	{
		out.x = ain.x - bin.getXf();
		out.y = ain.y - bin.getYf();
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2f
	 */
	public static final Vec2f mul(Vec2f ain, Tuple2 bin, Vec2f out)
	{
		out.x = ain.x * bin.getXf();
		out.y = ain.y * bin.getYf();
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2f
	 */
	public static final Vec2f div(Vec2f ain, Tuple2 bin, Vec2f out)
	{
		out.x = ain.x / bin.getXf();
		out.y = ain.y / bin.getYf();
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2f
	 */
	public static final Vec2f mod(Vec2f ain, Tuple2 bin, Vec2f out)
	{
		out.x = ain.x % bin.getXf();
		out.y = ain.y % bin.getYf();
		return out;
	}
	
	/**
	 * Lerp.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param x the x
	 * @param out the out
	 * @return the vec2f
	 */
	public static final Vec2f lerp(Vec2f ain, Tuple2 bin, float x, Vec2f out)
	{
		out.x = ain.x + (x * (bin.getXf() - ain.x));
		out.y = ain.y + (x * (bin.getYf() - ain.y));
		return out;
	}
	
	/* Default OUT=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2f
	 */
	public final Vec2f add(Tuple2 bin, Vec2f out)
	{
		out.x = this.x + bin.getXf();
		out.y = this.y + bin.getYf();
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2f
	 */
	public final Vec2f sub(Tuple2 bin, Vec2f out)
	{
		out.x = this.x - bin.getXf();
		out.y = this.y - bin.getYf();
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2f
	 */
	public final Vec2f mul(Tuple2 bin, Vec2f out)
	{
		out.x = this.x * bin.getXf();
		out.y = this.y * bin.getYf();
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2f
	 */
	public final Vec2f div(Tuple2 bin, Vec2f out)
	{
		out.x = this.x / bin.getXf();
		out.y = this.y / bin.getYf();
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec2f
	 */
	public final Vec2f mod(Tuple2 bin, Vec2f out)
	{
		out.x = this.x % bin.getXf();
		out.y = this.y % bin.getYf();
		return out;
	}
	
	/**
	 * Distance.
	 *
	 * @param bin the bin
	 * @return the float
	 */
	public final float distance(Tuple2 bin)
	{
		float offx = this.x - bin.getXf();
		float offy = this.y - bin.getYf();
		return (float) Math.sqrt(offx*offx + offy*offy);
	}
	
	/* Default THIS=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @return the vec2f
	 */
	public final Vec2f add(Tuple2 bin)
	{
		this.x = this.x + bin.getXf();
		this.y = this.y + bin.getYf();
		return this;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @return the vec2f
	 */
	public final Vec2f mul(Tuple2 bin)
	{
		this.x = this.x * bin.getXf();
		this.y = this.y * bin.getYf();
		return this;
	}
	
	public final void normalize()
	{
		float len = (float) Math.sqrt((x*x) + (y*y));
		x /= len;
		y /= len;
	}
	
	public final float length()
	{
		return (float) Math.sqrt((x*x) + (y*y));
	}
	
	/**
	 * Returns the dot-product of this vector with itself.
	 * @return the dot product of this vector with itself.
	 **/
	public final float dotproduct()
	{
		return (this.x*this.x) + (this.y*this.y);
	}
	
	/**
	 * Returns the dot-product of this vector with the given one.
	 * @param v The vector to create a dot-product with this one from.
	 * @return the dot product of this vector with the given one.
	 **/
	public final float dotproduct(Vec2f v)
	{
		return (this.x*v.x) + (this.y*v.y);
	}
	
	/**
	 * Returns the dot-product of this vector with the given one.
	 * @param v The vector to create a dot-product with this one from.
	 * @return the dot product of this vector with the given one.
	 **/
	public final double dotproduct(Vec2d v)
	{
		return (this.x*v.x) + (this.y*v.y);
	}
	
}
