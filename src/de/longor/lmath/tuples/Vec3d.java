package de.longor.lmath.tuples;

import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import de.longor.lmath.LMath;

/**
 * A class representing a 3-component (double precision floating-point) vector.
 */
public class Vec3d implements Tuple3
{
	
	/** The x. */
	public double x;
	
	/** The y. */
	public double y;
	
	/** The z. */
	public double z;
	
	/**
	 * Instantiates a new vec3d.
	 */
	public Vec3d()
	{
		x = 0;
		y = 0;
		z = 0;
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param XYZ the xyz
	 */
	public Vec3d(int XYZ)
	{
		x = XYZ;
		y = XYZ;
		z = XYZ;
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param X the x
	 * @param Y the y
	 */
	public Vec3d(int X, int Y)
	{
		x = X;
		y = Y;
		z = 0;
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 */
	public Vec3d(int X, int Y, int Z)
	{
		x = X;
		y = Y;
		z = Z;
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param XYZ the xyz
	 */
	public Vec3d(float XYZ)
	{
		x = XYZ;
		y = XYZ;
		z = XYZ;
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param X the x
	 * @param Y the y
	 */
	public Vec3d(float X, float Y)
	{
		x = X;
		y = Y;
		z = 0;
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 */
	public Vec3d(float X, float Y, float Z)
	{
		x = X;
		y = Y;
		z = Z;
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param XYZ the xyz
	 */
	public Vec3d(double XYZ)
	{
		x = XYZ;
		y = XYZ;
		z = XYZ;
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param X the x
	 * @param Y the y
	 */
	public Vec3d(double X, double Y)
	{
		x = X;
		y = Y;
		z = 0;
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 */
	public Vec3d(double X, double Y, double Z)
	{
		x = X;
		y = Y;
		z = Z;
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param vec the vec
	 */
	public Vec3d(Vec2f vec)
	{
		x = vec.x;
		y = vec.y;
		z = 0;
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param vec the vec
	 */
	public Vec3d(Vec3f vec)
	{
		x = vec.x;
		y = vec.y;
		z = vec.z;
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param vec the vec
	 */
	public Vec3d(Vec4f vec)
	{
		x = vec.x;
		y = vec.y;
		z = vec.z;
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param vec the vec
	 */
	public Vec3d(Vec2d vec)
	{
		x = vec.x;
		y = vec.y;
		z = 0;
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param vec the vec
	 */
	public Vec3d(Vec3d vec)
	{
		x = vec.x;
		y = vec.y;
		z = vec.z;
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param vec the vec
	 */
	public Vec3d(Vec4d vec)
	{
		x = vec.x;
		y = vec.y;
		z = vec.z;
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param tuple the tuple
	 */
	public Vec3d(Tuple2 tuple)
	{
		x = tuple.getXd();
		y = tuple.getYd();
		z = 0;
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param tuple the tuple
	 */
	public Vec3d(Tuple3 tuple)
	{
		x = tuple.getXd();
		y = tuple.getYd();
		z = tuple.getZd();
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param tuple the tuple
	 */
	public Vec3d(Tuple4 tuple)
	{
		x = tuple.getXd();
		y = tuple.getYd();
		z = tuple.getZd();
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param buffer the buffer
	 */
	public Vec3d(IntBuffer buffer)
	{
		x = buffer.get(0);
		y = buffer.get(1);
		z = buffer.get(2);
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param buffer the buffer
	 */
	public Vec3d(FloatBuffer buffer)
	{
		x = buffer.get(0);
		y = buffer.get(1);
		z = buffer.get(2);
	}
	
	/**
	 * Instantiates a new vec3d.
	 *
	 * @param buffer the buffer
	 */
	public Vec3d(DoubleBuffer buffer)
	{
		x = buffer.get(0);
		y = buffer.get(1);
		z = buffer.get(2);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getXf()
	 */
	@Override
	public float getXf() {
		return (float) x;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getXd()
	 */
	@Override
	public double getXd() {
		return x;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getYf()
	 */
	@Override
	public float getYf() {
		return (float) y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#getYd()
	 */
	@Override
	public double getYd() {
		return y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#getZf()
	 */
	@Override
	public float getZf() {
		return (float) z;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#getZd()
	 */
	@Override
	public double getZd() {
		return z;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setX(float)
	 */
	@Override
	public void setX(float value) {
		x = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setX(double)
	 */
	@Override
	public void setX(double value) {
		x =  value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setY(float)
	 */
	@Override
	public void setY(float value) {
		y = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setY(double)
	 */
	@Override
	public void setY(double value) {
		y =  value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setZ(float)
	 */
	@Override
	public void setZ(float value) {
		z = value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setZ(double)
	 */
	@Override
	public void setZ(double value) {
		z =  value;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(float, float)
	 */
	@Override
	public void setXY(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(double, double)
	 */
	@Override
	public void setXY(double x, double y) {
		this.x =  x;
		this.y =  y;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(float, float, float)
	 */
	@Override
	public void setXYZ(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(double, double, double)
	 */
	@Override
	public void setXYZ(double x, double y, double z) {
		this.x =  x;
		this.y =  y;
		this.z =  z;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(float)
	 */
	@Override
	public void setXY(float xy) {
		this.x = xy;
		this.y = xy;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(double)
	 */
	@Override
	public void setXY(double xy) {
		this.x =  xy;
		this.y =  xy;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(float)
	 */
	@Override
	public void setXYZ(float xyz) {
		this.x = xyz;
		this.y = xyz;
		this.z = xyz;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(double)
	 */
	@Override
	public void setXYZ(double xyz) {
		this.x =  xyz;
		this.y =  xyz;
		this.z =  xyz;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(de.longor.lmath.tuples.Tuple2)
	 */
	@Override
	public void setXY(Tuple2 tuple) {
		this.x = tuple.getXf();
		this.y = tuple.getYf();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(de.longor.lmath.tuples.Tuple3)
	 */
	@Override
	public void setXY(Tuple3 tuple) {
		this.x = tuple.getXf();
		this.y = tuple.getYf();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple2#setXY(de.longor.lmath.tuples.Tuple4)
	 */
	@Override
	public void setXY(Tuple4 tuple) {
		this.x = tuple.getXf();
		this.y = tuple.getYf();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(de.longor.lmath.tuples.Tuple3)
	 */
	@Override
	public void setXYZ(Tuple3 tuple) {
		this.x = tuple.getXf();
		this.y = tuple.getYf();
		this.z = tuple.getZf();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.lmath.tuples.Tuple3#setXYZ(de.longor.lmath.tuples.Tuple4)
	 */
	@Override
	public void setXYZ(Tuple4 tuple) {
		this.x = tuple.getXf();
		this.y = tuple.getYf();
		this.z = tuple.getZf();
	}
	
	@Override
	public String toString()
	{
		return "vec3d[" + x + ", " + y + ", " + z + "]";
	}
	
	/* Default Static OUT=IN-OP-IN Method
	public static final Vec2f add(Vec2f ain, Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3d
	 */
	public static final Vec3d add(Vec3d ain, Vec3d bin, Vec3d out)
	{
		out.x = ain.x + bin.x;
		out.y = ain.y + bin.y;
		out.z = ain.z + bin.z;
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3d
	 */
	public static final Vec3d sub(Vec3d ain, Vec3d bin, Vec3d out)
	{
		out.x = ain.x - bin.x;
		out.y = ain.y - bin.y;
		out.z = ain.z - bin.z;
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3d
	 */
	public static final Vec3d mul(Vec3d ain, Vec3d bin, Vec3d out)
	{
		out.x = ain.x * bin.x;
		out.y = ain.y * bin.y;
		out.z = ain.z * bin.z;
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3d
	 */
	public static final Vec3d div(Vec3d ain, Vec3d bin, Vec3d out)
	{
		out.x = ain.x / bin.x;
		out.y = ain.y / bin.y;
		out.z = ain.z / bin.z;
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3d
	 */
	public static final Vec3d mod(Vec3d ain, Vec3d bin, Vec3d out)
	{
		out.x = ain.x % bin.x;
		out.y = ain.y % bin.y;
		out.z = ain.z % bin.z;
		return out;
	}
	
	/**
	 * Distance.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @return the double
	 */
	public static final double distance(Vec3d ain, Vec3d bin)
	{
		double offx = bin.x - ain.x;
		double offy = bin.y - ain.y;
		double offz = bin.z - ain.z;
		return Math.sqrt(offx*offx + offy*offy + offz*offz);
	}
	
	/**
	 * Lerp.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param fraction the fraction
	 * @param out the out
	 * @return the vec3d
	 */
	public static final Vec3d lerp(Vec3d ain, Vec3d bin, double fraction, Vec3d out)
	{
		out.x = LMath.lerp(ain.x, bin.x, fraction);
		out.y = LMath.lerp(ain.y, bin.y, fraction);
		out.z = LMath.lerp(ain.z, bin.z, fraction);
		return out;
	}
	
	/* Default OUT=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3d
	 */
	public final Vec3d add(Vec3d bin, Vec3d out)
	{
		out.x = this.x + bin.x;
		out.y = this.y + bin.y;
		out.z = this.z + bin.z;
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3d
	 */
	public final Vec3d sub(Vec3d bin, Vec3d out)
	{
		out.x = this.x - bin.x;
		out.y = this.y - bin.y;
		out.z = this.z - bin.z;
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3d
	 */
	public final Vec3d mul(Vec3d bin, Vec3d out)
	{
		out.x = this.x * bin.x;
		out.y = this.y * bin.y;
		out.z = this.z * bin.z;
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3d
	 */
	public final Vec3d div(Vec3d bin, Vec3d out)
	{
		out.x = this.x / bin.x;
		out.y = this.y / bin.y;
		out.z = this.z / bin.z;
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3d
	 */
	public final Vec3d mod(Vec3d bin, Vec3d out)
	{
		out.x = this.x % bin.x;
		out.y = this.y % bin.y;
		out.z = this.z % bin.z;
		return out;
	}
	
	/**
	 * Distance.
	 *
	 * @param bin the bin
	 * @return the double
	 */
	public final double distance(Vec3d bin)
	{
		double offx = bin.x - this.x;
		double offy = bin.y - this.y;
		double offz = bin.z - this.z;
		return Math.sqrt(offx*offx + offy*offy + offz*offz);
	}
	
	/**
	 * Lerp.
	 *
	 * @param bin the bin
	 * @param fraction the fraction
	 * @param out the out
	 * @return the vec3d
	 */
	public final Vec3d lerp(Vec3d bin, double fraction, Vec3d out)
	{
		out.x = LMath.lerp(this.x, bin.x, fraction);
		out.y = LMath.lerp(this.y, bin.y, fraction);
		out.z = LMath.lerp(this.z, bin.z, fraction);
		return out;
	}
	
	/* Default THIS=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @return the vec3d
	 */
	public final Vec3d add(Vec3d bin)
	{
		this.x = this.x + bin.x;
		this.y = this.y + bin.y;
		this.z = this.z + bin.z;
		return this;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @return the vec3d
	 */
	public final Vec3d sub(Vec3d bin)
	{
		this.x = this.x - bin.x;
		this.y = this.y - bin.y;
		this.z = this.z - bin.z;
		return this;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @return the vec3d
	 */
	public final Vec3d mul(Vec3d bin)
	{
		this.x = this.x * bin.x;
		this.y = this.y * bin.y;
		this.z = this.z * bin.z;
		return this;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @return the vec3d
	 */
	public final Vec3d div(Vec3d bin)
	{
		this.x = this.x / bin.x;
		this.y = this.y / bin.y;
		this.z = this.z / bin.z;
		return this;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @return the vec3d
	 */
	public final Vec3d mod(Vec3d bin)
	{
		this.x = this.x % bin.x;
		this.y = this.y % bin.y;
		this.z = this.z % bin.z;
		return this;
	}
	
	/* Default THIS=THIS-OP-THIS Method
	public final Vec2f add(Vec2f bin)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @return the vec3d
	 */
	public final Vec3d add()
	{
		this.x = this.x + this.x;
		this.y = this.y + this.y;
		this.z = this.z + this.z;
		return this;
	}
	
	/**
	 * Mul.
	 *
	 * @return the vec3d
	 */
	public final Vec3d mul()
	{
		this.x = this.x * this.x;
		this.y = this.y * this.y;
		this.z = this.z * this.z;
		return this;
	}
	
	/* Default Static OUT=IN-OP-IN Method
	public static final Vec2f add(Vec2f ain, Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3d
	 */
	public static final Vec3d add(Vec3d ain, Tuple3 bin, Vec3d out)
	{
		out.x = ain.x + bin.getXf();
		out.y = ain.y + bin.getYf();
		out.z = ain.z + bin.getZf();
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3d
	 */
	public static final Vec3d sub(Vec3d ain, Tuple3 bin, Vec3d out)
	{
		out.x = ain.x - bin.getXf();
		out.y = ain.y - bin.getYf();
		out.z = ain.z - bin.getZf();
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3d
	 */
	public static final Vec3d mul(Vec3d ain, Tuple3 bin, Vec3d out)
	{
		out.x = ain.x * bin.getXf();
		out.y = ain.y * bin.getYf();
		out.z = ain.z * bin.getZf();
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3d
	 */
	public static final Vec3d div(Vec3d ain, Tuple3 bin, Vec3d out)
	{
		out.x = ain.x / bin.getXf();
		out.y = ain.y / bin.getYf();
		out.z = ain.z / bin.getZf();
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3d
	 */
	public static final Vec3d mod(Vec3d ain, Tuple3 bin, Vec3d out)
	{
		out.x = ain.x % bin.getXf();
		out.y = ain.y % bin.getYf();
		out.z = ain.z % bin.getZf();
		return out;
	}
	
	/* Default OUT=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3d
	 */
	public final Vec3d add(Tuple3 bin, Vec3d out)
	{
		out.x = this.x + bin.getXf();
		out.y = this.y + bin.getYf();
		out.z = this.z + bin.getZf();
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3d
	 */
	public final Vec3d sub(Tuple3 bin, Vec3d out)
	{
		out.x = this.x - bin.getXf();
		out.y = this.y - bin.getYf();
		out.z = this.z - bin.getZf();
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3d
	 */
	public final Vec3d mul(Tuple3 bin, Vec3d out)
	{
		out.x = this.x * bin.getXf();
		out.y = this.y * bin.getYf();
		out.z = this.z * bin.getZf();
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3d
	 */
	public final Vec3d div(Tuple3 bin, Vec3d out)
	{
		out.x = this.x / bin.getXf();
		out.y = this.y / bin.getYf();
		out.z = this.z / bin.getZf();
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @param out the out
	 * @return the vec3d
	 */
	public final Vec3d mod(Tuple3 bin, Vec3d out)
	{
		out.x = this.x % bin.getXf();
		out.y = this.y % bin.getYf();
		out.z = this.z % bin.getZf();
		return out;
	}
	
	/**
	 * Distance.
	 *
	 * @param bin the bin
	 * @return the double
	 */
	public final double distance(Tuple3 bin)
	{
		double offx = bin.getXd() - this.x;
		double offy = bin.getYd() - this.y;
		double offz = bin.getZd() - this.z;
		return Math.sqrt(offx*offx + offy*offy + offz*offz);
	}
	
	/**
	 * Lerp.
	 *
	 * @param bin the bin
	 * @param fraction the fraction
	 * @param out the out
	 * @return the vec3d
	 */
	public final Vec3d lerp(Tuple3 bin, double fraction, Vec3d out)
	{
		out.x = LMath.lerp(this.x, bin.getXd(), fraction);
		out.y = LMath.lerp(this.y, bin.getYd(), fraction);
		out.z = LMath.lerp(this.z, bin.getZd(), fraction);
		return out;
	}
	
	/* Default THIS=THIS-OP-IN Method
	public final Vec2f add(Vec2f bin)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param bin the bin
	 * @return the vec3d
	 */
	public final Vec3d add(Tuple3 bin)
	{
		this.x = this.x + bin.getXf();
		this.y = this.y + bin.getYf();
		this.z = this.z + bin.getZf();
		return this;
	}
	
	/**
	 * Sub.
	 *
	 * @param bin the bin
	 * @return the vec3d
	 */
	public final Vec3d sub(Tuple3 bin)
	{
		this.x = this.x - bin.getXf();
		this.y = this.y - bin.getYf();
		this.z = this.z - bin.getZf();
		return this;
	}
	
	/**
	 * Mul.
	 *
	 * @param bin the bin
	 * @return the vec3d
	 */
	public final Vec3d mul(Tuple3 bin)
	{
		this.x = this.x * bin.getXf();
		this.y = this.y * bin.getYf();
		this.z = this.z * bin.getZf();
		return this;
	}
	
	/**
	 * Div.
	 *
	 * @param bin the bin
	 * @return the vec3d
	 */
	public final Vec3d div(Tuple3 bin)
	{
		this.x = this.x / bin.getXf();
		this.y = this.y / bin.getYf();
		this.z = this.z / bin.getZf();
		return this;
	}
	
	/**
	 * Mod.
	 *
	 * @param bin the bin
	 * @return the vec3d
	 */
	public final Vec3d mod(Tuple3 bin)
	{
		this.x = this.x % bin.getXf();
		this.y = this.y % bin.getYf();
		this.z = this.z % bin.getZf();
		return this;
	}
	
	/* Default Static OUT=IN-OP-IN Method
	public static final Vec2f add(Vec2f ain, Vec2f bin, Vec2f out)
	{
		
		
		return out;
	}
	 */
	
	/**
	 * Adds the.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the tuple3
	 */
	public static final Tuple3 add(Vec3d ain, Tuple3 bin, Tuple3 out)
	{
		out.setX(ain.x + bin.getXf());
		out.setY(ain.y + bin.getYf());
		out.setZ(ain.z + bin.getZf());
		return out;
	}
	
	/**
	 * Sub.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the tuple3
	 */
	public static final Tuple3 sub(Vec3d ain, Tuple3 bin, Tuple3 out)
	{
		out.setX(ain.x - bin.getXf());
		out.setY(ain.y - bin.getYf());
		out.setZ(ain.z - bin.getZf());
		return out;
	}
	
	/**
	 * Mul.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the tuple3
	 */
	public static final Tuple3 mul(Vec3d ain, Tuple3 bin, Tuple3 out)
	{
		out.setX(ain.x * bin.getXf());
		out.setY(ain.y * bin.getYf());
		out.setZ(ain.z * bin.getZf());
		return out;
	}
	
	/**
	 * Div.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the tuple3
	 */
	public static final Tuple3 div(Vec3d ain, Tuple3 bin, Tuple3 out)
	{
		out.setX(ain.x / bin.getXf());
		out.setY(ain.y / bin.getYf());
		out.setZ(ain.z / bin.getZf());
		return out;
	}
	
	/**
	 * Mod.
	 *
	 * @param ain the ain
	 * @param bin the bin
	 * @param out the out
	 * @return the tuple3
	 */
	public static final Tuple3 mod(Vec3d ain, Tuple3 bin, Tuple3 out)
	{
		out.setX(ain.x % bin.getXf());
		out.setY(ain.y % bin.getYf());
		out.setZ(ain.z % bin.getZf());
		return out;
	}
	
	public final Vec3d normalize()
	{
		double len = Math.sqrt((x*x) + (y*y) + (z*z));
		x /= len;
		y /= len;
		z /= len;
		return this;
	}
	
	public final Vec3d normalize(Vec3d out)
	{
		double len = Math.sqrt((x*x) + (y*y) + (z*z));
		out.x = x / len;
		out.y = y / len;
		out.z = z / len;
		return out;
	}
	
	public final double length()
	{
		return Math.sqrt((x*x) + (y*y) + (z*z));
	}
	
	/**
	 * Returns the dot-product of this vector with itself.
	 * @return the dot product of this vector with itself.
	 **/
	public final double dotproduct()
	{
		return (this.x*this.x) + (this.y*this.y) + (this.z*this.z);
	}
	
	/**
	 * Returns the dot-product of this vector with the given one.
	 * @param v The vector to create a dot-product with this one from.
	 * @return the dot product of this vector with the given one.
	 **/
	public final double dotproduct(Vec3f v)
	{
		return (this.x*v.x) + (this.y*v.y) + (this.z*v.z);
	}
	
	/**
	 * Returns the dot-product of this vector with the given one.
	 * @param v The vector to create a dot-product with this one from.
	 * @return the dot product of this vector with the given one.
	 **/
	public final double dotproduct(Vec3d v)
	{
		return (this.x*v.x) + (this.y*v.y) + (this.z*v.z);
	}
	
	/**
	 * Returns the dot-product of this vector with the given one.
	 * @param v The vector to create a dot-product with this one from.
	 * @return the dot product of this vector with the given one.
	 **/
	public final double dotproduct(Vec4f v)
	{
		return (this.x*v.x) + (this.y*v.y) + (this.z*v.z);
	}
	
	/**
	 * Returns the dot-product of this vector with the given one.
	 * @param v The vector to create a dot-product with this one from.
	 * @return the dot product of this vector with the given one.
	 **/
	public final double dotproduct(Vec4d v)
	{
		return (this.x*v.x) + (this.y*v.y) + (this.z*v.z);
	}
	
	/**
	 * Calculates the cross-product of this vector with the given vector and stores the result in this vector.
	 * @param v The right vector in the calculation.
	 * @return This vector with the result inside.
	 **/
	public final Vec3d crossproduct(Vec3d v)
	{
		double X = (this.y * v.z) - (this.z * v.y);
		double Y = (this.z * v.x) - (this.x * v.z);
	    double Z = (this.x * v.y) - (this.y * v.x);
	    
	    this.x = X;
	    this.y = Y;
	    this.z = Z;
	    
	    return this;
	}
	
	/**
	 * Calculates the cross-product of this vector with the given vector and stores the result in this vector.
	 * @param v The right vector in the calculation.
	 * @return This vector with the result inside.
	 **/
	public final Vec3d crossproduct(Vec3f v)
	{
		double X = (this.y * v.z) - (this.z * v.y);
		double Y = (this.z * v.x) - (this.x * v.z);
	    double Z = (this.x * v.y) - (this.y * v.x);
	    
	    this.x = X;
	    this.y = Y;
	    this.z = Z;
	    
	    return this;
	}
	
	/**
	 * Calculates the cross-product of this vector with the given vector and stores the result in the OUT vector.
	 * @param v The right vector in the calculation.
	 * @return The vector with the result inside.
	 **/
	public final Vec3d crossproduct(Vec3d v, Vec3d out)
	{
		double X = (this.y * v.z) - (this.z * v.y);
		double Y = (this.z * v.x) - (this.x * v.z);
		double Z = (this.x * v.y) - (this.y * v.x);
	    
	    out.x = X;
	    out.y = Y;
	    out.z = Z;
	    
	    return out;
	}
	
	/**
	 * Calculates the cross-product of this vector with the given vector and stores the result in the OUT vector.
	 * @param v The right vector in the calculation.
	 * @return The vector with the result inside.
	 **/
	public final Vec3d crossproduct(Vec3f v, Vec3d out)
	{
		double X = (this.y * v.z) - (this.z * v.y);
		double Y = (this.z * v.x) - (this.x * v.z);
		double Z = (this.x * v.y) - (this.y * v.x);
	    
	    out.x = X;
	    out.y = Y;
	    out.z = Z;
	    
	    return out;
	}
	
	/**
	 * Calculates the cross-product of this vector with the given vector and stores the result in the OUT vector.
	 * @param v The right vector in the calculation.
	 * @return The vector with the result inside.
	 **/
	public final Tuple3 crossproduct(Vec3f v, Tuple3 out)
	{
		double X = (this.y * v.z) - (this.z * v.y);
		double Y = (this.z * v.x) - (this.x * v.z);
		double Z = (this.x * v.y) - (this.y * v.x);
	    
	    out.setX(X);
	    out.setY(Y);
	    out.setZ(Z);
	    
	    return out;
	}

	public Vec3d rotateAroundX(float theta)
	{
		double ny = ((this.y*Math.cos(theta)) - (this.z*Math.sin(theta)));
		double nz = ((this.y*Math.sin(theta)) + (this.z*Math.cos(theta)));
		double nx = this.z;
		this.x = nx;
		this.y = ny;
		this.z = nz;
		return this;
	}

	public Vec3d rotateAroundY(float theta)
	{
		double nz = ((this.z*Math.cos(theta)) - (this.x*Math.sin(theta)));
		double nx = ((this.z*Math.sin(theta)) + (this.x*Math.cos(theta)));
		double ny = this.z;
		this.x = nx;
		this.y = ny;
		this.z = nz;
		return this;
	}
	
	public Vec3d rotateAroundZ(float theta)
	{
		double nx = ((this.x*Math.cos(theta)) - (this.y*Math.sin(theta)));
		double ny = ((this.x*Math.sin(theta)) + (this.y*Math.cos(theta)));
		double nz = this.z;
		this.x = nx;
		this.y = ny;
		this.z = nz;
		return this;
	}

	public Vec3d rotateAroundX(float theta, Vec3d out)
	{
		double ny = ((this.y*Math.cos(theta)) - (this.z*Math.sin(theta)));
		double nz = ((this.y*Math.sin(theta)) + (this.z*Math.cos(theta)));
		double nx = this.z;
		out.x = nx;
		out.y = ny;
		out.z = nz;
		return out;
	}

	public Vec3d rotateAroundY(float theta, Vec3d out)
	{
		double nz = ((this.z*Math.cos(theta)) - (this.x*Math.sin(theta)));
		double nx = ((this.z*Math.sin(theta)) + (this.x*Math.cos(theta)));
		double ny = this.z;
		out.x = nx;
		out.y = ny;
		out.z = nz;
		return out;
	}
	
	public Vec3d rotateAroundZ(float theta, Vec3d out)
	{
		double nx = ((this.x*Math.cos(theta)) - (this.y*Math.sin(theta)));
		double ny = ((this.x*Math.sin(theta)) + (this.y*Math.cos(theta)));
		double nz = this.z;
		out.x = nx;
		out.y = ny;
		out.z = nz;
		return out;
	}
	
}
